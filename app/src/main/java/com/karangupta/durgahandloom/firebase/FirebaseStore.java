package com.karangupta.durgahandloom.firebase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.karangupta.durgahandloom.address.AddressActivityPresenterImpl;
import com.karangupta.durgahandloom.cart.CartPresenterImpl;
import com.karangupta.durgahandloom.collections.CollectionsPresenterImpl;
import com.karangupta.durgahandloom.details.DetailsPresenterImpl;
import com.karangupta.durgahandloom.intro.SplashActivity;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentPresenterImpl;
import com.karangupta.durgahandloom.main_activity.order_history.OrderHistoryPresenterImpl;
import com.karangupta.durgahandloom.order_detail.OrderDetailPresenterImpl;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.cart_model.CartModel;
import com.karangupta.durgahandloom.util.cart_model.CartModelWrapper;
import com.karangupta.durgahandloom.util.count_model.CountModel;
import com.karangupta.durgahandloom.util.count_model.CountModelWrapper;
import com.karangupta.durgahandloom.util.order_detail_model.OrderDetailModel;
import com.karangupta.durgahandloom.util.order_detail_model.OrderDetailModelWrapper;
import com.karangupta.durgahandloom.util.order_history_model.OrderHistoryModel;
import com.karangupta.durgahandloom.util.order_history_model.OrderHistoryModelWrapper;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;
import com.karangupta.durgahandloom.util.views_model.ViewsModelWrapper;
import com.karangupta.durgahandloom.wishlist.WishlistPresenterImpl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Maybe;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;

/**
 * Created by karangupta on 27/03/18.
 */
@Singleton
public class FirebaseStore {
    @NonNull private final FirebaseDatabase firebaseDatabase;
    @NonNull private final FirebaseStorage firebaseStorage;
    @NonNull private final Context context;
    @Nullable private SharedPreferences pref;

    private int flag = 0;

    @Inject
    public FirebaseStore(@NonNull Context context) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        this.context = context;
    }

    public Boolean isUserLoggedIn() {
        return FirebaseAuth.getInstance().getCurrentUser() != null;
    }

    public Maybe<DataSnapshot> fetchBaluchuri() {
        Query query = firebaseDatabase.getReference().child("Home").child("Baluchuri").orderByKey();
        return RxFirebaseDatabase.observeSingleValueEvent(query);

       /* //  HomeSaree homeSaree1=new HomeSaree();HomeSaree homeSaree2=new HomeSaree();
        // HomeSaree homeSaree3=new HomeSaree();HomeSaree homeSaree4=new HomeSaree();

        // homeSaree1.setCategory("Dhakai");homeSaree1.setUrl("https://firebasestorage.googleapis.com/v0/b/durga-handloom.appspot.com/o/Home%2FBaluchuri%2FNo-Nakshapar.jpg?alt=media&token=3daccfc8-f2a4-40bd-8cfc-ca88a1ea965f");
        // homeSaree2.setCategory("Matha");homeSaree2.setUrl("https://firebasestorage.googleapis.com/v0/b/durga-handloom.appspot.com/o/Home%2FBaluchuri%2FNakshapar.jpg?alt=media&token=666e9c8a-20d9-4b64-ac62-117f9e3b9841");
        //homeSaree3.setCategory("Baluchuri(S)");homeSaree3.setUrl("https://firebasestorage.googleapis.com/v0/b/durga-handloom.appspot.com/o/Home%2FBaluchuri%2FBaluchuri(S).jpg?alt=media&token=ffff5174-778f-439d-8a45-4afdb293b962");
        //homeSaree4.setCategory("Baluchuri(T)");homeSaree4.setUrl("https://firebasestorage.googleapis.com/v0/b/durga-handloom.appspot.com/o/Home%2FBaluchuri%2FBaluchuri(T).jpg?alt=media&token=d210658a-c753-442c-9f19-381224e474c0");
        // List<HomeSaree> list=new ArrayList<>();
        // list.add(homeSaree1);
        // list.add(homeSaree2);
        // list.add(homeSaree3);
        //list.add(homeSaree4);
        //firebaseDatabase.getReference().child("Home").child("Jaamdani").setValue(list);
        //firebaseDatabase.getReference().child("Home").child("Dhniya Kali").setValue(list);
        //firebaseDatabase.getReference().child("Home").child("BahaCotton").setValue(list);
        //firebaseDatabase.getReference().child("Home").child("Shantipur Sarees").setValue(list);*/

    }

    public Maybe<DataSnapshot> fetchDhaniyaKali() {
        Query query = firebaseDatabase.getReference().child("Home").child("Dhaniya Kali").orderByKey();
        return RxFirebaseDatabase.observeSingleValueEvent(query);
    }

    public Maybe<DataSnapshot> fetchJaamdani() {
        Query query = firebaseDatabase.getReference().child("Home").child("Jaamdani").orderByKey();
        return RxFirebaseDatabase.observeSingleValueEvent(query);
    }

    public Maybe<DataSnapshot> fetchFulia() {
        Query query = firebaseDatabase.getReference().child("Home").child("Fulia").orderByKey();
        return RxFirebaseDatabase.observeSingleValueEvent(query);
    }

    public Maybe<DataSnapshot> fetchBahaCotton() {
        Query query = firebaseDatabase.getReference().child("Home").child("Baha Cotton").orderByKey();
        return RxFirebaseDatabase.observeSingleValueEvent(query);
    }

    public Maybe<DataSnapshot> fetchShantipur() {
        Query query = firebaseDatabase.getReference().child("Home").child("Shantipur").orderByKey();
        return RxFirebaseDatabase.observeSingleValueEvent(query);
    }

    // HomeMainFragment.. app has just started(1)
    synchronized public void fetchCount(@NonNull MainFragmentPresenterImpl mainFragmentPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        Boolean b;

        if (currentUser != null) {
            String uid = currentUser.getUid();
            Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        // Fetch Cart snapshot to update count in firebase as well as database and also to update codes in database
                        fetchCartSnapshot(uid, mainFragmentPresenterImpl);

                    } else {
                        mainFragmentPresenterImpl.NewUser();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    String details = databaseError.getDetails();
                    Log.d("KARAN", details);
                    mainFragmentPresenterImpl.loadingFailed(details);

                }
            });
        }
    }

    //Home MainFragment.. app has just started(2)
    synchronized private void fetchCartSnapshot(@NonNull String uid,
                                                @NonNull MainFragmentPresenterImpl mainFragmentPresenterImpl) {
        Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Cart").orderByKey();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d("KARAN", "dataSnapshot.exists() ");

                    String cart_count = CartModelWrapper.getCount(dataSnapshot);
                    List<String> cart_codes = CartModelWrapper.getCodes(dataSnapshot);

                    // Adding count of cart to firebase refeshing its value
                    firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue(cart_count);
                    fetchFavouritesSnapshot(uid, cart_count, cart_codes, mainFragmentPresenterImpl);

                } else {
                    // Adding count of cart to firebase refeshing its value to 0
                    firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue("0");
                    fetchFavouritesSnapshot(uid, "0", null, mainFragmentPresenterImpl);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                String details = databaseError.getDetails();
                Log.d("KARAN", details);
                mainFragmentPresenterImpl.loadingFailed(details);
            }
        });
    }

    //Home MainFragment.. app has just started(3)
    synchronized private void fetchFavouritesSnapshot(
            @NonNull String uid,
            @NonNull String cart_count,
            @Nullable List<String> cart_codes,
            @NonNull MainFragmentPresenterImpl mainFragmentPresenterImpl) {

        Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Favourites").orderByKey();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d("KARAN", "dataSnapshot.exists() ");

                    String favourites_count = CartModelWrapper.getCount(dataSnapshot);
                    List<String> favourites_codes = CartModelWrapper.getCodes(dataSnapshot);

                    //adding count of Favourites to firebase refeshing its value
                    firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Favourites").setValue(favourites_count);

                    mainFragmentPresenterImpl.OldUser(cart_count, cart_codes, favourites_count, favourites_codes);
                } else {
                    //adding count of Favourites to firebase refeshing its value to 0
                    firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Favourites").setValue("0");

                    mainFragmentPresenterImpl.OldUser(cart_count, cart_codes, "0", null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                String details = databaseError.getDetails();
                Log.d("KARAN", details);
                mainFragmentPresenterImpl.loadingFailed(details);
            }
        });
    }

    synchronized public void setUidAndCountForNewUser() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String uid = currentUser.getUid();
            CountModel countModel = new CountModel();
            countModel.setCart("0");
            countModel.setFavourites("0");
            firebaseDatabase.getReference().child("UId").child(uid).child("Count").setValue(countModel);
        }
    }

    synchronized public void fetchViews(@NonNull String[] hierarchy,
                                        @NonNull CollectionsPresenterImpl collectionsPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                int length = hierarchy.length;
                Query query = null;
                if (length == 2 && hierarchy[0] != null && hierarchy[1] != null)
                    query = firebaseDatabase.getReference().child("Saree Types").child(hierarchy[0]).child(hierarchy[1]).orderByValue();
                else if (length == 3 && hierarchy[0] != null && hierarchy[1] != null && hierarchy[2] != null) {
                    query = firebaseDatabase.getReference().child("Saree Types").child(hierarchy[0]).child(hierarchy[1]).child(hierarchy[2]).orderByValue();
                }

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            Log.d("KARAN", "loadViewsAdapter dataSnapshot.exists() ");
                            collectionsPresenterImpl.displayViews(ViewsModelWrapper.getCountModel(dataSnapshot));

                        } else {
                            Log.d("KARAN", "loadViewsAdapter !dataSnapshot.exists() ");
                            collectionsPresenterImpl.displayNoViewstoLoad();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        Log.d("KARAN", details);
                        collectionsPresenterImpl.loadingFailed(details, null, null);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doProcessOfAddingToCartOrFavourites(@NonNull ViewsModel fo4,
                                                    @Nullable String cart_or_favourites,
                                                    @NonNull CollectionsPresenterImpl collectionsPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;
                if (cart_or_favourites != null) {

                    //retrieve count
                    query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String count;
                            if (cart_or_favourites.equals("Cart"))
                                count = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived
                            else
                                count = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived

                            //now set new count=old count+1
                            updateAddingNewCountValue(fo4, cart_or_favourites, uid, count, collectionsPresenterImpl);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            String details = databaseError.getDetails();
                            Log.d("KARAN", details);
                            collectionsPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String details = e.getMessage().toString();
            collectionsPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
        }
    }

    synchronized private void updateAddingNewCountValue(
            ViewsModel fo4,
            String cart_or_favourites,
            String uid,
            String count,
            CollectionsPresenterImpl collectionsPresenterImpl) {
        String scount;
        if (count == null)
            scount = "1";
        else
            scount = String.valueOf((Integer.parseInt(count)) + 1);
        Log.d("AJIT", "count firebase in firestore " + scount);
        //set count value
        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child(cart_or_favourites).setValue(scount);
        //set value in cart_or_favourites
        firebaseDatabase.getReference().child("UId").child(uid).child(cart_or_favourites).child(fo4.getCode()).setValue(CartModelWrapper.getCartModel_fromViewsModel(fo4));

        collectionsPresenterImpl.countUpdated(fo4, "add", scount, cart_or_favourites);
    }

    public void doProcessOfRemovingFromCartOrFavourites(@NonNull ViewsModel fo4,
                                                        @Nullable String cart_or_favourites,
                                                        @NonNull CollectionsPresenterImpl collectionsPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query;

                if (cart_or_favourites != null) {

                    //retrieve
                    query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String count;
                            if (cart_or_favourites.equals("Cart"))
                                count = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived
                            else
                                count = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived

                            //now set new count=old count-1
                            updateRemovingNewCountValue(fo4, cart_or_favourites, uid, count, collectionsPresenterImpl);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            String details = databaseError.getDetails();
                            Log.d("KARAN", details);
                            collectionsPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String details = e.getMessage();
            collectionsPresenterImpl.loadingFailed(details, "count remove", cart_or_favourites);
        }

    }

    synchronized private void updateRemovingNewCountValue(ViewsModel fo4, String cart_or_favourites, String uid, String count, CollectionsPresenterImpl collectionsPresenterImpl) {
        String scount = null;
        if (count == null || count.equals("0"))
            scount = "0";
        else
            scount = String.valueOf((Integer.parseInt(count)) - 1);
        Log.d("AJIT", "count firebase in firestore " + scount);
        //set count
        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child(cart_or_favourites).setValue(scount);
        //remove
        DatabaseReference ref = firebaseDatabase.getReference().child("UId").child(uid).child(cart_or_favourites).child(fo4.getCode());
        ref.setValue(null);
        collectionsPresenterImpl.countUpdated(fo4, "remove", scount, cart_or_favourites);
    }

    //CART
    synchronized public void removefromCart(CartModel fo4, CartPresenterImpl cartPresenterImpl) {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;
                //remove
                DatabaseReference ref = firebaseDatabase.getReference().child("UId").child(uid).child("Cart").child(fo4.getCode());
                ref.setValue(null);

                //retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String count;
                        count = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived

                        //now set new count=old count-1
                        updateRemovingNewCountValue_CART(fo4, "Cart", uid, count, cartPresenterImpl);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        Log.d("KARAN", details);
                        cartPresenterImpl.loadingFailed(details, "count add", "Cart");
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //CART
    synchronized void updateRemovingNewCountValue_CART(CartModel fo4, String cart, String uid, String count, CartPresenterImpl cartPresenterImpl) {
        String scount = null;
        if (count == null || count.equals("0"))
            scount = "0";
        else
            scount = String.valueOf((Integer.valueOf(count)) - 1);
        Log.d("AJIT", "count firebase in firestore " + scount);
        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue(scount);
        cartPresenterImpl.countUpdatedCart(fo4, "remove", scount, "Cart");
    }

    //CART
    synchronized public void fetCartViews(CartPresenterImpl cartPresenterImpl) {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                //retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Cart").orderByKey();
                query.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists())
                            cartPresenterImpl.displayCartViews(CartModelWrapper.getCartModelList(dataSnapshot));
                        else
                            cartPresenterImpl.displayCartViews(null);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        Log.d("KARAN", details);
                        cartPresenterImpl.loadingFailed(details, "Something went wrong", "Cart");
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    //Order Pressed
    synchronized public void pressOrderPressed(@NonNull AddressActivityPresenterImpl addressActivityPresenter,
                                               @NonNull String a1,
                                               @NonNull String a2,
                                               @NonNull String a3,
                                               @NonNull String a4,
                                               @NonNull String a5,
                                               @NonNull String a6) {


        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                //retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Cart").orderByKey();
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    //orderId,  firstName,  secondname,  address,  pin,  city,  phone,  UId,  date,  quantity
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            DatabaseReference ref = firebaseDatabase.getReference().child("UId").child(uid).child("Order History").push();
                            String order_id = ref.getKey();//order id
                            String date = Utilities.getPresentDate(); //present date
                            String Totalquantity = CartModelWrapper.getTotalQuantity(dataSnapshot);
                            OrderHistoryModel orderHistoryModel = OrderHistoryModelWrapper.getOrderHistoryModell(order_id, a1, a2, a3, a4, a5, a6, uid, date, Totalquantity);
                            OrderDetailModel orderDetailModel = OrderDetailModelWrapper.getOrderDetailModel(order_id, a1, a2, a3, a4, a5, a6, uid, date, Totalquantity, CartModelWrapper.getCartModelList(dataSnapshot));

                            //set Order History
                            firebaseDatabase.getReference().child("UId").child(uid).child("Order History").child(order_id).setValue(orderHistoryModel);

                            //set Order Detail in Uid of customer
                            firebaseDatabase.getReference().child("UId").child(uid).child("Order Detail").child(order_id).setValue(orderDetailModel);

                            //make cart empty
                            firebaseDatabase.getReference().child("UId").child(uid).child("Cart").setValue(null);

                            //make cart count=0
                            firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue("0");

                            //get all cart codes
                            String[] codes = Utilities.getCodes(CartModelWrapper.getCartModelList(dataSnapshot));
                            addressActivityPresenter.orderPlacedSuccessfully(codes, order_id);
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        addressActivityPresenter.loadingFailed(details);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    //CART     (1)                                      //fo4,"Favourites",CartPresenterImpl cartPresenterImpl
    synchronized public void addtoFavourites(CartModel fo4, String cart_or_favourites, CartPresenterImpl cartPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                if (cart_or_favourites != null) {

                    //remove from Cart
                    DatabaseReference ref = firebaseDatabase.getReference().child("UId").child(uid).child("Cart").child(fo4.getCode());
                    ref.setValue(null);

                    //check if fo4 already present in Favourites
                    checkif_CartModelal_presentinFavourites(fo4, uid, cart_or_favourites, cartPresenterImpl);

                 /*   //set value in favourites
                    firebaseDatabase.getReference().child("UId").child(uid).child(cart_or_favourites).child(fo4.getCode()).setValue(fo4);

                    //retrieve count of Cart and favourites
                    query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String count1,count2;
                                count1 = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived
                                count2 = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived

                            //now set new count=old count+1
                            updateAddingNewCountValueforFavourites(fo4,cart_or_favourites, uid, count1,count2, cartPresenterImpl);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            String details = databaseError.getDetails();
                            Log.d("KARAN", details);
                            cartPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
                        }
                    });*/


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String details = e.getMessage().toString();
            cartPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
        }
    }

    //CART  (2)
    synchronized private void checkif_CartModelal_presentinFavourites(CartModel fo4, String uid, String cart_or_favourites, CartPresenterImpl cartPresenterImpl) {
        Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Favourites").orderByKey().equalTo(fo4.getCode());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()) {
                    continue_addtoFavourites(fo4, uid, cart_or_favourites, cartPresenterImpl, false);//setiing boolean to true
                } else {
                    continue_addtoFavourites(fo4, uid, cart_or_favourites, cartPresenterImpl, true);//setiing boolean to false

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                String details = databaseError.getDetails();
                cartPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
            }
        });


    }


    //CART   (3)
    synchronized private void continue_addtoFavourites(CartModel fo4, String uid, String cart_or_favourites, CartPresenterImpl cartPresenterImpl, Boolean b) {

        //set value in favourites
        firebaseDatabase.getReference().child("UId").child(uid).child(cart_or_favourites).child(fo4.getCode()).setValue(fo4);

        //retrieve count of Cart and favourites
        Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String count1, count2;
                count1 = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived
                count2 = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived

                //now set new count=old count+1
                updateAddingNewCountValueforFavourites(fo4, cart_or_favourites, uid, count1, count2, cartPresenterImpl, b);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                String details = databaseError.getDetails();
                cartPresenterImpl.loadingFailed(details, "count add", cart_or_favourites);
            }
        });

    }

    //CART  (4)
    synchronized private void updateAddingNewCountValueforFavourites(CartModel fo4, String cart_or_favourites, String uid, String count1, String count2, CartPresenterImpl cartPresenterImpl, Boolean b) {

        //set decreased value for cart
        String scount1 = null;
        if (count1 == null)
            scount1 = "0";
        else
            scount1 = String.valueOf((Integer.valueOf(count1)) - 1);
        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue(scount1);

        String scount2 = null;
        //set increased count for Favourites
        if (b) {
            if (count2 == null)
                scount2 = "1";
            else
                scount2 = String.valueOf((Integer.valueOf(count2)) + 1);

        } else//set same count of Favourites
        {
            if (count2 == null)
                scount2 = "0";
            else
                scount2 = count2;
        }

        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Favourites").setValue(scount2);


        cartPresenterImpl.countUpdatedForFavouritesAndCart(fo4, "add and remove", scount1, scount2, cart_or_favourites);
    }


    //WISHLIST
    synchronized public void removefromWishlist(CartModel fo4, WishlistPresenterImpl wishlistPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;
                //remove
                DatabaseReference ref = firebaseDatabase.getReference().child("UId").child(uid).child("Favourites").child(fo4.getCode());
                ref.setValue(null);

                //retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String count;
                        count = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived

                        //now set new count=old count-1
                        updateRemovingNewCountValue_FAVOURITES(fo4, "Favourites", uid, count, wishlistPresenterImpl);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        wishlistPresenterImpl.loadingFailed(details, "count remove", "Cart");
                    }
                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //WISHLIST
    synchronized void updateRemovingNewCountValue_FAVOURITES(CartModel fo4, String cart, String uid, String count, WishlistPresenterImpl wishlistPresenterImpl) {
        String scount = null;
        if (count == null || count.equals("0"))
            scount = "0";
        else
            scount = String.valueOf((Integer.valueOf(count)) - 1);
        //Log.d("AJIT","count firebase in firestore "+scount);
        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Favourites").setValue(scount);
        wishlistPresenterImpl.countUpdatedWISHLIST(fo4, "remove", scount, "Favourites");
    }

    //WISHLIST
    synchronized public void fetchFavouriteViews(WishlistPresenterImpl wishlistPresenter) {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                //retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Favourites").orderByKey();
                query.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists())
                            wishlistPresenter.displayFavouriteViews(CartModelWrapper.getCartModelList(dataSnapshot));
                        else
                            wishlistPresenter.displayFavouriteViews(null);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        wishlistPresenter.loadingFailed(details, "Something went wrong", "Cart");
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    //WISHLIST
    synchronized public void addtoCart(CartModel fo4, String cart_or_favourites, WishlistPresenterImpl wishlistPresenter) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                if (cart_or_favourites != null) {

                    //remove from Favourites
                    DatabaseReference ref = firebaseDatabase.getReference().child("UId").child(uid).child("Favourites").child(fo4.getCode());
                    ref.setValue(null);

                    //check if fo4 already present in Cart
                    checkiIfCartModelPresentinCart(fo4, uid, cart_or_favourites, wishlistPresenter);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String details = e.getMessage().toString();
            wishlistPresenter.loadingFailed(details, "count add", cart_or_favourites);
        }
    }

    synchronized private void checkiIfCartModelPresentinCart(
            @NonNull CartModel fo4,
            @NonNull String uid,
            @NonNull String cart_or_favourites,
            @NonNull WishlistPresenterImpl wishlistPresenter) {
        Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Cart").orderByKey().equalTo(fo4.getCode());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                continue_addtoCart(fo4, uid, cart_or_favourites, wishlistPresenter, !dataSnapshot.exists()); // Setting boolean to true/false
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                String details = databaseError.getDetails();
                wishlistPresenter.loadingFailed(details, "count add", cart_or_favourites);
            }
        });
    }

    synchronized private void continue_addtoCart(@NonNull CartModel fo4,
                                                 @NonNull String uid,
                                                 @NonNull String cart_or_favourites,
                                                 @NonNull WishlistPresenterImpl wishlistPresenter, boolean b) {
        //set value in Cart
        firebaseDatabase.getReference().child("UId").child(uid).child(cart_or_favourites).child(fo4.getCode()).setValue(fo4);

        //retrieve count of Cart and Favourites
        Query query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String count1, count2;
                count1 = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived for Favourites
                count2 = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived for Cart

                //now set new count=old count+1
                updateAddingNewCountValueForCart(fo4, cart_or_favourites, uid, count1, count2, wishlistPresenter, b);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                String details = databaseError.getDetails();
                wishlistPresenter.loadingFailed(details, "count add", cart_or_favourites);
            }
        });

    }

    synchronized private void updateAddingNewCountValueForCart(@NonNull CartModel fo4,
                                                               @NonNull String cart_or_favourites,
                                                               @NonNull String uid,
                                                               @Nullable String count1,
                                                               @Nullable String count2,
                                                               @NonNull WishlistPresenterImpl wishlistPresenter,
                                                               boolean b) {

        //set decreased value for Favourites
        String sCount1;
        if (count1 == null)
            sCount1 = "0";
        else
            sCount1 = String.valueOf((Integer.parseInt(count1)) - 1);
        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Favourites").setValue(sCount1);

        String scount2 = null;
        //set increased count for Cart
        if (b) {
            if (count2 == null)
                scount2 = "1";
            else
                scount2 = String.valueOf((Integer.parseInt(count2)) + 1);

        } else//set same count of Cart
        {
            if (count2 == null)
                scount2 = "0";
            else
                scount2 = count2;
        }

        firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue(scount2);

        wishlistPresenter.countUpdatedforFavourites_andCart(fo4, "add and remove", sCount1, scount2, cart_or_favourites);
    }

    //ORDER HISTORY
    synchronized public void fetchOrdersHistory(@NonNull OrderHistoryPresenterImpl orderHistoryPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                //retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Order History").orderByKey();
                query.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists())
                            orderHistoryPresenterImpl.displayOrderHistory(OrderHistoryModelWrapper.getOrderHistoryModeList(dataSnapshot));
                        else
                            orderHistoryPresenterImpl.displayOrderHistory(null);

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        orderHistoryPresenterImpl.loadingFailed(details);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    //ORDER DETAIL
    synchronized public void fetchOrdersDetail(@NonNull String order_id,
                                               @NonNull OrderDetailPresenterImpl orderDetailPresenter) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query;

                // Retrieve
                query = firebaseDatabase.getReference().child("UId").child(uid).child("Order Detail").orderByKey().equalTo(order_id);
                query.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        orderDetailPresenter.displayOrderDeatils(OrderDetailModelWrapper.getOrderDetailModeltoView(dataSnapshot));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        String details = databaseError.getDetails();
                        orderDetailPresenter.loadingFailed(details);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //DETAILS  (1)
    synchronized public void saveToFavourites(@NonNull CartModel fo4,
                                              @Nullable String favourites,
                                              @NonNull DetailsPresenterImpl detailsPresenterImpl) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query = null;

                if (favourites != null) {

                    // Set value in favourites
                    firebaseDatabase.getReference().child("UId").child(uid).child(favourites).child(fo4.getCode()).setValue(fo4);
                    query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String count1, count2;
                            count1 = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();// value retrieved
                            count2 = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites(); // value retrieved

                            // Now set new count=old count+1
                            updateAddingCountValueForFavourites(fo4, "Favourites", uid, count1, count2, detailsPresenterImpl);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            String details = databaseError.getDetails();
                            detailsPresenterImpl.loadingFailed(details, "count add", favourites);
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String details = e.getMessage();
            detailsPresenterImpl.loadingFailed(details, "count add", "Favourites");
        }
    }


    //DETAILS  (2)
    synchronized private void updateAddingCountValueForFavourites(@NonNull CartModel fo4,
                                                                  @Nullable String cart_or_favourites,
                                                                  @NonNull String uid,
                                                                  @Nullable String count1,
                                                                  @Nullable String count2,
                                                                  @NonNull DetailsPresenterImpl detailsPresenterImpl) {
        String scount2 = null;
        String scount1 = null;
        if (cart_or_favourites != null) {
            if (cart_or_favourites.equals("Favourites")) {
                //set increased count for Favourites

                if (count2 == null)
                    scount2 = "1";
                else
                    scount2 = String.valueOf((Integer.parseInt(count2)) + 1);

                firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Favourites").setValue(scount2);
            } else {
                //set increased count for Cart

                if (count1 == null)
                    scount1 = "1";
                else
                    scount1 = String.valueOf((Integer.parseInt(count1)) + 1);

                firebaseDatabase.getReference().child("UId").child(uid).child("Count").child("Cart").setValue(scount1);
            }
        }


        detailsPresenterImpl.countUpdatedforFavourites_andCart(fo4, "add", scount1, scount2, cart_or_favourites);
    }


    //DETAILS  (1)
    synchronized public void saveToCART(@NonNull CartModel fo4,
                                        @Nullable String cart,
                                        @NonNull DetailsPresenterImpl detailsPresenterImpl) {

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        try {
            if (currentUser != null) {
                String uid = currentUser.getUid();
                Query query;

                if (cart != null) {

                    // Set value in Cart
                    firebaseDatabase.getReference().child("UId").child(uid).child("Cart").child(fo4.getCode()).setValue(fo4);
                    query = firebaseDatabase.getReference().child("UId").child(uid).child("Count").orderByKey();
                    query.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String count1, count2;
                            count1 = (CountModelWrapper.getCountModel(dataSnapshot)).getCart();//value retrived
                            count2 = (CountModelWrapper.getCountModel(dataSnapshot)).getFavourites();//value retrived

                            //now set new count=old count+1
                            updateAddingCountValueForFavourites(fo4, "Cart", uid, count1, count2, detailsPresenterImpl);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            String details = databaseError.getDetails();
                            detailsPresenterImpl.loadingFailed(details, "count add", "Cart");
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String details = e.getMessage().toString();
            detailsPresenterImpl.loadingFailed(details, "count add", "Cart");
        }
    }

    //CART
    synchronized public void updateSpinner(String item, CartModel fo4, CartPresenterImpl cartPresenter) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        if (currentUser != null) {
            String uid = currentUser.getUid();
            firebaseDatabase.getReference().child("UId").child(uid).child("Cart").child(fo4.getCode()).child("quantity").setValue(item);
            cartPresenter.spinnerUpdated();
        }
    }

    //Logout
    public void logout() {
        final DatabaseReference ref = firebaseDatabase.getReference().child("Saree");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (flag == 1) {
                    FirebaseAuth mAuth;
                    mAuth = FirebaseAuth.getInstance();
                    mAuth.signOut();
                    LoginManager.getInstance().logOut();
                    Intent intent = new Intent(context, SplashActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);
                    flag = 0;
                } else {
                    flag = 1;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null)
            accessToken.getExpires().setTime(5000);
    }
}
