package com.karangupta.durgahandloom;

/**
 * @author arun
 */
public class Constants {

    public static final String HIERCHY = "HIERCHY";
    public static final String CHIP_MODEL_LIST = "CHIP_MODEL_LIST";

    public static final String ID = "ID";
    public static final String CODE = "CODE";
    public static final String QUANTITY = "QUANTITY";
    public static final String POSITION = "POSITION";
    public static String URLs = "URLs";
    public static String URL = "URL";
    public static String SERIES_NAME = "SERIES_NAME";


}
