package com.karangupta.durgahandloom.local_storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by karangupta on 27/03/18.
 */

public class LocalStorageStoreInteractorImpl implements LocalStorageStoreInteractor {
   @NonNull private final LocalStorageStore localStorageStore;

     LocalStorageStoreInteractorImpl(@NonNull LocalStorageStore localStorageStore) {
        this.localStorageStore = localStorageStore;
    }

    @Override
    public void setCountValueToSharedPrefs(@NonNull String cart_or_favourites_count,
                                           @NonNull String cart_or_favourite) {
        localStorageStore.setCountValueToSharedPrefs(cart_or_favourites_count, cart_or_favourite);
    }

    @Override
    public String[] getCountValueFromSharedPrefs() {
        return localStorageStore.getCountValueFromSharedPrefs();

    }

    @Override
    public void setCodeInSharedPrefs(@NonNull String code,
                                     @Nullable String add_or_remove,
                                     @Nullable String cart_or_favourites) {
        localStorageStore.setCodeInSharedPrefs(code,add_or_remove,cart_or_favourites);
    }

    @Override
    public boolean addedToCart(@NonNull String code) {
        return localStorageStore.addedToCart(code);
    }

    @Override
    public boolean addedToFavourites(@NonNull String code) {
        return localStorageStore.addedToFavourites(code);
    }

    @Override
    public void remove_all_SharedPreferences() {
        localStorageStore.remove_all_SharedPreferences();
    }
}
