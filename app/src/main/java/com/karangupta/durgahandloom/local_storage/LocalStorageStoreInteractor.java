package com.karangupta.durgahandloom.local_storage;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by karangupta on 27/03/18.
 */
public interface LocalStorageStoreInteractor {

    void setCountValueToSharedPrefs(@NonNull String cart_or_favourites_count,
                                    @NonNull String cart_or_favourite);

    String[] getCountValueFromSharedPrefs();

    void setCodeInSharedPrefs(@NonNull String code,
                              @Nullable String add_or_remove,
                              @Nullable String cart_or_favourites);

    boolean addedToCart(@NonNull String code);

    boolean addedToFavourites(@NonNull String code);

    void remove_all_SharedPreferences();
}
