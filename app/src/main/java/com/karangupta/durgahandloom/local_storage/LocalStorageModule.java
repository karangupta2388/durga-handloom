package com.karangupta.durgahandloom.local_storage;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 27/03/18.
 */
@Module
public class LocalStorageModule {
    @Provides
    @Singleton
    LocalStorageStoreInteractor provideLocalStorageInteractor(@NonNull LocalStorageStore store) {
        return new LocalStorageStoreInteractorImpl(store);
    }
}
