package com.karangupta.durgahandloom.local_storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.karangupta.durgahandloom.util.database_model.DatabaseInfo;

import javax.inject.Inject;
import javax.inject.Singleton;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;

import io.reactivex.annotations.Nullable;

/**
 * Created by karangupta on 27/03/18.
 */
@Singleton
public class LocalStorageStore {
    private static final String FAVOURITES = "Favorites";
    private static final String CART = "Cart";

    @NonNull private final SharedPreferences pref_favourite, pref_cart;
    @NonNull private final Context context;
    @Nullable private SharedPreferences sharedPreferences;

    private long id = 0;

    @Inject
    LocalStorageStore(@NonNull Context context) {
        this.context = context;
        pref_favourite = context.getApplicationContext().getSharedPreferences(FAVOURITES, Context.MODE_PRIVATE);
        pref_cart = context.getApplicationContext().getSharedPreferences(CART, Context.MODE_PRIVATE);
        ActiveAndroid.initialize(context);
    }

    synchronized void setCountValueToSharedPrefs(@NonNull String cart_or_favourites_count,
                                                 @Nullable String cart_or_favourites) {
        if (cart_or_favourites != null) {
            if (cart_or_favourites.equals("Cart"))
                pref_cart.edit().putString(CART, cart_or_favourites_count).apply();
            else if (cart_or_favourites.equals("Favourites"))
                pref_favourite.edit().putString(FAVOURITES, cart_or_favourites_count).apply();
        }
    }

    String[] getCountValueFromSharedPrefs() {
        String[] count = new String[2];
        count[0] = pref_favourite.getString(FAVOURITES, "0");
        count[1] = pref_cart.getString(CART, "0");
        return count;
    }

    public void setCodeInSharedPrefs(
            @NonNull String code,
            @Nullable String add_or_remove,
            @Nullable String cart_or_favourites) {
        int i = 0;
        if (cart_or_favourites != null) {
            if (cart_or_favourites.equals("Cart")) {
                i = 1;
                sharedPreferences = getDefaultSharedPreferences(context.getApplicationContext());
            } else {
                i = 2;
                sharedPreferences = getDefaultSharedPreferences(context.getApplicationContext());
            }
        }
        if (add_or_remove != null) {
            if (add_or_remove.equals("add") && sharedPreferences != null) {
                if (i == 1) {
                    sharedPreferences.edit().putString(code + CART, "1").apply();
                } else {
                    sharedPreferences.edit().putString(code + FAVOURITES, "1").apply();
                }
            } else if (add_or_remove.equals("remove") && sharedPreferences != null) {
                if (i == 1) {
                    sharedPreferences.edit().remove(code + CART).apply();
                } else {
                    sharedPreferences.edit().remove(code + FAVOURITES).apply();
                }
            }
        }
    }

    public boolean addedToCart(@NonNull String code) {
        sharedPreferences = getDefaultSharedPreferences(context.getApplicationContext());
        return sharedPreferences.getString(code + CART, "0").equals("1");
    }

    public boolean addedToFavourites(@NonNull String code) {
        sharedPreferences = getDefaultSharedPreferences(context.getApplicationContext());
        return sharedPreferences.getString(code + FAVOURITES, "0").equals("1");
    }

    public void remove_all_SharedPreferences() {
        getDefaultSharedPreferences(context).edit().clear().apply();
    }

    // Query
    public DatabaseInfo retrieveFromDatabase() {
        return new Select().from(DatabaseInfo.class).executeSingle();
    }

    // Insert
    public void storeInDatabase(@Nullable String a1,
                                @Nullable String a2,
                                @Nullable String a3,
                                @Nullable String a4,
                                @Nullable String a5,
                                @Nullable String a6) {
        id = new DatabaseInfo(a1, a2, a3, a4, a5, a6).save();
    }

    // Update
    public void updateDatabase(String a1, String a2, String a3, String a4, String a5, String a6) {
        DatabaseInfo info = new Select().from(DatabaseInfo.class).where("id = ?", id).executeSingle();
        info.setFirstname(a1);
        info.setSecondname(a2);
        info.setAddress(a3);
        info.setPin(a4);
        info.setCity(a5);
        info.setPhone(a6);
        info.save();
    }

    // Delete
    public void removeFromDatabase() {
        new Delete().from(DatabaseInfo.class).execute();
    }
}
