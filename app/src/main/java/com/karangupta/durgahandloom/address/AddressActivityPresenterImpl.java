package com.karangupta.durgahandloom.address;

import com.karangupta.durgahandloom.local_storage.LocalStorageStore;
import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.database_model.DatabaseInfo;

import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 04/04/18.
 */

public class AddressActivityPresenterImpl implements AddressActivityPresenter {

    @NonNull private final ConnectionDetector connectionDetector;
    @NonNull private final LocalStorageStore localStorageStore;
    @NonNull private final AddressActivityInteractor addressActivityInteractor;
    private AddressActivityView view;

    public AddressActivityPresenterImpl(@NonNull ConnectionDetector connectionDetector,
                                        @NonNull AddressActivityInteractor addressActivityInteractor,
                                        @NonNull LocalStorageStore localStorageStore) {
        this.addressActivityInteractor = addressActivityInteractor;
        this.connectionDetector = connectionDetector;
        this.localStorageStore = localStorageStore;
    }

    @Override
    public void setView(@NonNull AddressActivity cart) {
        this.view = cart;
    }

    @Override
    public void destroy() {
        view = null;
    }

    @Override
    public void retrieveFromDatabase() {
        DatabaseInfo info = localStorageStore.retrieveFromDatabase();
        if (isViewAttached()) {
            if (info == null) {
                view.displayDatababseEmpty();
            } else
                view.displayDatababseNotEmpty(info);
        }
    }

    @Override
    public void removeFromDatabase() {
        localStorageStore.removeFromDatabase();
    }

    @Override
    public void storeInDatabase(@NonNull String a1,
                                @NonNull String a2,
                                @NonNull String a3,
                                @NonNull String a4,
                                @NonNull String a5,
                                @NonNull String a6) {
        localStorageStore.storeInDatabase(a1, a2, a3, a4, a5, a6);
    }

    @Override
    public void pressOrderPressed(@NonNull String a1,
                                  @NonNull String a2,
                                  @NonNull String a3,
                                  @NonNull String a4,
                                  @NonNull String a5,
                                  @NonNull String a6) {
        if (connectionDetector.isConnectingToInternet()) {
            view.startLoadingAnim();
            addressActivityInteractor.pressOrderPressed(this, a1, a2, a3, a4, a5, a6);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    public void loadingFailed(String message) {
        if (isViewAttached())
            view.loadingFailed(message);
    }

    private boolean isViewAttached() {
        return view != null;
    }

    public void orderPlacedSuccessfully(String[] codes, String order_id) {
        // Removing codes from local storage
        for (int i = 0; i < codes.length; i++) {
            localStorageStore.setCodeInSharedPrefs(codes[i], "remove", "Cart");
        }
        if (isViewAttached()) {
            view.orderPlacedSuccessfully(order_id);
        }
    }
}
