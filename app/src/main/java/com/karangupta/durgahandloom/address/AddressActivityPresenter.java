package com.karangupta.durgahandloom.address;

import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 04/04/18.
 */
interface AddressActivityPresenter {

    void setView(@NonNull AddressActivity cart);

    void destroy();

    void retrieveFromDatabase();

    void removeFromDatabase();

    void storeInDatabase(@NonNull String a1,
                         @NonNull String a2,
                         @NonNull String a3,
                         @NonNull String a4,
                         @NonNull String a5,
                         @NonNull String a6);

    void pressOrderPressed(@NonNull String a1,
                           @NonNull String a2,
                           @NonNull String a3,
                           @NonNull String a4,
                           @NonNull String a5,
                           @NonNull String a6);
}
