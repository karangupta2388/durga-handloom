package com.karangupta.durgahandloom.address;

import com.karangupta.durgahandloom.util.database_model.DatabaseInfo;

/**
 * Created by karangupta on 04/04/18.
 */

public interface AddressActivityView {

    void loadingFailed(String message);

    void loadingStarted();

    void stopLoading();

    void startLoadingAnim();

    void stopLoadingAnim();

    void displayDatababseEmpty();

    void displayDatababseNotEmpty(DatabaseInfo info);

    void orderPlacedSuccessfully(String order_id);

    void noInternet();
}
