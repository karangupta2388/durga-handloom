package com.karangupta.durgahandloom.address;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 04/04/18.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AddressScope {
}