package com.karangupta.durgahandloom.address;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.order_placed_successful.OrderPlacedSuccessful;
import com.karangupta.durgahandloom.util.database_model.DatabaseInfo;
import com.wang.avi.AVLoadingIndicatorView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customfonts.MyEditText;
import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 03/04/18.
 */
public class AddressActivity extends AppCompatActivity implements AddressActivityView {

    @Inject
    AddressActivityPresenter addressActivityPresenter;

    @BindView(R.id.firstname)
    MyEditText firstname;

    @BindView(R.id.secondname)
    MyEditText secondname;

    @BindView(R.id.address)
    MyEditText address;

    @BindView(R.id.pin)
    MyEditText pin;

    @BindView(R.id.city)
    MyEditText city;

    @BindView(R.id.phoneno)
    MyEditText phoneno;

    @BindView(R.id.switch1)
    Switch switch1;

    @BindView(R.id.place_order1)
    TextView place_order1;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.loading_frame_layout)
    FrameLayout loading_frame_layout;

    @BindView(R.id.loadingAVI)
    AVLoadingIndicatorView loadingAVI;

    private Unbinder unbinder;

    // Required empty public constructor
    public AddressActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.address_activity);
        unbinder = ButterKnife.bind(this);
        ((BaseApplication) getApplication()).createAddressActivityComponent().inject(this);
        addressActivityPresenter.setView(AddressActivity.this);
        addressActivityPresenter.retrieveFromDatabase();

        place_order1.setOnClickListener(view -> {
            String a1 = firstname.getText().toString();
            String a2 = secondname.getText().toString();
            String a3 = address.getText().toString();
            String a4 = pin.getText().toString();
            String a5 = city.getText().toString();
            String a6 = phoneno.getText().toString();
            if (!TextUtils.isEmpty(a1)
                && !TextUtils.isEmpty(a2)
                && !TextUtils.isEmpty(a3)
                && !TextUtils.isEmpty(a4)
                && !TextUtils.isEmpty(a5)
                && !TextUtils.isEmpty(a6)) {
                addressActivityPresenter.pressOrderPressed(a1, a2, a3, a4, a5, a6);
            } else {
                Snackbar.make(place_order1, "Enter all fields", Snackbar.LENGTH_LONG).show();
            }
        });
        switch1.setOnCheckedChangeListener((buttonView, isChecked) -> {
            String a1 = firstname.getText().toString();
            String a2 = secondname.getText().toString();
            String a3 = address.getText().toString();
            String a4 = pin.getText().toString();
            String a5 = city.getText().toString();
            String a6 = phoneno.getText().toString();

            if (switch1.isChecked()) {
                if (!TextUtils.isEmpty(a1)
                    && !TextUtils.isEmpty(a2)
                    && !TextUtils.isEmpty(a3)
                    && !TextUtils.isEmpty(a4)
                    && !TextUtils.isEmpty(a5)
                    && !TextUtils.isEmpty(a6)) {
                    addressActivityPresenter.storeInDatabase(a1, a2, a3, a4, a5, a6);
                } else {
                    switch1.setChecked(false);
                    Snackbar.make(place_order1, "Enter all fields", Snackbar.LENGTH_LONG).show();
                }
            } else {
                addressActivityPresenter.removeFromDatabase();
            }
        });
    }


    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void startLoadingAnim() {
        loadingAVI.show();
        loading_frame_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoadingAnim() {
        loadingAVI.hide();
        loading_frame_layout.setVisibility(View.GONE);
    }

    @Override
    public void displayDatababseEmpty() {
        switch1.setChecked(false);
    }

    @Override
    public void displayDatababseNotEmpty(@NonNull DatabaseInfo info) {
        switch1.setChecked(true);
        firstname.setText((CharSequence) info.getFirstname());

        secondname.setText((CharSequence) info.getSecondname());

        address.setText((CharSequence) info.getAddress());

        pin.setText((CharSequence) info.getPin());

        city.setText((CharSequence) info.getCity());

        phoneno.setText((CharSequence) info.getPhone());
    }

    @Override
    public void orderPlacedSuccessfully(@NonNull String order_id) {
        stopLoadingAnim();
        Toast.makeText(this, "order Placed", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(AddressActivity.this, OrderPlacedSuccessful.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("order_id", order_id);
        startActivity(intent);
    }

    @Override
    public void noInternet() {
        Snackbar.make(place_order1, R.string.no_internet, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void loadingFailed(@NonNull String errorMessage) {
        stopLoadingAnim();
        progress_bar.setVisibility(View.GONE);
        Snackbar.make(place_order1, "Something went wrong.", Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        addressActivityPresenter.destroy();
        unbinder.unbind();
        ((BaseApplication) getApplication()).releaseAddressActivityComponent();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

