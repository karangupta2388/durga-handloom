package com.karangupta.durgahandloom.address;

import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 04/04/18.
 */
interface AddressActivityInteractor {

    void pressOrderPressed(@NonNull AddressActivityPresenterImpl addressActivityPresenter,
                           @NonNull String a1,
                           @NonNull String a2,
                           @NonNull String a3,
                           @NonNull String a4,
                           @NonNull String a5,
                           @NonNull String a6);
}
