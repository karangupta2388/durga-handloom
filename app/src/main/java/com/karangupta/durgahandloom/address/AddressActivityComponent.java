package com.karangupta.durgahandloom.address;

import dagger.Subcomponent;

/**
 * Created by karangupta on 04/04/18.
 */
@AddressScope
@Subcomponent(modules = {AddressActivityModule.class})
public interface AddressActivityComponent {
    void inject(AddressActivity target);
}