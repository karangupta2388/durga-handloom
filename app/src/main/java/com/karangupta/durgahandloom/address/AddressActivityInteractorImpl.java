package com.karangupta.durgahandloom.address;

import com.karangupta.durgahandloom.firebase.FirebaseStore;

import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 04/04/18.
 */

public class AddressActivityInteractorImpl implements AddressActivityInteractor {

    @NonNull private final FirebaseStore firebaseStore;

    public AddressActivityInteractorImpl(@NonNull FirebaseStore firebaseStore) {
        this.firebaseStore = firebaseStore;
    }

    @Override
    public void pressOrderPressed(@NonNull AddressActivityPresenterImpl addressActivityPresenter,
                                  @NonNull String a1,
                                  @NonNull String a2,
                                  @NonNull String a3,
                                  @NonNull String a4,
                                  @NonNull String a5,
                                  @NonNull String a6) {
        synchronized (firebaseStore) {
            firebaseStore.pressOrderPressed(addressActivityPresenter, a1, a2, a3, a4, a5, a6);
        }
    }
}
