package com.karangupta.durgahandloom.address;


import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.local_storage.LocalStorageStore;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 04/04/18.
 */
@Module
public class AddressActivityModule {

    @Provides
    AddressActivityInteractor provideAddressInteractor(
            @NonNull FirebaseStore firebaseStore) {
        return new AddressActivityInteractorImpl(firebaseStore);
    }

    @Provides
    AddressActivityPresenter provideAddressPresenter(@NonNull ConnectionDetector connectionDetector,
                                                     @NonNull AddressActivityInteractor cartInteractor,
                                                     @NonNull LocalStorageStore localStorageStore) {
        return new AddressActivityPresenterImpl(connectionDetector, cartInteractor, localStorageStore);
    }

}
