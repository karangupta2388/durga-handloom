package com.karangupta.durgahandloom;

import com.karangupta.durgahandloom.address.AddressActivityComponent;
import com.karangupta.durgahandloom.address.AddressActivityModule;
import com.karangupta.durgahandloom.cart.CartComponent;
import com.karangupta.durgahandloom.cart.CartModule;
import com.karangupta.durgahandloom.collections.CollectionsComponent;
import com.karangupta.durgahandloom.collections.CollectionsModule;
import com.karangupta.durgahandloom.details.DetailsComponent;
import com.karangupta.durgahandloom.details.DetailsModule;
import com.karangupta.durgahandloom.firebase.FirebaseeModule;
import com.karangupta.durgahandloom.local_storage.LocalStorageModule;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentComponent;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentModule;
import com.karangupta.durgahandloom.main_activity.order_history.OrderHistoryComponent;
import com.karangupta.durgahandloom.main_activity.order_history.OrderHistoryModule;
import com.karangupta.durgahandloom.order_detail.OrderDetailComponent;
import com.karangupta.durgahandloom.order_detail.OrderDetailModule;
import com.karangupta.durgahandloom.wishlist.WishlistComponent;
import com.karangupta.durgahandloom.wishlist.WishlistModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        FirebaseeModule.class,
        LocalStorageModule.class})
public interface AppComponent {

    MainFragmentComponent plus(MainFragmentModule mainFragmentModule);

    CollectionsComponent plus(CollectionsModule collectionsModule);

    CartComponent plus(CartModule cartModule);

    AddressActivityComponent plus(AddressActivityModule addressActivityModule);

    WishlistComponent plus(WishlistModule wishlistModule);

    OrderHistoryComponent plus(OrderHistoryModule orderHistoryModule);

    OrderDetailComponent plus(OrderDetailModule orderDetailModule);

    DetailsComponent plus(DetailsModule detailsModule);
}
