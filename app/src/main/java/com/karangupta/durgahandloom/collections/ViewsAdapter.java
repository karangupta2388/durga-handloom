package com.karangupta.durgahandloom.collections;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karangupta on 31/03/18.
 */

class ViewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @NonNull final List<ViewsModel> list;
    @NonNull final CollectionsActivity collectionsActivity;
    @Nullable private Context context;

    public ViewsAdapter(@NonNull List<ViewsModel> viewsModel_list,
                        @NonNull CollectionsActivity collectionsActivity) {
        this.collectionsActivity = collectionsActivity;
        this.list = viewsModel_list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return (new ViewsAdapter.RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.collection_views, parent, false)));

    }

    public int getItemCount() {

        return (list.size());
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((ViewsAdapter.RowController) holder).bindModel(list.get(position), position);
    }

    private class RowController extends RecyclerView.ViewHolder {
        ImageView image_view_show_card = null;
        TextView title = null;
        TextView sub_title = null;
        LinearLayout add_to_cart;
        LinearLayout add_to_favourite;
        ImageView bag;
        ImageView favourites;
        ViewsModel fo4;
        View row;

        private RowController(View row) {
            super(row);
            this.row = row;
            this.title = ((TextView) row.findViewById(R.id.title));
            this.sub_title = ((TextView) row.findViewById(R.id.sub_title));
            this.add_to_cart = ((LinearLayout) row.findViewById(R.id.add_to_cart));
            this.add_to_favourite = ((LinearLayout) row.findViewById(R.id.add_to_favourite));
            this.image_view_show_card = ((ImageView) row.findViewById(R.id.image_view_show_card));
            this.bag = ((ImageView) row.findViewById(R.id.bag));
            this.favourites = ((ImageView) row.findViewById(R.id.favourites));
            this.image_view_show_card.getLayoutParams().width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);
            this.image_view_show_card.getLayoutParams().height = (int) ((context.getResources().getDisplayMetrics().widthPixels * 0.96) / 1.40);
        }

        private void bindModel(final ViewsModel fo4, int position) {
            this.fo4 = fo4;
            int px1 = (int) Utilities.convertDpToPixel(0.5f, context);//0.5 dp to pixel
            int px2 = (int) Utilities.convertDpToPixel(2, context);//1 dp to pixel
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) row.getLayoutParams();

            if (position % 2 == 0) {
                params.rightMargin = px1;
                params.topMargin = px1;
                params.bottomMargin = px1;
            } else {
                params.leftMargin = px1;
                params.topMargin = px1;
                params.bottomMargin = px1;

            }

            if (title != null && sub_title != null) {
                if (fo4.getSeries_Name() != null && !fo4.getSeries_Name().equals("")) {
                    String stitle = fo4.getSeries_Name().substring(0, fo4.getSeries_Name().indexOf(' '));
                    String ssub_title = fo4.getSeries_Name().substring(fo4.getSeries_Name().indexOf(' ') + 1, fo4.getSeries_Name().length());
                    StringBuilder sb = new StringBuilder(ssub_title);
                    try {
                        String ssub1 = ssub_title.substring(0, ssub_title.indexOf(' ') + 1);
                        String ssub2 = ssub_title.substring(ssub_title.indexOf(' ') + 1, sb.length());
                        ssub_title = ssub1 + "\n" + ssub2;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    title.setText(stitle);
                    sub_title.setText(ssub_title);
                    title.setVisibility(View.VISIBLE);
                    sub_title.setVisibility(View.VISIBLE);
                } else {
                    title.setVisibility(View.GONE);
                    sub_title.setVisibility(View.GONE);
                }

            }

            if (image_view_show_card != null) {
                image_view_show_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        collectionsActivity.imageClicked(fo4, image_view_show_card);
                    }
                });
            }


            if (image_view_show_card != null) {

                image_view_show_card.setImageResource(android.R.color.transparent);
                image_view_show_card.setVisibility(View.VISIBLE);
                ArrayList<String> list = Utilities.geEachUrl2(fo4.getUrl());

                Glide.with(context).load(list.get(0))
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(image_view_show_card);


            } else {
                image_view_show_card.setImageResource(android.R.color.transparent);
                image_view_show_card.setVisibility(View.INVISIBLE);
            }

            if (bag != null) {
                if (collectionsActivity.addedtoBag(fo4.getCode())) {
                    bag.setImageResource(R.drawable.add_to_cart5);
                } else {
                    bag.setImageResource(R.drawable.add_to_cart1);

                }

            }
            if (favourites != null) {
                if (collectionsActivity.addedtoFavourites(fo4.getCode())) {
                    favourites.setImageResource(R.drawable.add_to_favourite5);
                } else {
                    favourites.setImageResource(R.drawable.add_to_favourite1);

                }
            }

            add_to_cart.setOnClickListener(view -> {
                if (bag != null && !collectionsActivity.getLock()) {
                    Log.d("DADDY", "add to cart1");
                    if (bag.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.add_to_cart1).getConstantState()) {
                        Log.d("DADDY", "add to cart2");

                        bag.setImageDrawable(null);
                        bag.setImageResource(R.drawable.add_to_cart5);
                        //  Animations.onTapAnimation(bag,1,context);
                        if (!collectionsActivity.addedtoBag(fo4.getCode())) {
                            collectionsActivity.addToCart_orFavourites(fo4, bag, "Cart");
                        }

                    } else if (bag.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.add_to_cart5).getConstantState()) {
                        bag.setImageDrawable(null);
                        bag.setImageResource(R.drawable.add_to_cart1);

                        //Animations.onTapAnimation(bag,2,context);

                        //if its in bag then only remove
                        if (collectionsActivity.addedtoBag(fo4.getCode())) {
                            collectionsActivity.removeFromCart_orFavourites(fo4, bag, "Cart");
                        }
                    }
                }
            });

            add_to_favourite.setOnClickListener(view -> {
                if (favourites != null && !collectionsActivity.getLock()) {
                    if (favourites.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.add_to_favourite1).getConstantState()) {
                        favourites.setImageDrawable(null);
                        favourites.setImageResource(R.drawable.add_to_favourite5);

                        // Animations.onTapAnimation(favourites,3,context);
                        if (!collectionsActivity.addedtoFavourites(fo4.getCode())) {
                            collectionsActivity.addToCart_orFavourites(fo4, favourites, "Favourites");
                        }

                    } else if (favourites.getDrawable().getConstantState() == context.getResources().getDrawable(R.drawable.add_to_favourite5).getConstantState()) {
                        favourites.setImageDrawable(null);
                        favourites.setImageResource(R.drawable.add_to_favourite1);
                        //  Animations.onTapAnimation(bag,4,context);

                        //if its in favourites then only remove
                        if (collectionsActivity.addedtoFavourites(fo4.getCode())) {
                            collectionsActivity.removeFromCart_orFavourites(fo4, favourites, "Favourites");
                        }

                    }

                }
            });

            itemView.setOnLongClickListener(view -> {
                ClipData myClip;
                ClipboardManager clipboard = (ClipboardManager)
                        collectionsActivity.getSystemService(Context.CLIPBOARD_SERVICE);
                myClip = ClipData.newPlainText("text", fo4.getCode());
                clipboard.setPrimaryClip(myClip);
                Toast.makeText(collectionsActivity, "Code copied to clipboard", Toast.LENGTH_SHORT).show();
                return true;
            });

        }

       /* @Override
        public boolean onLongClick(View view) {
            Toast.makeText(collectionsActivity, "Long Click", Toast.LENGTH_SHORT).show();
            ClipData myClip;
            ClipboardManager clipboard = (ClipboardManager)
                    collectionsActivity.getSystemService(Context.CLIPBOARD_SERVICE);
            myClip = ClipData.newPlainText("text",fo4.getCode());
            clipboard.setPrimaryClip(myClip);
            return true;
        }*/
    }
}
