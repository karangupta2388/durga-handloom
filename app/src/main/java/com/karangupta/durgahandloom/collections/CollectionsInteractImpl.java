package com.karangupta.durgahandloom.collections;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 29/03/18.
 */
class CollectionsInteractImpl implements CollectionstInteractor {
    @NonNull private final FirebaseStore firebaseStore;

    public CollectionsInteractImpl(@NonNull FirebaseStore firebaseStore) {
        this.firebaseStore = firebaseStore;
    }

    @Override
    public void fetchViews(@NonNull String[] hierarchy,
                           @NonNull CollectionsPresenterImpl collectionsPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.fetchViews(hierarchy, collectionsPresenter);
        }
    }

    @Override
    public void doProcessOfAddingToCartOrFavourites(@NonNull ViewsModel fo4,
                                                    @NonNull String cart_or_favourites,
                                                    @NonNull CollectionsPresenterImpl collectionsPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.doProcessOfAddingToCartOrFavourites(fo4, cart_or_favourites, collectionsPresenter);
        }
    }

    @Override
    public void doProcessOfRemovingFromCartOrFavourites(@NonNull ViewsModel fo4,
                                                        @NonNull String cart_or_favourites,
                                                        @NonNull CollectionsPresenterImpl collectionsPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.doProcessOfRemovingFromCartOrFavourites(fo4, cart_or_favourites, collectionsPresenter);
        }
    }
}
