package com.karangupta.durgahandloom.collections;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.karangupta.durgahandloom.util.ChipModel;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import java.util.List;

/**
 * Created by karangupta on 29/03/18.
 */

interface CollectionsPresenter {
    void setView(CollectionsView collectionsActivity);

    void fetchViews(@NonNull String[] hierarchy, @Nullable List<ChipModel> chipModel_list);

    void destroy();

    void askPresenter_makeBorderinDropdown(int id);

    void fetchPaginationViews(int new_flag);

    void addToCart_orFavourites(ViewsModel fo4, String cart_or_favourites);

    void askPresenter_displayCount();

    void removeFromCart_orFavourites(ViewsModel fo4, String cart_or_favourites);

    boolean addedtoBag(String code);

    boolean addedtoFavourites(String code);

}
