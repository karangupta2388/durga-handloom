package com.karangupta.durgahandloom.collections;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.ChipModel;

import java.util.List;

/**
 * Created by karangupta on 29/03/18.
 */
public class ChipAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ChipModel> list;
    CollectionsView vieww;
    TextView textView = null;

    ChipAdapter(@NonNull List<ChipModel> chipModel_list, @NonNull CollectionsView collectionsActivity) {
        this.list = chipModel_list;
        this.vieww = collectionsActivity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return (new RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chip_views, parent, false)));
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        ((ChipAdapter.RowController) holder).bindModel(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private class RowController extends RecyclerView.ViewHolder {
        @NonNull TextView chip;
        @NonNull View row;

        private RowController(@NonNull View row) {
            super(row);
            this.row = row;
            this.chip = ((TextView) row.findViewById(R.id.chip));
        }

        private void bindModel(@NonNull final ChipModel fo4, int position) {
            if (fo4.getHierchy3() != null && !fo4.getHierchy3().equals("")) {
                chip.setText(fo4.getHierchy3());
                chip.setVisibility(View.VISIBLE);
            } else {
                chip.setVisibility(View.GONE);
            }

            if (position != 0) {
                chip.setBackgroundResource(R.drawable.shape_chip_drawable_white);
            } else {
                chip.setBackgroundResource(R.drawable.shape_chip_drawable_color_accent);
                textView = chip;
            }

            chip.setOnClickListener(view -> {
                String[] hierarchy;
                ChipModel chipModel = list.get(getAdapterPosition());
                if (chipModel.getHierchy3() != null) {
                    hierarchy = new String[3];
                    hierarchy[0] = chipModel.getHierchy1();
                    hierarchy[1] = chipModel.getHierchy2();
                    hierarchy[2] = chipModel.getHierchy3();
                } else {
                    hierarchy = new String[3];
                    hierarchy[0] = chipModel.getHierchy1();
                    hierarchy[1] = chipModel.getHierchy2();
                }

                textView.setBackgroundResource(R.drawable.shape_chip_drawable_white);
                chip.setBackgroundResource(R.drawable.shape_chip_drawable_color_accent);
                textView = chip;
                ((CollectionsActivity) vieww).onChipClicked(hierarchy);
            });
        }
    }
}