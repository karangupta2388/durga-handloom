package com.karangupta.durgahandloom.collections;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.Constants;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.cart.Cart;
import com.karangupta.durgahandloom.details.Details;
import com.karangupta.durgahandloom.util.ChipModel;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;
import com.karangupta.durgahandloom.wishlist.Wishlist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.karangupta.durgahandloom.util.Animations.animateHeight;

/**
 * Created by karangupta on 29/03/18.
 */

public class CollectionsActivity extends AppCompatActivity implements CollectionsView {

    @Inject
    CollectionsPresenter collectionsPresenter;

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.logo_dropdown_combo)
    LinearLayout logo_dropdown_combo;

    @Nullable
    @BindView(R.id.badge_count)
    TextView badge_count;

    @BindView(R.id.recycler_view_chip)
    RecyclerView recycler_view_chip;

    @BindView(R.id.recycler_view_views)
    RecyclerView recycler_view_views;

    @BindView(R.id.recycler_view_chip_frame_layout)
    FrameLayout recycler_view_chip_frame_layout;

    @BindView(R.id.recycler_view_views_frame_layout)
    FrameLayout recycler_view_views_frame_layout;

    @BindView(R.id.toolbar_text_combo)
    LinearLayout toolbar_text_combo;

    @BindView(R.id.dropdown)
    FrameLayout dropdown;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.heading)
    TextView heading;

    @BindView(R.id.sub_heading)
    TextView sub_heading;

    @BindView(R.id.cart_badgeCombo)
    RelativeLayout cart_badgeCombo;

    @BindView(R.id.heart)
    LinearLayout heart;

    @BindView(R.id._1)
    TextView _1;
    @BindView(R.id._2)
    TextView _2;
    @BindView(R.id._3)
    TextView _3;
    @BindView(R.id._4)
    TextView _4;
    @BindView(R.id._5)
    TextView _5;
    @BindView(R.id._6)
    TextView _6;
    @BindView(R.id._7)
    TextView _7;
    @BindView(R.id._8)
    TextView _8;
    @BindView(R.id._9)
    TextView _9;
    @BindView(R.id._10)
    TextView _10;
    @BindView(R.id._11)
    TextView _11;
    @BindView(R.id._12)
    TextView _12;
    @BindView(R.id._13)
    TextView _13;
    @BindView(R.id._14)
    TextView _14;
    @BindView(R.id._15)
    TextView _15;
    @BindView(R.id._16)
    TextView _16;
    @BindView(R.id._17)
    TextView _17;

    @NonNull final List<ChipModel> chipModel_list = new ArrayList<>();
    @NonNull final List<ViewsModel> viewsModel_List = new ArrayList<>();

    @Nullable private Unbinder unbinder;
    @Nullable private ChipAdapter chipAdapter;
    @Nullable private ViewsAdapter viewsAdapter;
    @Nullable private ImageView cart_or_favourites_imageView;

    private int i = 0;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    private boolean loading = true;

    private boolean locked = false;

    // Required empty public constructor
    public CollectionsActivity() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.collections);
        unbinder = ButterKnife.bind(this);
        setToolbar();
        ((BaseApplication) getApplication()).createCollectionsComponent().inject(this);
        collectionsPresenter.setView(CollectionsActivity.this);
        collectionsPresenter.askPresenter_displayCount();
        String[] hierchy = getIntent().getStringArrayExtra(Constants.HIERCHY);
        ArrayList<ChipModel> chipModel_list = getIntent().getParcelableArrayListExtra(Constants.CHIP_MODEL_LIST);
        int id = (int) getIntent().getIntExtra(Constants.ID, 0);
        getIntent().removeExtra(Constants.HIERCHY);
        getIntent().removeExtra(Constants.CHIP_MODEL_LIST);
        getIntent().removeExtra(Constants.ID);

        cart_badgeCombo.setOnClickListener(view -> {
            startActivity(new Intent(CollectionsActivity.this, Cart.class));
            overridePendingTransition(R.anim.slide_to_top, 0);

        });
        heart.setOnClickListener(view -> startActivity(
                new Intent(CollectionsActivity.this, Wishlist.class)));

        View.OnClickListener onClickListener = view -> {
            String[] hierarchy1 = Utilities.getHierchy(view.getId());
            ArrayList<ChipModel> chipModel_list1 = Utilities.getChipModel(view.getId());
            askPresenter_makeBorderinDropdown(view.getId());
            fetchViewsFromPresenter(hierarchy1, chipModel_list1);
        };

        setOnClickListener_on_Dropdown_Views(onClickListener);
        initAdapter();
        fetchViewsFromPresenter(hierchy, chipModel_list);
        askPresenter_makeBorderinDropdown(id);
    }

    private void askPresenter_makeBorderinDropdown(int id) {
        collectionsPresenter.askPresenter_makeBorderinDropdown(id);
    }

    private void setOnClickListener_on_Dropdown_Views(View.OnClickListener onClickListener) {
        _1.setOnClickListener(onClickListener);
        _2.setOnClickListener(onClickListener);
        _3.setOnClickListener(onClickListener);
        _4.setOnClickListener(onClickListener);
        _5.setOnClickListener(onClickListener);
        _6.setOnClickListener(onClickListener);
        _7.setOnClickListener(onClickListener);
        _8.setOnClickListener(onClickListener);
        _9.setOnClickListener(onClickListener);
        _10.setOnClickListener(onClickListener);
        _11.setOnClickListener(onClickListener);
        _12.setOnClickListener(onClickListener);
        _13.setOnClickListener(onClickListener);
        _14.setOnClickListener(onClickListener);
        _15.setOnClickListener(onClickListener);
        _16.setOnClickListener(onClickListener);
        _17.setOnClickListener(onClickListener);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        if (logo_dropdown_combo != null) {
            logo_dropdown_combo.setVisibility(View.VISIBLE);
        }
        setListener_for_animateDropdown();
    }

    public void setListener_for_animateDropdown() {
        toolbar_text_combo.setOnClickListener(view -> animateDropdown());
    }

    @Override
    public void animateDropdown() {
        if (i == 0) {
            animateHeight(
                    dropdown,
                    0,
                    (int) ((getResources().getDisplayMetrics().heightPixels * 0.70)), 200);
            i = 1;
        } else {
            animateHeight(
                    dropdown,
                    (int) ((getResources().getDisplayMetrics().heightPixels * 0.70)), 0, 200);
            i = 0;
        }
    }

    @Override
    public void closeDropdownIfOpen() {
        if (dropdown.getLayoutParams().height > 0) {
            animateHeight(
                    dropdown,
                    (int) ((getResources().getDisplayMetrics().heightPixels * 0.70)),
                    0,
                    200);
            i = 0;
        }
    }

    void fetchViewsFromPresenter(@NonNull String[] hierarchy,
                                 @Nullable ArrayList<ChipModel> chipModel_list) {
        collectionsPresenter.fetchViews(hierarchy, chipModel_list);

    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        locked = false;
        progress_bar.setVisibility(View.GONE);

    }

    @Override
    public void loadingFailed(@NonNull String errorMessage,
                              @Nullable String countAddOrRemove,
                              @Nullable String cart_or_favourites) {
        locked = false;
        progress_bar.setVisibility(View.GONE);
        revertChanges(countAddOrRemove, cart_or_favourites);
        Snackbar.make(recycler_view_chip, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void noInternet(@Nullable String countAddOrRemove, @Nullable String cart_or_favourites) {
        locked = false;
        if (countAddOrRemove != null && cart_or_favourites != null) {
            revertChanges(countAddOrRemove, cart_or_favourites);
        }
        Snackbar.make(recycler_view_chip, R.string.no_internet, Snackbar.LENGTH_LONG).show();
    }

    private void revertChanges(@Nullable String countAddOrRemove, @Nullable String cart_or_favourites) {
        if (countAddOrRemove != null && cart_or_favourites != null && cart_or_favourites_imageView != null) {
            if (countAddOrRemove.equals("count add")) {
                if (cart_or_favourites.equals("Cart"))
                    cart_or_favourites_imageView.setImageResource(R.drawable.add_to_cart1);
                else {
                    cart_or_favourites_imageView.setImageResource(R.drawable.add_to_favourite1);
                }
            } else if (countAddOrRemove.equals("count remove")) {
                if (cart_or_favourites.equals("Cart"))
                    cart_or_favourites_imageView.setImageResource(R.drawable.add_to_cart5);
                else
                    cart_or_favourites_imageView.setImageResource(R.drawable.add_to_favourite5);
            }
        }
    }

    private void initAdapter() {
        // For chip recycler view
        recycler_view_chip.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        chipAdapter = new ChipAdapter(chipModel_list, this);
        recycler_view_chip.setAdapter(chipAdapter);

        // For Views recycler view
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recycler_view_views.setLayoutManager(gridLayoutManager);
        viewsAdapter = new ViewsAdapter(viewsModel_List, this);
        recycler_view_views.setAdapter(viewsAdapter);
        recycler_view_views.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int firstVisibleItem = gridLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    loadingStarted();
                    collectionsPresenter.fetchPaginationViews(0);
                    loading = true;
                }
            }
        });
    }

    @Override
    public void displayChip_RecyclerView(List<ChipModel> chipModel_list) {
        this.chipModel_list.clear();
        this.chipModel_list.addAll(chipModel_list);
        chipAdapter.notifyDataSetChanged();
    }

    @Override
    public void displayViews_RecyclerView(@NonNull List<ViewsModel> list, int new_flag) {
        if (new_flag == 1) {
            this.viewsModel_List.clear();
            Collections.reverse(list);
            this.viewsModel_List.addAll(list);
        } else {
            this.viewsModel_List.addAll(list);
        }
        recycler_view_views.post(() -> {
            viewsAdapter.notifyDataSetChanged();
        });
        stopLoading();
    }

    @Override
    public void displayNoViewsToLoad() {
        locked = false;
        Snackbar.make(recycler_view_views_frame_layout, "No sarees to display", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onChipClicked(@NonNull String[] hierarchy) {
        fetchViewsFromPresenter(hierarchy, null);
    }

    @Override
    public void displayHeading_and_Subheading(@NonNull String hierarchy1, @NonNull String hierarchy2) {
        heading.setText(hierarchy1);
        sub_heading.setText(hierarchy2);
    }

    @Override
    public void createBorderInDropdown(int id) {
        ((TextView) findViewById(id)).setBackgroundResource(R.drawable.drop_down_border);
    }

    @Override
    public void createAndRemoveBorderInDropdown(int create, int remove) {
        if (remove != 0) {
            ((TextView) findViewById(remove)).setBackgroundResource(android.R.color.transparent);
        }
        ((TextView) findViewById(create)).setBackgroundResource(R.drawable.drop_down_border);
    }


    @Override
    public void resetScrollValues() {
        previousTotal = 0;
        visibleThreshold = 5;
        loading = true;
    }

    @Override
    public void displayCount(@Nullable String add_or_remove,
                             @Nullable String cart_or_favourite,
                             @NonNull String[] count) {
        locked = false;
        if (cart_or_favourite != null && add_or_remove != null) {
            if (add_or_remove.equals("add"))
                Snackbar.make(recycler_view_views, "Added to " + cart_or_favourite, Snackbar.LENGTH_SHORT).show();
            else
                Snackbar.make(recycler_view_views, "Removed from " + cart_or_favourite, Snackbar.LENGTH_SHORT).show();
        }

        if (count[1] != null && badge_count != null) {
            if (count[1].equals("0")) {
                badge_count.setVisibility(View.GONE);
            } else {
                badge_count.setText(count[1]);
                badge_count.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        collectionsPresenter.destroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
        ((BaseApplication) getApplication()).releaseCollectionsComponent();

        super.onDestroy();
    }

    public void addToCart_orFavourites(@NonNull ViewsModel fo4,
                                       @NonNull ImageView cart_or_favourites_imageView,
                                       @NonNull String cart_or_favourites) {
        this.locked = true;
        this.cart_or_favourites_imageView = cart_or_favourites_imageView;
        collectionsPresenter.addToCart_orFavourites(fo4, cart_or_favourites);
    }

    @Override
    protected void onStart() {
        collectionsPresenter.askPresenter_displayCount();

        super.onStart();
    }

    public void removeFromCart_orFavourites(ViewsModel fo4, ImageView cart_or_favourites_imageView, String cart_or_favourites) {
        this.cart_or_favourites_imageView = cart_or_favourites_imageView;
        this.locked = true;
        collectionsPresenter.removeFromCart_orFavourites(fo4, cart_or_favourites);
    }

    public boolean addedtoBag(String code) {
        return collectionsPresenter.addedtoBag(code);
    }

    public boolean addedtoFavourites(String code) {
        return collectionsPresenter.addedtoFavourites(code);
    }

    public void imageClicked(ViewsModel fo4, ImageView image_view_show_card) {

        ArrayList<String> list = Utilities.geEachUrl2(fo4.getUrl());
        Intent intent = new Intent(CollectionsActivity.this, Details.class);
        intent.putStringArrayListExtra(Constants.URLs, list);

        intent.putExtra(Constants.URL, fo4.getUrl());
        intent.putExtra(Constants.SERIES_NAME, fo4.getSeries_Name());
        intent.putExtra(Constants.CODE, fo4.getCode());
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, (View) image_view_show_card, "your_shared_transition_name");
        ActivityCompat.startActivityForResult(this, intent, 5, options.toBundle());
        //startActivity(intent);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

    }

    @Override
    protected void onStop() {
        locked = false;
        super.onStop();
    }

    Boolean getLock() {
        return locked;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == 5 && resultCode == RESULT_OK) {

                String requiredValue = data.getStringExtra("Key");
                Toast.makeText(this, "value returned" + requiredValue,
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }
}
