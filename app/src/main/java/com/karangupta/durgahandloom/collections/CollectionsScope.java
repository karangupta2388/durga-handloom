package com.karangupta.durgahandloom.collections;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 29/03/18.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CollectionsScope {
}
