package com.karangupta.durgahandloom.collections;

import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import io.reactivex.annotations.NonNull;

/**
 * Created by karangupta on 29/03/18.
 */

public interface CollectionstInteractor {


    void fetchViews(@NonNull String[] hierarchy, @NonNull CollectionsPresenterImpl collectionsPresenter);

    void doProcessOfAddingToCartOrFavourites(@NonNull ViewsModel fo4,
                                             @NonNull String cart_or_favourites,
                                             @NonNull CollectionsPresenterImpl collectionsPresenter);

    void doProcessOfRemovingFromCartOrFavourites(ViewsModel fo4, String cart_or_favourites, CollectionsPresenterImpl collectionsPresenter);
}
