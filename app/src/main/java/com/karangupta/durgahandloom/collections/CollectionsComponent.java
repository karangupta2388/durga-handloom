package com.karangupta.durgahandloom.collections;

import dagger.Subcomponent;

/**
 * Created by karangupta on 29/03/18.
 */

@CollectionsScope
@Subcomponent(modules = {CollectionsModule.class})
public interface CollectionsComponent {
    void inject(CollectionsActivity target);
}