package com.karangupta.durgahandloom.collections;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 29/03/18.
 */
@Module
public class CollectionsModule {
    @Provides
    CollectionstInteractor provideCollectionsInteractor(FirebaseStore firebaseStore) {
        return new CollectionsInteractImpl(firebaseStore);
    }

    @Provides
    CollectionsPresenter provideCollectionsPresenter(ConnectionDetector connectionDetector, CollectionstInteractor collectionstInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
        return new CollectionsPresenterImpl(connectionDetector, collectionstInteractor, localStorageStoreInteractor);
    }
}
