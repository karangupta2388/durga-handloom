package com.karangupta.durgahandloom.collections;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.karangupta.durgahandloom.util.ChipModel;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import java.util.List;

/**
 * Created by karangupta on 29/03/18.
 */

interface CollectionsView {

    void animateDropdown();

    void closeDropdownIfOpen();

    void loadingStarted();

    void stopLoading();

    void loadingFailed(@NonNull String errorMessage,
                       @Nullable String count,
                       @Nullable String cart_or_favourites);

    void noInternet(@Nullable String countAddOrRemove, @Nullable String cart_or_favourites);

    void displayChip_RecyclerView(List<ChipModel> chipModel_list);

    void displayNoViewsToLoad();

    void onChipClicked(@NonNull String[] hierarchy);

    void displayHeading_and_Subheading(@NonNull String hierarchy1, @NonNull String hierarchy2);

    void createBorderInDropdown(int id);

    void createAndRemoveBorderInDropdown(int create, int remove);

    void displayViews_RecyclerView(@NonNull List<ViewsModel> list, int new_flag);

    void resetScrollValues();

    void displayCount(@Nullable String add_or_remove,
                      @Nullable String cart_or_favourite,
                      @NonNull String[] count);

}
