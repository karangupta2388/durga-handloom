package com.karangupta.durgahandloom.collections;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ChipModel;
import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karangupta on 29/03/18.
 */

public class CollectionsPresenterImpl implements CollectionsPresenter {
    private ConnectionDetector connectionDetector;
    private LocalStorageStoreInteractor localStorageStoreInteractor;
    private CollectionstInteractor collectionsInteractor;
    private CollectionsView view;
    private String hierchy1 = null, hierchy2 = null, hierchy3 = null;
    private int remove = 0;//to store which dropdown id is having a border
    private DataSnapshot dataSnapshot;//snapshot of presently loaded view
    private int from = 0;//for pagination to load "from" to 15 more than from ie from+15
    private List<ViewsModel> present_list;//list that is presently visible
    private List<ViewsModel> baluchuri_jariBorder;
    private List<ViewsModel> baluchuri_nojariBorder;
    private List<ViewsModel> shantipur_colour;
    private List<ViewsModel> shantipur_white_cream;
    private List<ViewsModel> shantipur_embroidary;
    private List<ViewsModel> shantipur_print;


    CollectionsPresenterImpl(ConnectionDetector connectionDetector, CollectionstInteractor collectionstInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
        this.collectionsInteractor = collectionstInteractor;
        this.connectionDetector = connectionDetector;
        this.localStorageStoreInteractor = localStorageStoreInteractor;
    }

    @Override
    public void setView(CollectionsView collectionsView) {
        this.view = collectionsView;

    }

    @Override
    public void askPresenter_displayCount() {
        if (isViewAttached())
            view.displayCount(null, null, localStorageStoreInteractor.getCountValueFromSharedPrefs());
    }


    @Override
    synchronized public void fetchViews(@NonNull String[] hierarchy, @Nullable List<ChipModel> chipModel_list) {
        if (connectionDetector.isConnectingToInternet()) {
            // Now must  remove all chips fom view
            if (hierchy3 != null && hierarchy.length == 2) {
                // Empty list to remove all chips fom view
                displayChip_RecyclerView(new ArrayList<ChipModel>());
            }
            // ie. If the same chip or the same dropdown is not pressed then only do something
            if (!(hierarchy.length == 3
                && hierarchy[2].equals(hierchy3))
                && !(hierarchy.length == 2
                && hierarchy[1].equals(hierchy2))) {
                if (hierarchy.length == 3) {
                    hierchy1 = hierarchy[0];
                    hierchy2 = hierarchy[1];
                    hierchy3 = hierarchy[2];
                } else {
                    hierchy1 = hierarchy[0];
                    hierchy2 = hierarchy[1];
                    hierchy3 = null;
                }

                displayHeading_and_Subheading(hierarchy[0], hierarchy[1]);
                if (isViewAttached()) {
                    view.closeDropdownIfOpen();
                    view.loadingStarted();
                }
                int flag = 0;
                if (hierchy3 != null) {
                    if (hierchy3.equals("Jari Border") && this.baluchuri_jariBorder != null) {
                        flag = 1;
                        this.present_list = this.baluchuri_jariBorder;

                    } else if (hierchy3.equals("Without Jari Border") && this.baluchuri_nojariBorder != null) {
                        flag = 1;
                        this.present_list = this.baluchuri_nojariBorder;
                    } else if (hierchy3.equals("Colour") && this.shantipur_colour != null) {
                        flag = 1;
                        this.present_list = this.shantipur_colour;
                    } else if (hierchy3.equals("White Cream") && this.shantipur_white_cream != null) {
                        flag = 1;
                        this.present_list = this.shantipur_white_cream;
                    } else if (hierchy3.equals("Embroidery") && this.shantipur_embroidary != null) {
                        flag = 1;
                        this.present_list = this.shantipur_embroidary;
                    } else if (hierchy3.equals("Print") && this.shantipur_print != null) {
                        flag = 1;
                        this.present_list = this.shantipur_print;
                    }
                }
                if (flag == 1 && isViewAttached()) {
                    this.from = 0;
                    view.resetScrollValues();
                    fetchPaginationViews(1);
                } else if (flag == 0) {
                    // Fetch from Firebase_store
                    collectionsInteractor.fetchViews(hierarchy, this);
                }

                if (chipModel_list != null) {
                    displayChip_RecyclerView(chipModel_list);
                }
            }
        } else if (isViewAttached()) {
            view.noInternet(null, null);
        }
    }

    synchronized private void displayHeading_and_Subheading(@NonNull String s1, @NonNull String s2) {
        if (isViewAttached()) {
            view.displayHeading_and_Subheading(s1, s2);
        }
    }

    public void loadingFailed(@NonNull String message,
                              @Nullable String countAddorRemove,
                              @Nullable String cart_or_favourites) {
        if (isViewAttached())
            view.loadingFailed(message, countAddorRemove, cart_or_favourites);
    }

    @Override
    public void destroy() {
        view = null;

    }

    @Override
    synchronized public void askPresenter_makeBorderinDropdown(int create) {
        if (isViewAttached()) {
            view.createAndRemoveBorderInDropdown(create, remove);
            remove = create;
        }
    }

    @Override
    synchronized public void fetchPaginationViews(int new_flag) {
        //int i=0;
        List<ViewsModel> temp_list = new ArrayList<>();
        int change_flag = 0;
        if (present_list != null) {
            // int to = present_list.size() <= 4 ? present_list.size() : 4;
            // remaining=present_list.size()-to;
            int to = from + 4;
            for (; from < to; from++) {
                if (from < present_list.size()) {
                    change_flag = 1;
                    temp_list.add(present_list.get(from));
                }
            }
        }
        if (isViewAttached()) {
            // from=from+15;
            if (!temp_list.isEmpty() && change_flag == 1) {
                view.displayViews_RecyclerView(temp_list, new_flag);
            } else {
                view.stopLoading();
            }
        }

    }

    @Override
    public void addToCart_orFavourites(ViewsModel fo4, String cart_or_favourites) {
        if (connectionDetector.isConnectingToInternet()) {
            synchronized ((CollectionsInteractImpl) collectionsInteractor) {
                collectionsInteractor.doProcessOfAddingToCartOrFavourites(fo4, cart_or_favourites, this);
            }
        } else if (isViewAttached()) {
            view.noInternet("count add", cart_or_favourites);
        }
    }

    @Override
    public void removeFromCart_orFavourites(ViewsModel fo4, String cart_or_favourites) {
        if (connectionDetector.isConnectingToInternet()) {
            synchronized ((CollectionsInteractImpl) collectionsInteractor) {
                collectionsInteractor.doProcessOfRemovingFromCartOrFavourites(fo4, cart_or_favourites, this);
            }
        } else if (isViewAttached()) {
            view.noInternet("count remove", cart_or_favourites);
        }

    }

    @Override
    public boolean addedtoBag(String code) {
        return localStorageStoreInteractor.addedToCart(code);
    }

    @Override
    public boolean addedtoFavourites(String code) {
        return localStorageStoreInteractor.addedToFavourites(code);
    }

    synchronized private void displayChip_RecyclerView(List<ChipModel> chipModel_list) {
        if (isViewAttached()) {
            view.displayChip_RecyclerView(chipModel_list);
        }
    }

    private boolean isViewAttached() {
        return view != null;
    }

    //new Views of new hierchy loaded from FirebaseStore
    synchronized public void displayViews(List<ViewsModel> viewsModel_List) {
        this.present_list = viewsModel_List;
        this.from = 0;
        if (hierchy3 != null) {
            if (hierchy3.equals("Jari Border")) {
                Log.d("KARAN", "fresh loaded jari border and stored");
                this.baluchuri_jariBorder = viewsModel_List;

            } else if (hierchy3.equals("Without Jari Border")) {
                Log.d("KARAN", "fresh loaded without jari border and stored");

                this.baluchuri_nojariBorder = viewsModel_List;

            } else if (hierchy3.equals("Colour")) {
                this.shantipur_colour = viewsModel_List;

            } else if (hierchy3.equals("White Cream")) {
                this.shantipur_white_cream = viewsModel_List;

            } else if (hierchy3.equals("Embroidery")) {
                this.shantipur_embroidary = viewsModel_List;

            } else if (hierchy3.equals("Print")) {
                this.shantipur_print = viewsModel_List;

            }

        }
        if (isViewAttached()) {
            view.resetScrollValues();
            fetchPaginationViews(1);
        }
        //view.displayViews_RecyclerView(viewsModel_List);
    }

    public void displayNoViewstoLoad() {
        if (isViewAttached())
            view.displayNoViewsToLoad();
    }

    public void countUpdated(ViewsModel fo4, String add_or_remove, String count, String cart_or_favourites) {
        localStorageStoreInteractor.setCountValueToSharedPrefs(count, cart_or_favourites);//setting count value in sharedprefs
        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(), add_or_remove, cart_or_favourites);
        if (isViewAttached())
            view.displayCount(add_or_remove, cart_or_favourites, localStorageStoreInteractor.getCountValueFromSharedPrefs());

    }
}
