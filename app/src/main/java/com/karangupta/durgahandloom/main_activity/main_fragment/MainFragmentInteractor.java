package com.karangupta.durgahandloom.main_activity.main_fragment;

import com.karangupta.durgahandloom.util.home_model.HomeSaree;

import java.util.List;

import io.reactivex.Maybe;

/**
 * Created by karangupta on 27/03/18.
 */

public interface MainFragmentInteractor {
    Boolean isUserLoggedIn();
    Maybe<List<HomeSaree>> fetchBaluchuri();
    Maybe<List<HomeSaree>> fetchDhaniyaKali();
    Maybe<List<HomeSaree>> fetchJaamdani();
    Maybe<List<HomeSaree>> fetchFulia();
    Maybe<List<HomeSaree>> fetchBahaCotton();
    Maybe<List<HomeSaree>> fetchShantipur();

   void fetchCount(MainFragmentPresenterImpl mainFragmentPresenter);

    void setUidandCountforNewUser();

    void logout();
}
