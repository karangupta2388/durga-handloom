package com.karangupta.durgahandloom.main_activity.order_history;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.order_history_model.OrderHistoryModel;

import java.util.List;

import customfonts.MyTextView;

/**
 * Created by karangupta on 05/04/18.
 */

class OrderHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<OrderHistoryModel> list;
    OrderHistory orderHistory;
    private Context context;

    public OrderHistoryAdapter(List<OrderHistoryModel> list, OrderHistory orderHistory) {
        this.orderHistory=orderHistory;
        this.list= list;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        return (new OrderHistoryAdapter.RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_history_views, parent, false)));

    }
    public int getItemCount() {

        return (list.size());
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((OrderHistoryAdapter.RowController) holder).bindModel(list.get(position));
    }
    private class RowController extends RecyclerView.ViewHolder
    {
        MyTextView date;
        MyTextView orderId;
        MyTextView quantity;
        //MyTextView name;
       // MyTextView phone;
        MyTextView address;
        //MyTextView uid;

        MyTextView details;



        View row=null;
        private RowController(View row) {
            super(row);
            this.row=row;
            this.date=((MyTextView)row.findViewById(R.id.date));
            this.orderId=((MyTextView)row.findViewById(R.id.orderId));
            this.quantity=((MyTextView)row.findViewById(R.id.quantity));
            //this.name=((MyTextView)row.findViewById(R.id.name));
            //this.phone=((MyTextView)row.findViewById(R.id.phone));
            this.address=((MyTextView)row.findViewById(R.id.address));
            this.details=((MyTextView)row.findViewById(R.id.details));

            //this.uid=((MyTextView)row.findViewById(R.id.uid));
        }

        private void bindModel(final OrderHistoryModel fo4) {

            if(details!=null)
            {
                details.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        orderHistory.detailsClicked(fo4.getOrderId());
                    }
                });
            }
            if (date != null) {
                if (fo4.getDate() != null && !fo4.getDate().equals("")) {
                    date.setText(fo4.getDate());
                    date.setVisibility(View.VISIBLE);
                } else {
                    date.setVisibility(View.GONE);
                }

            }

            if (orderId != null) {
                if (fo4.getOrderId() != null && !fo4.getOrderId().equals("")) {
                    orderId.setText(fo4.getOrderId());
                    orderId.setVisibility(View.VISIBLE);
                } else {
                    orderId.setVisibility(View.GONE);
                }

            }

            if (quantity != null) {
                if (fo4.getQuantity() != null && !fo4.getQuantity().equals("")) {
                    quantity.setText(fo4.getQuantity());
                    quantity.setVisibility(View.VISIBLE);
                } else {
                    quantity.setVisibility(View.GONE);
                }

            }

            /*String name1=null;
            if (name != null) {
                if (fo4.getFirstName() != null && !fo4.getFirstName().equals("")  &&  fo4.getSecondname() != null && !fo4.getSecondname().equals("")) {
                name1=fo4.getFirstName()+" "+ fo4.getSecondname();
                    name.setText(name1);
                    name.setVisibility(View.VISIBLE);
                } else {
                    name.setVisibility(View.GONE);
                }

            }*/
String address1=null;
            if (address != null) {
                if (fo4.getAddress() != null && !fo4.getAddress().equals("") && fo4.getCity() != null && !fo4.getCity().equals("") && fo4.getPin() != null && !fo4.getPin().equals("")) {
                   address1=fo4.getAddress()+" "+fo4.getCity()+" "+fo4.getPin();
                    address.setText(address1);
                    address.setVisibility(View.VISIBLE);
                } else {
                    address.setVisibility(View.GONE);
                }

            }

        }
    }


}