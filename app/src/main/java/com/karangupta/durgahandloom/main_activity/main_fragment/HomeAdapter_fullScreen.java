package com.karangupta.durgahandloom.main_activity.main_fragment;

/**
 * @author arun
 */


/*public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {
    private List<HomeSaree> list;
    private MainFragmentView view;
    private Context context;

    public HomeAdapter(List<HomeSaree> list,MainFragmentView view) {
        this.list = list;
this.view=view;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image_view)
        ImageView image_view;
        @BindView(R.id.category_text)
        TextView category_text;

        public HomeSaree homeSaree;

        public ViewHolder(View root) {
            super(root);
            ButterKnife.bind(this, root);
            image_view.getLayoutParams().width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.82);
            image_view.getLayoutParams().height = (int) ((context.getResources().getDisplayMetrics().widthPixels * 0.95) / 1.77);
        }

        @Override
        public void onClick(View view) {
            Log.d("KARAN",list.get(getAdapterPosition()).getId() );
            HomeAdapter.this.view.onHomeSareeClicked(list.get(getAdapterPosition()).getId());
        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View rootView = LayoutInflater.from(context).inflate(R.layout.home_views, parent, false);

        return new ViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.itemView.setOnClickListener(holder);
        holder.homeSaree = list.get(position);
        holder.category_text.setText(holder.homeSaree.getCategory());


        Glide.with(context).load(holder.homeSaree.getUrl())
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image_view);

    }



    @Override
    public int getItemCount() {
        return list.size();
    }
}*/

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.home_model.HomeSaree;

import java.util.List;


public class HomeAdapter_fullScreen extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HomeSaree> list;
    private MainFragmentView view;
    private Context context;

    public HomeAdapter_fullScreen(List<HomeSaree> list, MainFragmentView view) {
        this.list = list;
        this.view=view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        return (new RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_views, parent, false)));

    }

    @Override
    public int getItemCount() {

        return (list.size());
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((HomeAdapter_fullScreen.RowController) holder).bindModel(list.get(position));
    }


    private    class RowController extends RecyclerView.ViewHolder
    {
        Spinner spinner=null;
        ImageView imageView=null;
        TextView category_text=null;

        View row=null;
        private RowController(View row) {
            super(row);
            this.row=row;
            this.category_text=((TextView)row.findViewById(R.id.category_text));
            this.imageView=((ImageView)row.findViewById(R.id.image_view));
            this.imageView.getLayoutParams().width = (int) (context.getResources().getDisplayMetrics().widthPixels );
            this.imageView.getLayoutParams().height = (int) (context.getResources().getDisplayMetrics().widthPixels *0.65);
        }

        private void bindModel(final HomeSaree fo4) {

            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) row.getLayoutParams();
                params.rightMargin = 0; params.leftMargin = 0;

           /* if (category_text != null) {
                if (fo4.getCategory() != null && !fo4.getCategory().equals("")) {
                    category_text.setText(fo4.getCategory());
                    category_text.setVisibility(View.VISIBLE);
                } else {
                    category_text.setVisibility(View.GONE);
                }

            }*/
            category_text.setVisibility(View.GONE);

            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    HomeAdapter_fullScreen.this.view.onHomeSareeClicked(list.get(getAdapterPosition()).getId());

                }
            });



            if (imageView != null) {

                imageView.setImageResource(android.R.color.transparent);
                imageView.setVisibility(View.VISIBLE);

                Glide.with(context).load(fo4.getUrl())
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);


            }else
            {
                imageView.setImageResource(android.R.color.transparent);
                imageView.setVisibility(View.INVISIBLE);
            }




        }
    }


}


