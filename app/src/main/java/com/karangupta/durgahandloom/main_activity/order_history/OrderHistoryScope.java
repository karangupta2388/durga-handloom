package com.karangupta.durgahandloom.main_activity.order_history;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 05/04/18.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderHistoryScope
{
}