package com.karangupta.durgahandloom.main_activity.main_fragment;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.home_model.HomeSaree;
import com.karangupta.durgahandloom.util.home_model.HomeSareeWrapper;

import java.util.List;

import io.reactivex.Maybe;

/**
 * Created by karangupta on 27/03/18.
 */

public class MainFragmentInteractorImpl implements MainFragmentInteractor{
    private FirebaseStore firebaseStore;

    public MainFragmentInteractorImpl(FirebaseStore firebaseStore) {
        this.firebaseStore=firebaseStore;
    }

    @Override
    public Boolean isUserLoggedIn() {
        return firebaseStore.isUserLoggedIn();
    }

    @Override
    public Maybe<List<HomeSaree>> fetchBaluchuri() {
       // return new HomeSareeWrapper(firebaseStore.fetchBaluchuri());
        return HomeSareeWrapper.getMaybe(firebaseStore.fetchBaluchuri());
        //.map(HomeSareeWrapper::getMaybe);
    }

    @Override
    public Maybe<List<HomeSaree>> fetchDhaniyaKali() {
      //  return firebaseStore.fetchDhaniyaKali().map(HomeSareeWrapper::getBaluchuri);
        return HomeSareeWrapper.getMaybe(firebaseStore.fetchDhaniyaKali());
    }

    @Override
    public Maybe<List<HomeSaree>> fetchJaamdani() {
        //return firebaseStore.fetchJaamdani().map(HomeSareeWrapper::getBaluchuri);
        return HomeSareeWrapper.getMaybe(firebaseStore.fetchJaamdani());

    }

    @Override
    public Maybe<List<HomeSaree>> fetchFulia() {
       // return firebaseStore.fetchFulia().map(HomeSareeWrapper::getBaluchuri);
        return HomeSareeWrapper.getMaybe(firebaseStore.fetchFulia());

    }

    @Override
    public Maybe<List<HomeSaree>> fetchBahaCotton() {
       // return firebaseStore.fetchBahaCotton().map(HomeSareeWrapper::getBaluchuri);
        return HomeSareeWrapper.getMaybe(firebaseStore.fetchBahaCotton());

    }

    @Override
    public Maybe<List<HomeSaree>> fetchShantipur() {
        //return firebaseStore.fetchShantipur().map(HomeSareeWrapper::getBaluchuri);
        return HomeSareeWrapper.getMaybe(firebaseStore.fetchShantipur());

    }

    @Override
    public void fetchCount(MainFragmentPresenterImpl mainFragmentPresenter) throws NullPointerException {
        firebaseStore.fetchCount(mainFragmentPresenter);
    }

    @Override
    public void setUidandCountforNewUser() {
        firebaseStore.setUidAndCountForNewUser();
    }

    @Override
    public void logout() {
        firebaseStore.logout();
    }
}
