package com.karangupta.durgahandloom.main_activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.cart.Cart;
import com.karangupta.durgahandloom.intro.SplashActivity;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragment;
import com.karangupta.durgahandloom.main_activity.order_history.OrderHistory;
import com.karangupta.durgahandloom.wishlist.Wishlist;

import butterknife.BindView;
import butterknife.ButterKnife;
import customfonts.MyTextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainFragment.Callback {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @Nullable
    @BindView(R.id.badge_count)
    TextView badge_count;

    @BindView(R.id.cart_badgeCombo)
    RelativeLayout cart_badgeCombo;

    @BindView(R.id.heart)
    LinearLayout heart;

    @BindView(R.id.appbar_linear_layout)
    LinearLayout appbar_linear_layout;

    @BindView(R.id.title)
    MyTextView title;

    FrameLayout dropdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setToolbar(1);
        cart_badgeCombo.setOnClickListener(view -> {
            startActivity(new Intent(MainActivity.this, Cart.class));
            overridePendingTransition(R.anim.slide_to_top, 0);
        });

        heart.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, Wishlist.class)));
        setFragment(new MainFragment());

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }

    public void setToolbar(int flag) {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);//cool
        if (flag == 1) {
            title.setVisibility(View.VISIBLE);
            title.setText("Durga Handloom");
            appbar_linear_layout.setVisibility(View.VISIBLE);


        } else if (flag == 2) {
            title.setVisibility(View.VISIBLE);
            title.setText("Order History");
            appbar_linear_layout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            setToolbar(1);
            if (getFragmentManager().getBackStackEntryCount() > 0)
                getFragmentManager().popBackStack();
            else
                setFragment(new MainFragment());
        } else if (id == R.id.order_history) {
            setToolbar(2);
            setFragment(new OrderHistory());

        } else if (id == R.id.logout) {
            FirebaseAuth mAuth;
            mAuth = FirebaseAuth.getInstance();
            mAuth.signOut();
            LoginManager.getInstance().logOut();
            Intent intent = new Intent(this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setFragment(Fragment fragment) {
        android.app.FragmentManager fragmentManager = getFragmentManager();
        android.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_frame_layout, fragment);
        if (fragment instanceof OrderHistory) {
            fragmentTransaction.addToBackStack("Backstack").commit();
        } else
            fragmentTransaction.commit();
    }

    @Override
    public void setBatchCount(String count) {
        if (badge_count == null) {
            return;
        }
        if (count.equals("0")) {
            badge_count.setVisibility(View.GONE);
        } else {
            badge_count.setText(count);
            badge_count.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
