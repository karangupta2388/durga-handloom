package com.karangupta.durgahandloom.main_activity.order_history;

import com.karangupta.durgahandloom.util.order_history_model.OrderHistoryModel;

import java.util.List;

/**
 * Created by karangupta on 05/04/18.
 */

interface OrderHistoryView {
    void loadingFailed(String message);

    void displayOrderHistory(List<OrderHistoryModel> orderHistoryModeList);

    void displayEmptyList();

    void loadingStarted();

    void noInternet();

    void displayTotalText(int size);
}
