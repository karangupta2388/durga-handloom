package com.karangupta.durgahandloom.main_activity.main_fragment;

import dagger.Subcomponent;

/**
 * Created by karangupta on 27/03/18.
 */

@MainFragmentScope
@Subcomponent(modules = {MainFragmentModule.class})
public interface MainFragmentComponent
{
    void inject(MainFragment target);
}
