package com.karangupta.durgahandloom.main_activity.main_fragment;

/**
 * Created by karangupta on 27/03/18.
 */

interface MainFragmentPresenter {
    void setView(MainFragmentView mainFragmentViews);

    void displayCount();

    public void displaySarees();
    void destroy();


}
