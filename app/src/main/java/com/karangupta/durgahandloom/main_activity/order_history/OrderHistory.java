package com.karangupta.durgahandloom.main_activity.order_history;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.order_detail.OrderDetail;
import com.karangupta.durgahandloom.util.order_history_model.OrderHistoryModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customfonts.MyTextView;

/**
 * Created by karangupta on 27/03/18.
 */

public class OrderHistory extends Fragment implements OrderHistoryView{

    @Inject
    OrderHistoryPresenter orderHistoryPresenter;

    @BindView(R.id.recycler_view_order_history_views)
    RecyclerView recycler_view_order_history_views;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.total)
    TextView total;

    @BindView(R.id.empty)
    MyTextView empty;

   // private OrderHistory.Callback2 callback2;
    private Unbinder unbinder;
    private OrderHistoryAdapter orderHistoryAdapter;

    private List<OrderHistoryModel> orderHistoryModelList = new ArrayList<>();


    public OrderHistory() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //callback2 = (OrderHistory.Callback2) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
        ((BaseApplication) getActivity().getApplication()).createOrderHistoryComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.order_history, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        initLayoutReferences();
        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        orderHistoryPresenter.setView(this);
        orderHistoryPresenter.fetchOrdersHistory();
    }

    private void initLayoutReferences() {

        recycler_view_order_history_views.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        orderHistoryAdapter = new OrderHistoryAdapter(orderHistoryModelList,this );
        recycler_view_order_history_views.setAdapter(orderHistoryAdapter);
    }

    @Override
    synchronized public void displayOrderHistory(List<OrderHistoryModel> orderHistoryModelList) {
        //Toast.makeText(getActivity(), "displayOrderHistory", Toast.LENGTH_SHORT).show();
            stopLoading();
            empty.setVisibility(View.GONE);
            this.orderHistoryModelList.clear();
            this.orderHistoryModelList.addAll(orderHistoryModelList);
            orderHistoryAdapter.notifyDataSetChanged();


    }

    @Override
    public void displayEmptyList() {
        stopLoading();
        empty.setVisibility(View.VISIBLE);

    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }
    private  void stopLoading()
    {

        progress_bar.setVisibility(View.GONE);

    }
    @Override
    public void loadingFailed(String errorMessage) {
        progress_bar.setVisibility(View.GONE);
        Snackbar.make(recycler_view_order_history_views, "Something Went Wrong", Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void noInternet() {
        Snackbar.make(recycler_view_order_history_views, R.string.no_internet, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void displayTotalText(int size) {
        total.setText(String.valueOf(size)+" Orders placed");
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        orderHistoryPresenter.destroy();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        //callback2 = null;
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseApplication) getActivity().getApplication()).releaseOrderHistoryComponent();
    }

    public void detailsClicked(String order_id) {
        startActivity((new Intent(getActivity(), OrderDetail.class)).putExtra("Order Id",order_id));
    }


  /*  public void retrieveUser(){
        Query query = FirebaseDatabase.getInstance().getReference().child("user_ref").orderByChild("id").equalTo(0);;
        Maybe<PublicUserData> publicUserDataMaybe1 = RxFirebaseDatabase.observeSingleValueEvent(query, PublicUserData.class);
publicUserDataMaybe1.map(PublicUserData::getUsers).
        Maybe<PublicUserData> publicUserDataMaybe =  (Maybe<PublicUserData>)RxFirebaseDatabase.observeSingleValueEvent(query, PublicUserData.class);
        publicUserDataMaybe.map(Users->{return Users})
                .subscribe(userData -> updateUserUi(userData),
                        throwable -> manageError(throwable));
    }*/



    public interface Callback2 {
        void setBatchCount(String count);
    }
}


