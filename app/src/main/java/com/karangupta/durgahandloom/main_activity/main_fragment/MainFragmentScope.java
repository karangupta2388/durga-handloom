package com.karangupta.durgahandloom.main_activity.main_fragment;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 27/03/18.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainFragmentScope
{
}