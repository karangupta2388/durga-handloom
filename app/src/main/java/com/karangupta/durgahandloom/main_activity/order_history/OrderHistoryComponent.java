package com.karangupta.durgahandloom.main_activity.order_history;

import dagger.Subcomponent;

/**
 * Created by karangupta on 05/04/18.
 */

@OrderHistoryScope
@Subcomponent(modules = {OrderHistoryModule.class})
public interface OrderHistoryComponent
{
    void inject(OrderHistory target);
}