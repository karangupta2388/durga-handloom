package com.karangupta.durgahandloom.main_activity.order_history;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 05/04/18.
 */
@Module
public class OrderHistoryModule {
    @Provides
    OrderHistoryInteractor provideOrderHistoryInteractor(FirebaseStore firebaseStore) {
        return new OrderHistoryInteractorImpl(firebaseStore);
    }

    @Provides
    OrderHistoryPresenter provideOrderHistoryPresenter(ConnectionDetector connectionDetector, OrderHistoryInteractor orderHistoryInteractor) {
        return new OrderHistoryPresenterImpl(connectionDetector,orderHistoryInteractor);
    }
}
