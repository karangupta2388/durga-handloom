package com.karangupta.durgahandloom.main_activity.main_fragment;

import com.karangupta.durgahandloom.util.home_model.HomeSaree;

import java.util.List;

/**
 * Created by karangupta on 27/03/18.
 */

public interface MainFragmentView {
    public void showSarees(  List<HomeSaree> list_baluchuri,
                             List<HomeSaree> list_dhaniya_kali,
                             List<HomeSaree> list_jaamdani ,
                             List<HomeSaree>  list_fulia,
                             List<HomeSaree> list_baha_cotton,
                             List<HomeSaree> list_shantipur);
    void loadingStarted();
    void loadingFailed(String errorMessage);
    void onHomeSareeClicked(String id);
    void noInternet();
    void displayCount(String[] count);
}
