package com.karangupta.durgahandloom.main_activity.order_history;

import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.order_history_model.OrderHistoryModel;

import java.util.List;

/**
 * Created by karangupta on 05/04/18.
 */

public class OrderHistoryPresenterImpl implements OrderHistoryPresenter {

    private OrderHistoryView view;

    ConnectionDetector connectionDetector;
    OrderHistoryInteractor orderHistoryInteractor;
    public OrderHistoryPresenterImpl(ConnectionDetector connectionDetector, OrderHistoryInteractor orderHistoryInteractor) {
        this.connectionDetector=connectionDetector;
        this.orderHistoryInteractor=orderHistoryInteractor;
    }

    @Override
    public void destroy() {
        view = null;
        this.view = view;

    }



    @Override
    public void setView(OrderHistory orderHistory) {
view=orderHistory;
    }

    @Override
    public void fetchOrdersHistory() {
        if(connectionDetector.isConnectingToInternet()) {
            if (isViewAttached())
                view.loadingStarted();
            orderHistoryInteractor.fetchOrdersHistory(this);
        }else
        {
            if(isViewAttached())
            {
                view.noInternet();
            }
        }
    }

    private boolean isViewAttached() {
        return view != null;
    }

    public void loadingFailed(String message) {
        if(isViewAttached())
            view.loadingFailed(message);
    }

    public void displayOrderHistory(List<OrderHistoryModel> orderHistoryModeList) {
        if(isViewAttached()) {
            if (orderHistoryModeList == null) {
                view.displayEmptyList();
            }else
            {
                view.displayOrderHistory(orderHistoryModeList);
                view.displayTotalText(orderHistoryModeList.size());

            }

        }
    }
}
