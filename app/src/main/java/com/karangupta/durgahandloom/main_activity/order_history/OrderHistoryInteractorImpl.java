package com.karangupta.durgahandloom.main_activity.order_history;

import com.karangupta.durgahandloom.firebase.FirebaseStore;

/**
 * Created by karangupta on 05/04/18.
 */

class OrderHistoryInteractorImpl implements OrderHistoryInteractor {
    FirebaseStore firebaseStore;
    public OrderHistoryInteractorImpl(FirebaseStore firebaseStore) {
        this.firebaseStore=firebaseStore;
    }

    @Override
    public void fetchOrdersHistory(OrderHistoryPresenterImpl orderHistoryPresenter) {
        firebaseStore.fetchOrdersHistory(orderHistoryPresenter);
    }
}
