package com.karangupta.durgahandloom.main_activity.main_fragment;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

@Module
public class MainFragmentModule {
    @Provides
    MainFragmentInteractor provideMainFragmentInteractor(FirebaseStore firebaseStore) {
        return new MainFragmentInteractorImpl(firebaseStore);
    }

    @Provides
    MainFragmentPresenter provideMainFragmentPresenter(ConnectionDetector connectionDetector, MainFragmentInteractor mainFragmentInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
        return new MainFragmentPresenterImpl(connectionDetector,mainFragmentInteractor,localStorageStoreInteractor);
    }
}