package com.karangupta.durgahandloom.main_activity.main_fragment;

import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.RxUtils;
import com.karangupta.durgahandloom.util.home_model.HomeSaree;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by karangupta on 27/03/18.
 */

public class MainFragmentPresenterImpl implements MainFragmentPresenter {

    private LocalStorageStoreInteractor localStorageStoreInteractor;
    private MainFragmentInteractor mainFragmentInteractor;
    private MainFragmentView view;
    private Disposable fetchSubscription1;
    private Disposable fetchSubscription2;
    private Disposable fetchSubscription3;
    private Disposable fetchSubscription4;
    private Disposable fetchSubscription5;
    private Disposable fetchSubscription6;
    //private Disposable fetchSubscription7;

    private ConnectionDetector connectionDetector;


    public MainFragmentPresenterImpl(ConnectionDetector connectionDetector,MainFragmentInteractor mainFragmentInteractor,LocalStorageStoreInteractor localStorageStoreInteractor) {
        this.localStorageStoreInteractor = localStorageStoreInteractor;
        this.mainFragmentInteractor=mainFragmentInteractor;
        this.connectionDetector=connectionDetector;
    }

    @Override
    public void setView(MainFragmentView view) {
        this.view = view;
        localStorageStoreInteractor.remove_all_SharedPreferences();
        if(connectionDetector.isConnectingToInternet())
        {
            mainFragmentInteractor.logout();
            displayCount();//display count of favourites and cart
        displaySarees();
        }else
            showNoInternet();
    }
    @Override
    public void displayCount() {

        mainFragmentInteractor.fetchCount(this);
    }




    public void NewUser() {
    //new user
    String count[]=new String[2];
    //user is new
    count[0]="0";
    count[1]="0";
    view.displayCount(count);
    setUidandCountforNewUser();//set UID key and Count key and its value in database

    }


    //This method is called means the user exists and is an old user
    /* public void OldUser( CountModel countModel)
    {

        Log.d("KARAN","OldUSer");

            String favourites = countModel.getFavourites();
            String cart = countModel.getCart();
        Log.d("AJIT","cart"+cart+"" + "\nfavourites"+favourites);

        String count[] = new String[2];

            //old user
            localStorageStoreInteractor.setCountValuetoSharedPrefs(cart, "Cart");//storing in SharedPrefs
        localStorageStoreInteractor.setCountValuetoSharedPrefs(favourites,"Favourites");
            count[0] = favourites;
            count[1] = cart;
            if(isViewAttached())
            view.displayCount(count);

    }*/


    public void OldUser(String cart_count, List<String> cart_codes, String favourites_count, List<String> favourites_codes) {

        String count[] = new String[2];
        count[0] = favourites_count;
        count[1] = cart_count;
        //old user updating(refreshing) count values in database
        localStorageStoreInteractor.setCountValueToSharedPrefs(cart_count, "Cart");//storing in SharedPrefs
        localStorageStoreInteractor.setCountValueToSharedPrefs(favourites_count,"Favourites");//storing in SharedPrefs


        //old user updating(refreshing) all codes  in database
        if(cart_codes!=null)
        {
            for(int i=0;i<cart_codes.size();i++)//For Cart
        {
            localStorageStoreInteractor.setCodeInSharedPrefs(cart_codes.get(i),"add","Cart");

        }
        }

        if(favourites_codes!=null) {
            for (int i = 0; i < favourites_codes.size(); i++)//For favourites
            {

                localStorageStoreInteractor.setCodeInSharedPrefs(favourites_codes.get(i), "add", "Favourites");

            }
        }

        if(isViewAttached())
            view.displayCount(count);

    }














    private void setUidandCountforNewUser() {
        mainFragmentInteractor.setUidandCountforNewUser();
    }


    public void displaySarees() {
        if( mainFragmentInteractor.isUserLoggedIn())

        {
            showLoading();
            fetchSubscription1 = mainFragmentInteractor.fetchBaluchuri()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onFetchSuccess_baluchuri, this::onMovieFetchFailed);
           fetchSubscription2 = mainFragmentInteractor.fetchDhaniyaKali()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onFetchSuccess_DhaniyaKali, this::onMovieFetchFailed);
            fetchSubscription3 = mainFragmentInteractor.fetchJaamdani()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onFetchSuccess_Jaamdani, this::onMovieFetchFailed);
            fetchSubscription4 = mainFragmentInteractor.fetchFulia()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onFetchSuccess_Fulia, this::onMovieFetchFailed);
            fetchSubscription5 = mainFragmentInteractor.fetchBahaCotton()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onFetchSuccess_BahaCotton, this::onMovieFetchFailed);
            fetchSubscription6 = mainFragmentInteractor.fetchShantipur()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onFetchSuccess_Shantipur, this::onMovieFetchFailed);
        }
        else
            showNoInternet();

    }

    @Override
    public void destroy() {
        view = null;
        RxUtils.unsubscribe(fetchSubscription1);
        RxUtils.unsubscribe(fetchSubscription2);
        RxUtils.unsubscribe(fetchSubscription3);
        RxUtils.unsubscribe(fetchSubscription4);
        RxUtils.unsubscribe(fetchSubscription5);
        RxUtils.unsubscribe(fetchSubscription6);
      //  RxUtils.unsubscribe(fetchSubscription7);


    }

    private void showNoInternet() {
        if (isViewAttached()) {
            view.noInternet();
        }
    }


    private void showLoading() {
        if (isViewAttached()) {
            view.loadingStarted();
        }
    }
    private void onFetchSuccess_baluchuri(List<HomeSaree> homeSarees) {
        if (isViewAttached()) {
            view.showSarees(homeSarees,null,null,null,null,null);
        }
    }
    private void onFetchSuccess_DhaniyaKali(List<HomeSaree> homeSarees) {
        if (isViewAttached()) {
            view.showSarees(null,homeSarees,null,null,null,null);
        }
    }
    private void onFetchSuccess_Jaamdani(List<HomeSaree> homeSarees) {
        if (isViewAttached()) {
            view.showSarees(null,null,homeSarees,null,null,null);
        }
    }
    private void onFetchSuccess_Fulia(List<HomeSaree> homeSarees) {
        if (isViewAttached()) {
            view.showSarees(null,null,null,homeSarees,null,null);
        }
    }
    private void onFetchSuccess_BahaCotton(List<HomeSaree> homeSarees) {
        if (isViewAttached()) {
            view.showSarees(null,null,null,null,homeSarees,null);
        }
    }
    private void onFetchSuccess_Shantipur(List<HomeSaree> homeSarees) {
        if (isViewAttached()) {
            view.showSarees(null,null,null,null,null,homeSarees);
        }
    }


    public void onMovieFetchFailed(Throwable e) {
        view.loadingFailed(e.getMessage());
        e.printStackTrace();
    }
    private boolean isViewAttached() {
        return view != null;
    }

    public void loadingFailed(String message) {
        if(isViewAttached())
            view.loadingFailed(message);
    }


}
