package com.karangupta.durgahandloom.main_activity.order_history;

/**
 * Created by karangupta on 05/04/18.
 */

interface OrderHistoryPresenter {
    void destroy();

    void setView(OrderHistory orderHistory);

    void fetchOrdersHistory();
}
