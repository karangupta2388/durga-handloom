package com.karangupta.durgahandloom.main_activity.order_history;

/**
 * Created by karangupta on 05/04/18.
 */

interface OrderHistoryInteractor {
    void fetchOrdersHistory(OrderHistoryPresenterImpl orderHistoryPresenter);
}
