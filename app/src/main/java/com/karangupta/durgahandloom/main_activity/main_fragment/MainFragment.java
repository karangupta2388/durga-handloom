package com.karangupta.durgahandloom.main_activity.main_fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.Constants;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.collections.CollectionsActivity;
import com.karangupta.durgahandloom.main_activity.MainActivity;
import com.karangupta.durgahandloom.util.ChipModel;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.home_model.HomeSaree;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by karangupta on 27/03/18.
 */

public class MainFragment extends Fragment implements MainFragmentView {
    @Inject
    MainFragmentPresenter mainFragmentPresenter;

    @BindView(R.id.recycler_view_baluchuri)
    RecyclerView recycler_view_baluchuri;

    @BindView(R.id.recycler_view_jaamdani)
    RecyclerView recycler_view_jaamdani;

    @BindView(R.id.recycler_view_fulia)
    RecyclerView recycler_view_fulia;

    @BindView(R.id.recycler_dhaniya_kali)
    RecyclerView recycler_dhaniya_kali;

    @BindView(R.id.recycler_view_baha_cotton)
    RecyclerView recycler_view_baha_cotton;

    @BindView(R.id.recycler_view_shantipur_sarees)
    RecyclerView recycler_view_shantipur_sarees;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.layout_baluchuri)
    LinearLayout layout_baluchuri;

    @BindView(R.id.layout_baha_cotton)
    LinearLayout layout_baha_cotton;

    @BindView(R.id.layout_dhaniya_kali)
    LinearLayout layout_dhaniya_kali;

    @BindView(R.id.layout_fulia)
    LinearLayout layout_fulia;

    @BindView(R.id.layout_jaamdani)
    LinearLayout layout_jaamdani;

    @BindView(R.id.layout_shantipur_sarees)
    LinearLayout layout_shantipur_sarees;


    @BindView(R.id.message)
    TextView message;

    private Callback callback;
    private Unbinder unbinder;
    private HomeAdapter adapter_baluchuri, adapter_dhaniya_kali, adapter_jaamdani, adapter_baha_cotton, adapter_shantipur;
    private HomeAdapter_fullScreen adapter_fulia;

    private List<HomeSaree> list_baluchuri = new ArrayList<>();
    private List<HomeSaree> list_dhaniya_kali = new ArrayList<>();
    private List<HomeSaree> list_jaamdani = new ArrayList<>();
    private List<HomeSaree> list_fulia = new ArrayList<>();
    private List<HomeSaree> list_baha_cotton = new ArrayList<>();
    private List<HomeSaree> list_shantipur = new ArrayList<>();
    private int flag = 0;


    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (MainActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
        //Toast.makeText(getActivity(), "MainFragment", Toast.LENGTH_SHORT).show();
        setRetainInstance(true);
        ((BaseApplication) getActivity().getApplication()).createMainFragmentComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.main_fragment2, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        if (callback != null)
            callback.setToolbar(1);
        initLayoutReferences();
        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mainFragmentPresenter.setView(this);
    }

    private void initLayoutReferences() {
        //RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
      /*  (new LinearSnapHelper()).attachToRecyclerView(recycler_view_baluchuri);
        (new LinearSnapHelper()).attachToRecyclerView(recycler_view_jaamdani);
        (new LinearSnapHelper()).attachToRecyclerView(recycler_view_fulia);
        (new LinearSnapHelper()).attachToRecyclerView(recycler_dhaniya_kali);
        (new LinearSnapHelper()).attachToRecyclerView(recycler_view_baha_cotton);
        (new LinearSnapHelper()).attachToRecyclerView(recycler_view_shantipur_sarees);*/

        recycler_view_baluchuri.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_view_jaamdani.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_view_fulia.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_dhaniya_kali.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_view_baha_cotton.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        recycler_view_shantipur_sarees.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));


        adapter_baluchuri = new HomeAdapter(list_baluchuri, this);
        adapter_dhaniya_kali = new HomeAdapter(list_dhaniya_kali, this);
        adapter_jaamdani = new HomeAdapter(list_jaamdani, this);
        adapter_fulia = new HomeAdapter_fullScreen(list_fulia, this);
        adapter_baha_cotton = new HomeAdapter(list_baha_cotton, this);
        adapter_shantipur = new HomeAdapter(list_shantipur, this);


        recycler_view_baluchuri.setAdapter(adapter_baluchuri);
        recycler_view_jaamdani.setAdapter(adapter_jaamdani);
        recycler_view_fulia.setAdapter(adapter_fulia);
        recycler_dhaniya_kali.setAdapter(adapter_dhaniya_kali);
        recycler_view_baha_cotton.setAdapter(adapter_baha_cotton);
        recycler_view_shantipur_sarees.setAdapter(adapter_shantipur);
    }

    @Override
    synchronized public void showSarees(List<HomeSaree> list_baluchuri,
                                        List<HomeSaree> list_dhaniya_kali,
                                        List<HomeSaree> list_jaamdani,
                                        List<HomeSaree> list_fulia,
                                        List<HomeSaree> list_baha_cotton,
                                        List<HomeSaree> list_shantipur) {
        if (list_baluchuri != null) {
            //Toast.makeText(getActivity(), String.valueOf(list_baluchuri.size()), Toast.LENGTH_SHORT).show();
            this.list_baluchuri.clear();
            this.list_baluchuri.addAll(list_baluchuri);
            layout_baluchuri.setVisibility(View.VISIBLE);
            recycler_view_baluchuri.setVisibility(View.VISIBLE);
            adapter_baluchuri.notifyDataSetChanged();
            flag++;
            stopLoading();
        } else if (list_dhaniya_kali != null) {
            this.list_dhaniya_kali.clear();
            this.list_dhaniya_kali.addAll(list_dhaniya_kali);
            layout_dhaniya_kali.setVisibility(View.VISIBLE);
            recycler_dhaniya_kali.setVisibility(View.VISIBLE);
            adapter_dhaniya_kali.notifyDataSetChanged();
            flag++;
            stopLoading();

        } else if (list_jaamdani != null) {
            this.list_jaamdani.clear();
            this.list_jaamdani.addAll(list_jaamdani);
            layout_jaamdani.setVisibility(View.VISIBLE);
            recycler_view_jaamdani.setVisibility(View.VISIBLE);
            adapter_jaamdani.notifyDataSetChanged();
            flag++;
            stopLoading();

        } else if (list_fulia != null) {
            this.list_fulia.clear();
            this.list_fulia.addAll(list_fulia);
            layout_fulia.setVisibility(View.VISIBLE);
            recycler_view_fulia.setVisibility(View.VISIBLE);
            adapter_fulia.notifyDataSetChanged();
            flag++;
            stopLoading();

        } else if (list_baha_cotton != null) {
            this.list_baha_cotton.clear();
            this.list_baha_cotton.addAll(list_baha_cotton);
            layout_baha_cotton.setVisibility(View.VISIBLE);
            recycler_view_baha_cotton.setVisibility(View.VISIBLE);
            adapter_baha_cotton.notifyDataSetChanged();
            flag++;
            stopLoading();
        } else if (list_shantipur != null) {
            this.list_shantipur.clear();
            this.list_shantipur.addAll(list_shantipur);
            layout_shantipur_sarees.setVisibility(View.VISIBLE);
            recycler_view_shantipur_sarees.setVisibility(View.VISIBLE);
            adapter_shantipur.notifyDataSetChanged();
            flag++;
            stopLoading();

        }

    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    private void stopLoading() {
        if (flag == 6) {
            progress_bar.setVisibility(View.GONE);
            flag = 0;
            message.setText("Enjoy!!");
        }

    }

    @Override
    public void loadingFailed(String errorMessage) {
        progress_bar.setVisibility(View.GONE);

        Snackbar.make(recycler_view_baha_cotton, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onHomeSareeClicked(String id) {
        String[] hierchy = Utilities.getHierchy(Utilities.StringId_tointId(id));
        //String[] hierchy=Utilities.getHierchyString(id);
        ArrayList<ChipModel> chipModel_list = Utilities.getChipModelString(id);
        Intent intent = new Intent(getActivity(), CollectionsActivity.class);
        intent.putParcelableArrayListExtra(Constants.CHIP_MODEL_LIST, chipModel_list);
        intent.putExtra(Constants.HIERCHY, hierchy);
        intent.putExtra(Constants.ID, Utilities.StringId_tointId(id));
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

    }


    @Override
    public void noInternet() {
        Snackbar.make(recycler_view_baha_cotton, R.string.no_internet, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void displayCount(String[] count) {
        callback = (MainActivity) getActivity();
        if (callback != null) {
            //Toast.makeText(getActivity(), "Callback!=null", Toast.LENGTH_SHORT).show();
            if (count[1] != null) {
                //Toast.makeText(getActivity(), "count[1]!=null && badge_count!=null", Toast.LENGTH_SHORT).show();
                if (count[1].equals("0")) {
                    callback.setBatchCount("0");

                } else {
                    // badge_count.setVisibility(View.VISIBLE);
                    // badge_count.setText(count[1]);
                    callback.setBatchCount(count[1]);

                }

            }
        }
    }

    @Override
    public void onStart() {
        mainFragmentPresenter.displayCount();
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mainFragmentPresenter.destroy();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        callback = null;
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ((BaseApplication) getActivity().getApplication()).releaseMainFragmentComponent();
    }


  /*  public void retrieveUser(){
        Query query = FirebaseDatabase.getInstance().getReference().child("user_ref").orderByChild("id").equalTo(0);;
        Maybe<PublicUserData> publicUserDataMaybe1 = RxFirebaseDatabase.observeSingleValueEvent(query, PublicUserData.class);
publicUserDataMaybe1.map(PublicUserData::getUsers).
        Maybe<PublicUserData> publicUserDataMaybe =  (Maybe<PublicUserData>)RxFirebaseDatabase.observeSingleValueEvent(query, PublicUserData.class);
        publicUserDataMaybe.map(Users->{return Users})
                .subscribe(userData -> updateUserUi(userData),
                        throwable -> manageError(throwable));
    }*/


    public interface Callback {
        void setBatchCount(String count);

        void setToolbar(int flag);
    }
}
