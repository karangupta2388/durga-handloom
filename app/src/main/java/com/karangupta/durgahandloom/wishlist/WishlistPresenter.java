package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 04/04/18.
 */

interface WishlistPresenter {

    void setView(Wishlist wishlist);

    void askPresenter_displayCount();

    void fetchFavouriteViewsfromPresenter();

    void removefromWishlist(CartModel fo4);

    void addtoCart(CartModel fo4);

    void destroy();
}
