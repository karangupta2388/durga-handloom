package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 04/04/18.
 */

class WishlistInteractorImpl implements WishlistInteractor {

    FirebaseStore firebaseStore;
    public WishlistInteractorImpl(FirebaseStore firebaseStore) {
        this.firebaseStore=firebaseStore;
    }

    @Override
    public void removefromWishlist(CartModel fo4, WishlistPresenterImpl wishlistPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.removefromWishlist(fo4, wishlistPresenter);
        }
    }

    @Override
    public void addtoCart(CartModel fo4, String cart, WishlistPresenterImpl wishlistPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.addtoCart(fo4, cart, wishlistPresenter);
        }
    }

    @Override
    public void fetchFavouriteViews(WishlistPresenterImpl wishlistPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.fetchFavouriteViews(wishlistPresenter);
        }
    }
}
