package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.cart.Cart;
import com.karangupta.durgahandloom.cart.CartModule;
import com.karangupta.durgahandloom.cart.CartScope;

import dagger.Subcomponent;

/**
 * Created by karangupta on 04/04/18.
 */

@WishlistScope
@Subcomponent(modules = {WishlistModule.class})
public interface WishlistComponent
{
    void inject(Wishlist target);
}