package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

/**
 * Created by karangupta on 04/04/18.
 */

interface WishlistView {
    void loadingStarted();

    void stopLoading();

    void displayCount(String o, String  o1, String[] countValuefromSharedPrefs);

    void displayFavouritesView(List<CartModel> present_list);

    void displayMovedtoCart();

    void loadingFailed(String errorMessage,String countAddorRemove,String cart_or_favourites);

    void displayEmptyList();

    void displayRemovedFromWishlist();

    void startLoadingAnim();

    void stopLoadingAnim();

    void noInternet();
}
