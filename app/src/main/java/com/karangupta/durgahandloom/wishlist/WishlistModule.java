package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 04/04/18.
 */
@Module
public class WishlistModule {

    @Provides
    WishlistInteractor provideWishlistInteractor(FirebaseStore firebaseStore) {
        return new WishlistInteractorImpl(firebaseStore);
    }

    @Provides
  WishlistPresenter provideWishlistPresenter(ConnectionDetector connectionDetector, WishlistInteractor wishlistInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
        return new WishlistPresenterImpl(connectionDetector,wishlistInteractor,localStorageStoreInteractor);
    }
}
