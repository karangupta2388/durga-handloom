package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 04/04/18.
 */

interface WishlistInteractor {
    void removefromWishlist(CartModel fo4, WishlistPresenterImpl wishlistPresenter);

    void addtoCart(CartModel fo4, String cart, WishlistPresenterImpl wishlistPresenter);

    void fetchFavouriteViews(WishlistPresenterImpl wishlistPresenter);
}
