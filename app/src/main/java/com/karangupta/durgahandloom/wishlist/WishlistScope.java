package com.karangupta.durgahandloom.wishlist;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 04/04/18.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface WishlistScope {}
