package com.karangupta.durgahandloom.wishlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.Constants;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.cart.Cart;
import com.karangupta.durgahandloom.details.Details;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.cart_model.CartModel;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by karangupta on 04/04/18.
 */

public class Wishlist extends AppCompatActivity implements WishlistView {
    @Inject
    WishlistPresenter wishlistPresenter;

    @BindView(R.id.badge_count)
    TextView badge_count;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recycler_view_views)
    RecyclerView recycler_view_views;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.heart)
    LinearLayout heart;

    @BindView(R.id.cart_badgeCombo)
    RelativeLayout cart_badgeCombo;

    @BindView(R.id.loading_frame_layout)
    FrameLayout loading_frame_layout;

    @BindView(R.id.loadingAVI)
    AVLoadingIndicatorView loadingAVI;

    private Unbinder unbinder;
    private WishlistAdapter wishlistAdapter;

    List<CartModel> cartModel_List =new ArrayList<>();

    public Wishlist() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.wishlist_activity);
        unbinder= ButterKnife.bind(this);
        setToolbar();
        ((BaseApplication)getApplication()).createWishlistComponent().inject(this);
        wishlistPresenter.setView(Wishlist.this);
        cart_badgeCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Wishlist.this,Cart.class));
                overridePendingTransition(R.anim.slide_to_top,0);

            }
        });
       /* cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/

        wishlistPresenter.askPresenter_displayCount();
        initAdapter();
        wishlistPresenter.fetchFavouriteViewsfromPresenter();
        // new Intent(this, MainActivity.class).putParcelableArrayListExtra(Constants.CHIP_MODEL,chipModel_list);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void setToolbar() {
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);//cool
        //setDropdown_Height(0);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("WISHLIST");
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        heart.setVisibility(View.INVISIBLE);
    }

    private void initAdapter() {

        //for Views recycler view
        final GridLayoutManager gridLayoutManager=new GridLayoutManager(this, 2);
        recycler_view_views.setLayoutManager(gridLayoutManager);
        wishlistAdapter = new WishlistAdapter(cartModel_List,this );
        recycler_view_views.setAdapter(wishlistAdapter);

    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public  void stopLoading()
    {
        progress_bar.setVisibility(View.GONE);

    }

    @Override
    public void startLoadingAnim() {
        loadingAVI.show();
        loading_frame_layout.setVisibility(View.VISIBLE);

        //startAnim();
    }
    @Override
    public void stopLoadingAnim() {
        loadingAVI.hide();
        loading_frame_layout.setVisibility(View.GONE);

        //stopAnim();

    }

    @Override
    public void displayFavouritesView(List<CartModel> cartModel_List) {
        text.setVisibility(View.GONE);
        image.setVisibility(View.GONE);
        stopLoadingAnim();
        stopLoading();
        this.cartModel_List.clear();
        this.cartModel_List.addAll(cartModel_List);
        recycler_view_views.post(new Runnable() {
            public void run() {
                wishlistAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void displayEmptyList() {
        stopLoadingAnim();
        stopLoading();
        text.setVisibility(View.VISIBLE);
        image.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayRemovedFromWishlist() {
        stopLoadingAnim();
        Snackbar.make(recycler_view_views,"Removed from Wishlist",Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void displayMovedtoCart() {
        stopLoadingAnim();
        Snackbar.make(recycler_view_views,"Moved to Cart",Snackbar.LENGTH_LONG).show();
    }



    //From CartAdapter
    public void removefromWishlist(CartModel fo4) {

        wishlistPresenter.removefromWishlist(fo4);
    }
    //From CartAdapter
    public void addtoCart(CartModel fo4) {
        wishlistPresenter.addtoCart(fo4);
    }
    @Override
    public void displayCount(String add_or_remove, String cart_or_favourite, String[] count) {
        if (cart_or_favourite != null && add_or_remove!=null)
        {
            if(add_or_remove.equals("add"))
                Snackbar.make(recycler_view_views, "Added to " + cart_or_favourite, Snackbar.LENGTH_SHORT).show();
            else
                Snackbar.make(recycler_view_views, "Removed from " + cart_or_favourite, Snackbar.LENGTH_SHORT).show();

        }

        if (count[1] != null)
        {
            // Toast.makeText(getActivity(), "count[1]!=null && badge_count!=null", Toast.LENGTH_SHORT).show();
            if (count[1].equals("0")) {
                badge_count.setVisibility(View.GONE);
            } else {
                badge_count.setText(count[1]);
                badge_count.setVisibility(View.VISIBLE);
            }
        }

    }










    @Override
    public void noInternet() {
        Snackbar.make(recycler_view_views, R.string.no_internet, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void loadingFailed(String errorMessage,String countAddorRemove,String cart_or_favourites) {
        progress_bar.setVisibility(View.GONE);
        Snackbar.make(recycler_view_views, "Something went wrong.", Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        wishlistPresenter.askPresenter_displayCount();
        super.onStart();
    }
    @Override
    protected void onDestroy() {
        wishlistPresenter.destroy();
        unbinder.unbind();
        ((BaseApplication) getApplication()).releaseWishlistComponent();
        super.onDestroy();
    }

    public void imageClicked(CartModel fo4,ImageView image_view_show_card) {

        ArrayList<String> list= Utilities.geEachUrl2(fo4.getUrl());
        Intent intent=new Intent(Wishlist.this, Details.class);
        intent.putStringArrayListExtra(Constants.URLs,list);

        intent.putExtra(Constants.URL,fo4.getUrl());
        intent.putExtra(Constants.SERIES_NAME,fo4.getSeries_Name());
        intent.putExtra(Constants.CODE,fo4.getCode());
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, (View)image_view_show_card, "your_shared_transition_name");
        startActivity(intent, options.toBundle());
        //startActivity(intent);

    }
}
