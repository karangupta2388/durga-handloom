package com.karangupta.durgahandloom.wishlist;

import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

/**
 * Created by karangupta on 04/04/18.
 */

public class WishlistPresenterImpl implements WishlistPresenter {
    private ConnectionDetector connectionDetector;
    private WishlistInteractor wishlistInteractor;
    private LocalStorageStoreInteractor localStorageStoreInteractor;
    private WishlistView view;
    private List<CartModel> present_list=null;//list that is presently visible


    public WishlistPresenterImpl(ConnectionDetector connectionDetector, WishlistInteractor wishlistInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
   this.connectionDetector=connectionDetector;
   this.localStorageStoreInteractor=localStorageStoreInteractor;
   this.wishlistInteractor=wishlistInteractor;
    }



    @Override
    public void setView(Wishlist wishlist) {
        this.view=wishlist;

    }

    @Override
    public void askPresenter_displayCount() {
        if(isViewAttached())
            view.displayCount(null,null,localStorageStoreInteractor.getCountValueFromSharedPrefs());
    }

    @Override
    public void fetchFavouriteViewsfromPresenter() {
        if(connectionDetector.isConnectingToInternet()) {
            view.loadingStarted();
            wishlistInteractor.fetchFavouriteViews(this);
        }else
        {
            if(isViewAttached())
            {
                view.noInternet();
            }
        }
    }





    @Override
    public void addtoCart(CartModel fo4) {
        if(connectionDetector.isConnectingToInternet()) {
            if (isViewAttached())
                view.startLoadingAnim();
            wishlistInteractor.addtoCart(fo4, "Cart", this);
        }else
        {
            if(isViewAttached())
            {
                view.noInternet();
            }
        }

    }

    @Override
    public void removefromWishlist(CartModel fo4) {
        if(connectionDetector.isConnectingToInternet()) {
            if (isViewAttached())
                view.startLoadingAnim();
            wishlistInteractor.removefromWishlist(fo4, this);
        }else
        {
            if(isViewAttached())
            {
                view.noInternet();
            }
        }

    }

    //After removing from wishlist
    public void countUpdatedWISHLIST(CartModel fo4, String remove, String scount, String favourites) {
        localStorageStoreInteractor.setCountValueToSharedPrefs(scount,favourites);
        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(),remove,favourites);
        if(present_list.contains(fo4))
            present_list.remove(fo4);
        if(isViewAttached())
            view.displayRemovedFromWishlist();
        displayFavouriteViews(present_list);

    }

    public void displayFavouriteViews(List<CartModel> present_list) {
        this.present_list=present_list;
        view.stopLoading();
        if(isViewAttached())
        {

            if(present_list==null )
            {
                view.displayEmptyList();
            }else
            {

                this.present_list=present_list;
                view.stopLoadingAnim();
                view.stopLoading();
                view.displayFavouritesView(present_list);
                if(present_list.isEmpty())
                {
                    view.displayEmptyList();
                }
            }
        }


    }

    public void countUpdatedforFavourites_andCart(CartModel fo4, String add_or_remove, String count1,String count2, String cart_or_favourites) {
        localStorageStoreInteractor.setCountValueToSharedPrefs(count2,"Cart");//setting count value for Cart in Sharedprefs
        localStorageStoreInteractor.setCountValueToSharedPrefs(count1,"Favourites");//setting count value for Favoutite in Sharedprefs

        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(),"remove","Favourites");
        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(),"add","Cart");


        if(isViewAttached())
            view.displayMovedtoCart();
        if(present_list.contains(fo4))
            present_list.remove(fo4);
        displayFavouriteViews(present_list);

    }

    public void loadingFailed(String message,String countAddorRemove,String cart_or_favourites)
    {
        if(isViewAttached())
            view.loadingFailed(message,countAddorRemove,cart_or_favourites);
    }
    private boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void destroy() {
        view = null;
    }
}
