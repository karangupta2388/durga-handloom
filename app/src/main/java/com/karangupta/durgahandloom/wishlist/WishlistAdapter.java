package com.karangupta.durgahandloom.wishlist;

/**
 * Created by karangupta on 02/04/18.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.ArrayList;
import java.util.List;

import customfonts.MyTextView;


class WishlistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<CartModel> list;
    Wishlist wishlist;
    private Context context;

    public WishlistAdapter(List<CartModel> viewsModel_list, Wishlist wishlist) {
        this.wishlist=wishlist;
        this.list= viewsModel_list;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        return (new WishlistAdapter.RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favoutites_view, parent, false)));

    }
    public int getItemCount() {

        return (list.size());
    }
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((WishlistAdapter.RowController) holder).bindModel(list.get(position),position);
    }
    private    class RowController extends RecyclerView.ViewHolder
    {
        ImageView image_view_show_card=null;
        MyTextView title=null;
        MyTextView sub_title=null;
        LinearLayout trash;
        LinearLayout move_to_cart;

        View row=null;
        private RowController(View row) {
            super(row);
            this.row=row;
            this.title=((MyTextView)row.findViewById(R.id.title));
            this.sub_title=((MyTextView)row.findViewById(R.id.sub_title));
            this.image_view_show_card=((ImageView)row.findViewById(R.id.image_view_show_card));
            this.trash=((LinearLayout)row.findViewById(R.id.trash));
            this.move_to_cart=((LinearLayout)row.findViewById(R.id.move_to_cart));
            this.image_view_show_card.getLayoutParams().width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);
            this.image_view_show_card.getLayoutParams().height = (int) ((context.getResources().getDisplayMetrics().widthPixels * 0.96) / 1.40);

        }

        private void bindModel(final CartModel fo4, int position) {
            int px1=(int) Utilities.convertDpToPixel(0.5f,context);//0.5 dp to pixel
            int px2=(int)Utilities.convertDpToPixel(2,context);//1 dp to pixel
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) row.getLayoutParams();

            if(position%2==0)
            {
                params.rightMargin = px1; params.topMargin = px1; params.bottomMargin = px1;
            }else
            {
                params.leftMargin = px1; params.topMargin = px1; params.bottomMargin = px1;

            }

            if (title != null && sub_title!=null) {
                if (fo4.getSeries_Name() != null && !fo4.getSeries_Name().equals("")) {
                    String stitle=fo4.getSeries_Name().substring(0,fo4.getSeries_Name().indexOf(' '));
                    String ssub_title=fo4.getSeries_Name().substring(fo4.getSeries_Name().indexOf(' ')+1,fo4.getSeries_Name().length());
                    StringBuilder sb=new StringBuilder(ssub_title);
                    try { String ssub1=ssub_title.substring(0,ssub_title.indexOf(' ')+1);
                        String ssub2=ssub_title.substring(ssub_title.indexOf(' ')+1,sb.length());
                        ssub_title=ssub1+"\n"+ssub2;}catch (Exception e){e.printStackTrace();}
                    title.setText(stitle);
                    sub_title.setText(ssub_title);
                    title.setVisibility(View.VISIBLE);
                    sub_title.setVisibility(View.VISIBLE);
                } else {
                    title.setVisibility(View.GONE);
                    sub_title.setVisibility(View.GONE); }

            }


            row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //   ViewsAdapter.this.collectionsActivity.onViewClicked(list.get(getAdapterPosition()).getId());

                }
            });


            if(image_view_show_card!=null) {
                image_view_show_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        wishlist.imageClicked(fo4,image_view_show_card);
                    }
                });
            }


            if (image_view_show_card != null) {

                image_view_show_card.setImageResource(android.R.color.transparent);
                image_view_show_card.setVisibility(View.VISIBLE);
                ArrayList<String> list= Utilities.geEachUrl2(fo4.getUrl());
                Glide.with(context).load(list.get(0))
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(image_view_show_card);


            }else
            {
                image_view_show_card.setImageResource(android.R.color.transparent);
                image_view_show_card.setVisibility(View.INVISIBLE);
            }


            trash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    wishlist.removefromWishlist(fo4);

                }
            });

            move_to_cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    wishlist.addtoCart(fo4);

                }
            });

        }
    }


}
