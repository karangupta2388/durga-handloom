package com.karangupta.durgahandloom.order_detail;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

import customfonts.MyTextView;

/**
 * Created by karangupta on 06/04/18.
 */

class OrderDetailAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<CartModel> list;
    OrderDetail orderDetail;
    private Context context;

    public OrderDetailAdapter(List<CartModel> list, OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
        this.list = list;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();

        return (new OrderDetailAdapter.RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_detail_views, parent, false)));

    }

    public int getItemCount() {

        return (list.size());
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ((OrderDetailAdapter.RowController) holder).bindModel(list.get(position));
    }

    private class RowController extends RecyclerView.ViewHolder {

        ImageView imagee;
        MyTextView name;
        MyTextView quantity;
        //MyTextView code;



        View row = null;

        private RowController(View row) {
            super(row);
            this.row = row;
            this.name = ((MyTextView) row.findViewById(R.id.name));
            this.quantity = ((MyTextView) row.findViewById(R.id.quantity));
            this.imagee = ((ImageView) row.findViewById(R.id.imagee));
            //this.code=((MyTextView)row.findViewById(R.id.code));
            this.imagee.getLayoutParams().width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);
            this.imagee.getLayoutParams().height =  this.imagee.getLayoutParams().width;


        }

        private void bindModel(final CartModel fo4) {

            if (name != null) {
                if (fo4.getSeries_Name() != null && !fo4.getSeries_Name().equals("")) {
                    name.setText(fo4.getSeries_Name());
                    name.setVisibility(View.VISIBLE);
                } else {
                    name.setVisibility(View.GONE);
                }

            }

            /*if (code != null) {
                if (fo4.getCode() != null && !fo4.getCode().equals("")) {
                    code.setText(fo4.getCode());
                    code.setVisibility(View.VISIBLE);
                } else {
                    code.setVisibility(View.GONE);
                }

            }*/

            if (quantity != null) {
                if (fo4.getQuantity() != null && !fo4.getQuantity().equals("")) {
                    quantity.setText(fo4.getQuantity());
                    quantity.setVisibility(View.VISIBLE);
                } else {
                    quantity.setVisibility(View.GONE);
                }

            }


            imagee.setVisibility(View.GONE);
            /*if (imagee != null) {

                imagee.setImageResource(android.R.color.transparent);
                imagee.setVisibility(View.VISIBLE);

                Glide.with(context).load(fo4.getUrl())
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imagee);

            }else
            {
                imagee.setImageResource(android.R.color.transparent);
                imagee.setVisibility(View.INVISIBLE);
            }*/

        }
    }
}
