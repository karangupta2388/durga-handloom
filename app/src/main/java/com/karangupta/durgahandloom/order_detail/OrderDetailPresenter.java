package com.karangupta.durgahandloom.order_detail;

/**
 * Created by karangupta on 06/04/18.
 */

interface OrderDetailPresenter {
    void setView(OrderDetail orderDetail);

    void fetchOrdersDeatil(String order_id);

    void destroy();
}
