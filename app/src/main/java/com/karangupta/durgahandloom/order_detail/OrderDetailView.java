package com.karangupta.durgahandloom.order_detail;

import com.karangupta.durgahandloom.util.order_detail_model.OrderDetailModel;

/**
 * Created by karangupta on 06/04/18.
 */

interface OrderDetailView {
    void loadingStarted();

    void loadingFailed(String message);

    void displayOrderDeatils(OrderDetailModel orderDetailModel);

    void noInternet();
}
