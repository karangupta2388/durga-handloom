package com.karangupta.durgahandloom.order_detail;

import dagger.Subcomponent;

/**
 * Created by karangupta on 06/04/18.
 */



@OrderDetailScope
@Subcomponent(modules = {OrderDetailModule.class})
public interface OrderDetailComponent
{
    void inject(OrderDetail target);
}