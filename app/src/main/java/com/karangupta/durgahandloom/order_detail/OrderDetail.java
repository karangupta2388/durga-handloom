package com.karangupta.durgahandloom.order_detail;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.cart_model.CartModel;
import com.karangupta.durgahandloom.util.order_detail_model.OrderDetailModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by karangupta on 06/04/18.
 */

public class OrderDetail extends AppCompatActivity implements OrderDetailView {

    @Inject
    OrderDetailPresenter orderDetailPresenter;

    @BindView(R.id.recycler_view_order_deatils)
    RecyclerView recycler_view_order_deatils;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    private Unbinder unbinder;
    private OrderDetailAdapter orderDetailAdapter;

    private List<CartModel> orderDetailModelList = new ArrayList<>();


    public OrderDetail() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.order_details);
        unbinder= ButterKnife.bind(this);
        setToolbar();
        ((BaseApplication)getApplication()).createOrderDetailComponent().inject(this);
        orderDetailPresenter.setView(OrderDetail.this);
String order_id=getIntent().getStringExtra("Order Id");
getIntent().removeExtra("Order Id");
        initLayoutReferences();
        orderDetailPresenter.fetchOrdersDeatil(order_id);
    }

    private void setToolbar() {
    }
    private void initLayoutReferences() {
        /*final GridLayoutManager gridLayoutManager=new GridLayoutManager(this, 2);
        recycler_view_order_deatils.setLayoutManager(gridLayoutManager);
        orderDetailAdapter = new OrderDetailAdapter(orderHistoryModelList,this );
        recycler_view_order_deatils.setAdapter(orderDetailAdapter);*/

        recycler_view_order_deatils.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        orderDetailAdapter = new OrderDetailAdapter(orderDetailModelList,this );
        recycler_view_order_deatils.setAdapter(orderDetailAdapter);
    }

    @Override
    synchronized public void displayOrderDeatils(OrderDetailModel orderDetailModel) {

        stopLoading();
        this.orderDetailModelList.clear();

        this.orderDetailModelList.addAll(orderDetailModel.getCart());
        orderDetailAdapter.notifyDataSetChanged();

    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }
    private  void stopLoading()
    {

        progress_bar.setVisibility(View.GONE);

    }
    @Override
    public void loadingFailed(String errorMessage) {
        progress_bar.setVisibility(View.GONE);
        Snackbar.make(recycler_view_order_deatils, "Something Went Wrong", Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void noInternet() {
        Snackbar.make(recycler_view_order_deatils, R.string.no_internet, Snackbar.LENGTH_LONG).show();

    }



    @Override
    public void onDestroy() {
        orderDetailPresenter.destroy();
        unbinder.unbind();
        ((BaseApplication)getApplication()).releaseOrderDetailComponent();
        super.onDestroy();

    }
}
