package com.karangupta.durgahandloom.order_detail;

import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.order_detail_model.OrderDetailModel;

/**
 * Created by karangupta on 06/04/18.
 */

public class OrderDetailPresenterImpl implements OrderDetailPresenter {

    private OrderDetailView view;

    ConnectionDetector connectionDetector;
    OrderDetailInteractor orderHistoryInteractor;
    public OrderDetailPresenterImpl(ConnectionDetector connectionDetector, OrderDetailInteractor orderHistoryInteractor) {
        this.orderHistoryInteractor=orderHistoryInteractor;
        this.connectionDetector=connectionDetector;
    }

    @Override
    public void destroy() {
        view = null;
        this.view = view;

    }



    @Override
    public void setView(OrderDetail orderDetail) {
        view=orderDetail;
    }

    @Override
    public void fetchOrdersDeatil(String order_id) {
        if(connectionDetector.isConnectingToInternet()) {
            if (isViewAttached())
                view.loadingStarted();
            orderHistoryInteractor.fetchOrdersDetail(order_id, this);
        }else
        {
            if(isViewAttached())
            {
                view.noInternet();
            }
        }
    }

    private boolean isViewAttached() {
        return view != null;
    }

    public void loadingFailed(String message) {
        if(isViewAttached())
            view.loadingFailed(message);
    }

    public void displayOrderDeatils(OrderDetailModel orderDetailModel) {
        if(isViewAttached()) {
            view.displayOrderDeatils(orderDetailModel);


        }
    }
}
