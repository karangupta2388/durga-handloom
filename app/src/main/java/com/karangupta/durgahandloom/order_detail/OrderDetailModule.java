package com.karangupta.durgahandloom.order_detail;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 06/04/18.
 */
@Module
public class OrderDetailModule {
    @Provides
    OrderDetailInteractor provideOrderDetailInteractor(FirebaseStore firebaseStore) {
        return new OrderDetailInteractorImpl(firebaseStore);
    }

    @Provides
    OrderDetailPresenter provideOrderDetailPresenter(ConnectionDetector connectionDetector, OrderDetailInteractor orderHistoryInteractor) {
        return new OrderDetailPresenterImpl(connectionDetector,orderHistoryInteractor);
    }
}
