package com.karangupta.durgahandloom.order_detail;

import com.karangupta.durgahandloom.firebase.FirebaseStore;

/**
 * Created by karangupta on 06/04/18.
 */

class OrderDetailInteractorImpl implements OrderDetailInteractor {
    FirebaseStore firebaseStore;
    public OrderDetailInteractorImpl(FirebaseStore firebaseStore) {
        this.firebaseStore=firebaseStore;
    }


    @Override
    public void fetchOrdersDetail(String order_id, OrderDetailPresenterImpl orderDetailPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.fetchOrdersDetail(order_id, orderDetailPresenter);
        }
    }
}
