package com.karangupta.durgahandloom.order_detail;

/**
 * Created by karangupta on 06/04/18.
 */

interface OrderDetailInteractor {

    void fetchOrdersDetail(String order_id, OrderDetailPresenterImpl orderDetailPresenter);
}
