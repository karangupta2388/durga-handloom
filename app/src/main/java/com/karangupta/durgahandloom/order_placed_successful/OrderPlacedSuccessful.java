package com.karangupta.durgahandloom.order_placed_successful;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.main_activity.MainActivity;
import com.karangupta.durgahandloom.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customfonts.MyTextView;

/**
 * Created by karangupta on 04/04/18.
 */

public class OrderPlacedSuccessful extends AppCompatActivity {

    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;

    @BindView(R.id.back_to_home)
    MyTextView back_to_home;

    @BindView(R.id.appbar_linear_layout)
    LinearLayout appbar_linear_layout;

    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.order_placed_successful);
        Utilities.hideKeyboard(this);
        unbinder = ButterKnife.bind(this);
        setToolbar();
        back_to_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OrderPlacedSuccessful.this, MainActivity.class));
                finish();
            }
        });
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayShowTitleEnabled(false);//cool
        //setDropdown_Height(0);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("RECEIPT");
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setLogo(R.drawable.home24dp);
        }
        appbar_linear_layout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(OrderPlacedSuccessful.this, MainActivity.class));
        finish();
    }
}
