package com.karangupta.durgahandloom.util.views_model;

/**
 * Created by karangupta on 29/03/18.
 */

public class ViewsModel {
    String Code;
    String Url;
    String Series_Name;

    ViewsModel()
    {

    }
    public ViewsModel(String code, String url, String series_Name) {
        Code = code;
        Url = url;
        Series_Name = series_Name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getSeries_Name() {
        return Series_Name;
    }

    public void setSeries_Name(String series_Name) {
        Series_Name = series_Name;
    }
}
