package com.karangupta.durgahandloom.util.count_model;

/**
 * Created by karangupta on 29/03/18.
 */

public class CountModel {
    String Favourites;
    String Cart;
    public CountModel()
    {

    }
    public CountModel(String favourites, String cart) {
        Favourites = favourites;
        Cart = cart;
    }

    public String getFavourites() {
        return Favourites;
    }

    public void setFavourites(String favourites) {
        Favourites = favourites;
    }

    public String getCart() {
        return Cart;
    }

    public void setCart(String cart) {
        Cart = cart;
    }
}
