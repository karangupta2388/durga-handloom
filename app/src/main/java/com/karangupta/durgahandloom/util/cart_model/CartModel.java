package com.karangupta.durgahandloom.util.cart_model;

/**
 * Created by karangupta on 03/04/18.
 */

public class CartModel {
    String Code;
    String Url;
    String Series_Name;
    String Quantity;

    CartModel()
    {

    }
    public CartModel(String code, String url, String series_Name,String Quantity) {
        Code = code;
        Url = url;
        Series_Name = series_Name;
        this.Quantity=Quantity;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getSeries_Name() {
        return Series_Name;
    }

    public void setSeries_Name(String series_Name) {
        Series_Name = series_Name;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }
}