package com.karangupta.durgahandloom.util.cart_model;

import com.google.firebase.database.DataSnapshot;
import com.karangupta.durgahandloom.util.views_model.ViewsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karangupta on 03/04/18.
 */

public class CartModelWrapper  {
    synchronized public static List<CartModel> getCartModelList(DataSnapshot dataSnapshot) {
        List<CartModel> list=new ArrayList<>();
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            list.add(dataSnapshot1.getValue(CartModel.class));
        }

        return list;
    }
    synchronized public  static CartModel getCartModel_fromViewsModel(ViewsModel fo4)
    {
        return new CartModel(fo4.getCode(),fo4.getUrl(),fo4.getSeries_Name(),"5");//default quantity is 5
    }

    synchronized public static String getTotalQuantity(DataSnapshot dataSnapshot) {
        int total=0;
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            String quantity=(((CartModel)dataSnapshot1.getValue(CartModel.class)).getQuantity());
            //String quantity1=quantity.substring(0,quantity.indexOf(' '));
            total=total+Integer.valueOf(quantity);
        }

        return String.valueOf(total);
    }

    synchronized public static  List<String> getCodes(DataSnapshot dataSnapshot) {
        List<String> list=new ArrayList<>();
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            list.add((dataSnapshot1.getValue(CartModel.class)).getCode());
        }
        return list;
    }

    synchronized public static  String getCount(DataSnapshot dataSnapshot) {
        List<CartModel> list=new ArrayList<>();
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            list.add(dataSnapshot1.getValue(CartModel.class));
        }
        return String.valueOf(list.size());
    }
}
