package com.karangupta.durgahandloom.util;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by karangupta on 29/03/18.
 */

public class ChipModel implements Parcelable
{
    String hierchy1;
    String hierchy2;
    String hierchy3;

    public ChipModel(String hierchy1, String hierchy2, String hierchy3) {
        this.hierchy1 = hierchy1;
        this.hierchy2 = hierchy2;
        this.hierchy3 = hierchy3;
    }

    public ChipModel(Parcel in) {
        hierchy1 = in.readString();
        hierchy2 = in.readString();
        hierchy3 = in.readString();
    }

    public String getHierchy1() {
        return hierchy1;
    }

    public String getHierchy2() {
        return hierchy2;
    }

    public String getHierchy3() {
        return hierchy3;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(hierchy1);
        parcel.writeString(hierchy2);
        parcel.writeString(hierchy3);

    }
    public static final Creator<ChipModel> CREATOR = new Creator<ChipModel>()
    {
        @Override
        public ChipModel createFromParcel(Parcel in)
        {
            return new ChipModel(in);
        }

        @Override
        public ChipModel[] newArray(int size)
        {
            return new ChipModel[size];
        }
    };
}
