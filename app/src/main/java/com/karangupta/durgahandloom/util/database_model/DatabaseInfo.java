package com.karangupta.durgahandloom.util.database_model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by karangupta on 04/04/18.
 */





    @Table(name = "DatabaseInfo")
    public class DatabaseInfo extends Model {
        @Column(name = "Firstname")
        public String firstname;

        @Column(name = "Secondname")
        public String secondname;

        @Column(name = "Address")
        public String address;

        @Column(name = "Pin")
        public String pin;

        @Column(name = "City")
        public String city;

        @Column(name = "Phone")
        public String phone;

        public DatabaseInfo(String firstname, String secondname, String address, String pin, String city, String phone) {
            this.firstname = firstname;
            this.secondname = secondname;
            this.address = address;
            this.pin = pin;
            this.city = city;
            this.phone = phone;
        }

       public DatabaseInfo()
        {

        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getSecondname() {
            return secondname;
        }

        public void setSecondname(String secondname) {
            this.secondname = secondname;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPin() {
            return pin;
        }

        public void setPin(String pin) {
            this.pin = pin;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }
    }



