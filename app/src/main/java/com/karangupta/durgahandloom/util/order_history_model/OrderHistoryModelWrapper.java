package com.karangupta.durgahandloom.util.order_history_model;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karangupta on 04/04/18.
 */

public class OrderHistoryModelWrapper {
    //for setting up Order history on Order being placed
    public synchronized static OrderHistoryModel getOrderHistoryModell(String order_id,String FirstName,
             String secondname,
             String address,
             String pin,
             String city,String phone,String uid,String date,String quantity)
    {

       return new OrderHistoryModel(order_id,FirstName,secondname,address,pin,city,phone,uid,date,quantity);

    }

    //for fetching ordr history to view
    public synchronized static List<OrderHistoryModel> getOrderHistoryModeList(DataSnapshot dataSnapshot)
    {
        List<OrderHistoryModel> list=new ArrayList<>();
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            list.add(dataSnapshot1.getValue(OrderHistoryModel.class));
        }
        return list;
    }

}
