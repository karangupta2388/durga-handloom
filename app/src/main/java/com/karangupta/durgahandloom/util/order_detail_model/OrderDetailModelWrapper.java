package com.karangupta.durgahandloom.util.order_detail_model;

import com.google.firebase.database.DataSnapshot;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

/**
 * Created by karangupta on 04/04/18.
 */

public class OrderDetailModelWrapper {
    //orderId,  firstName,  secondname,  address,  pin,  city,  phone,  UId,  date,  quantity
    public synchronized static OrderDetailModel getOrderDetailModel(String orderId, String firstName, String secondname, String address, String pin, String city, String phone, String uId,String date, String quantity, List<CartModel> cart)
    {
        return new OrderDetailModel(orderId,firstName,secondname,address,pin,city,phone,uId,date,quantity,cart);
    }

    public synchronized static OrderDetailModel getOrderDetailModeltoView(DataSnapshot dataSnapshot)
    {
        OrderDetailModel orderDetailModel=null;
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            orderDetailModel= (OrderDetailModel)dataSnapshot1.getValue(OrderDetailModel.class);
        }
        return orderDetailModel;
    }
}
