package com.karangupta.durgahandloom.util.views_model;

import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karangupta on 29/03/18.
 */

public class ViewsModelWrapper {
    synchronized public static List<ViewsModel> getCountModel(DataSnapshot dataSnapshot) {
        List<ViewsModel> list=new ArrayList<>();
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            list.add(dataSnapshot1.getValue(ViewsModel.class));
        }

        return list;
    }
   /* synchronized public static String getFavouritesCount(DataSnapshot dataSnapshot) {
        String favourites_count=null;
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            favourites_count=(String) dataSnapshot1.getValue();
        }

        return favourites_count;
    }
    synchronized public static String getCartCount(DataSnapshot dataSnapshot) {
        String cart_count=null;
        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
            cart_count=(String) dataSnapshot1.getValue();
        }

        return cart_count;
    } */

}
