package com.karangupta.durgahandloom.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by karangupta on 27/03/18.
 */

public class Utilities {

    public static void setView_Height(final View view, int val) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = val;
        view.setLayoutParams(layoutParams);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static String[] getHierchy(int id) {
        String hierchy2[] = new String[2];
        String hierchy3[] = new String[3];

        if (id == R.id._1 || id == R.id._17) {
            if (id == R.id._1) {
                hierchy3[0] = "Baluchuri";
                hierchy3[1] = "Nakshapar";
                hierchy3[2] = "Jari Border";//the 1st chip would be selected on new creation
                return hierchy3;
            } else {
                hierchy3[0] = "Shantipur";
                hierchy3[1] = "Nakshapar";
                hierchy3[2] = "Colour";//the 1st chip would be selected on new creation
                return hierchy3;
            }
        } else {
            if (id == R.id._2) {
                hierchy2[0] = "Baluchuri";
                hierchy2[1] = "Without Nakshapar";
            } else if (id == R.id._3) {
                hierchy2[0] = "Baluchuri";
                hierchy2[1] = "Baluchuri(S)";
            } else if (id == R.id._4) {
                hierchy2[0] = "Baluchuri";
                hierchy2[1] = "Baluchuri(T)";

            } else if (id == R.id._5) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Katki";
            } else if (id == R.id._6) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Stripe";
            } else if (id == R.id._7) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Checks Buti";
            } else if (id == R.id._8) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Plain Buti";

            } else if (id == R.id._9) {
                hierchy2[0] = "Jaamdani";
                hierchy2[1] = "Dhakai";
            } else if (id == R.id._10) {
                hierchy2[0] = "Jaamdani";
                hierchy2[1] = "Matha";

            } else if (id == R.id._11) {
                hierchy2[0] = "Fulia";
                hierchy2[1] = "All";

            } else if (id == R.id._12) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Plain";
            } else if (id == R.id._13) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Checks";
            } else if (id == R.id._14) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Buti Pattern";
            } else if (id == R.id._15) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Embroidery";

            } else if (id == R.id._16) {
                hierchy2[0] = "Shantipur";
                hierchy2[1] = "Jaamdani";
            }
            return hierchy2;
        }
    }


    public static String[] getHierchyString(String idd) {
        int id = Integer.valueOf(idd);
        String hierchy2[] = new String[2];
        String hierchy3[] = new String[3];

        if (id == 1 || id == 17) {
            if (id == 1) {
                hierchy3[0] = "Baluchuri";
                hierchy3[1] = "Nakshapar";
                hierchy3[2] = "Jari Border";//the 1st chip would be selected on new creation
                return hierchy3;
            } else {
                hierchy3[0] = "Shantipur";
                hierchy3[1] = "Nakshapar";
                hierchy3[2] = "Colour";//the 1st chip would be selected on new creation
                return hierchy3;
            }
        } else {
            if (id == 2) {
                hierchy2[0] = "Baluchuri";
                hierchy2[1] = "Without Nakshapar";
            } else if (id == 3) {
                hierchy2[0] = "Baluchuri";
                hierchy2[1] = "Baluchuri(S)";
            } else if (id == 4) {
                hierchy2[0] = "Baluchuri";
                hierchy2[1] = "Baluchuri(T)";

            } else if (id == 5) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Katki";
            } else if (id == 6) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Stripe";
            } else if (id == 7) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Checks Buti";
            } else if (id == 8) {
                hierchy2[0] = "Dhaniya Kali";
                hierchy2[1] = "Plain Buti";

            } else if (id == 9) {
                hierchy2[0] = "Jaamdani";
                hierchy2[1] = "Dhakai";
            } else if (id == 10) {
                hierchy2[0] = "Jaamdani";
                hierchy2[1] = "Matha";

            } else if (id == 11) {
                hierchy2[0] = "Fulia";
                hierchy2[1] = "All";

            } else if (id == 12) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Plain";
            } else if (id == 13) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Checks";
            } else if (id == 14) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Buti Pattern";
            } else if (id == 15) {
                hierchy2[0] = "Baha Cotton";
                hierchy2[1] = "Embroidery";

            } else if (id == 16) {
                hierchy2[0] = "Shantipur";
                hierchy2[1] = "Jaamdani";
            }
            return hierchy2;
        }
    }


    public static ArrayList<ChipModel> getChipModel(int id) {
        ArrayList<ChipModel> list = new ArrayList<>();
        ChipModel chipModel1, chipModel2, chipModel3, chipModel4;
        if (id == R.id._1) {
            chipModel1 = new ChipModel("Baluchuri", "Nakshapar", "Jari Border");
            chipModel2 = new ChipModel("Baluchuri", "Nakshapar", "Without Jari Border");
            list.add(chipModel1);
            list.add(chipModel2);
            return list;
        } else if (id == R.id._17) {
            chipModel1 = new ChipModel("Shantipur", "Nakshapar", "Colour");
            chipModel2 = new ChipModel("Shantipur", "Nakshapar", "White Cream");
            chipModel3 = new ChipModel("Shantipur", "Nakshapar", "Embroidery");
            chipModel4 = new ChipModel("Shantipur", "Nakshapar", "Print");
            list.add(chipModel1);
            list.add(chipModel2);
            list.add(chipModel3);
            list.add(chipModel4);
            return list;
        } else
            return null;
    }

    public static ArrayList<ChipModel> getChipModelString(String idd) {
        int id = Integer.valueOf(idd);
        ArrayList<ChipModel> list = new ArrayList<>();
        ChipModel chipModel1, chipModel2, chipModel3, chipModel4;
        if (id == 1) {
            chipModel1 = new ChipModel("Baluchuri", "Nakshapar", "Jari Border");
            chipModel2 = new ChipModel("Baluchuri", "Nakshapar", "Without Jari Border");
            list.add(chipModel1);
            list.add(chipModel2);
            return list;
        } else if (id == 17) {
            chipModel1 = new ChipModel("Shantipur", "Nakshapar", "Colour");
            chipModel2 = new ChipModel("Shantipur", "Nakshapar", "White Cream");
            chipModel3 = new ChipModel("Shantipur", "Nakshapar", "Embroidery");
            chipModel4 = new ChipModel("Shantipur", "Nakshapar", "Print");
            list.add(chipModel1);
            list.add(chipModel2);
            list.add(chipModel3);
            list.add(chipModel4);
            return list;
        } else
            return null;
    }


    public static int StringId_tointId(String id) {
        int idd = Integer.valueOf(id);
        if (idd == 1) {
            return R.id._1;
        } else if (idd == 2) {
            return R.id._2;

        } else if (idd == 3) {
            return R.id._3;

        } else if (idd == 4) {
            return R.id._4;

        } else if (idd == 5) {
            return R.id._5;

        } else if (idd == 6) {
            return R.id._6;

        } else if (idd == 7) {
            return R.id._7;

        } else if (idd == 8) {
            return R.id._8;

        } else if (idd == 9) {
            return R.id._9;

        } else if (idd == 10) {
            return R.id._10;

        } else if (idd == 11) {
            return R.id._11;

        } else if (idd == 12) {
            return R.id._12;

        } else if (idd == 13) {
            return R.id._13;

        } else if (idd == 14) {
            return R.id._14;

        } else if (idd == 15) {
            return R.id._15;

        } else if (idd == 16) {
            return R.id._16;

        } else if (idd == 17) {
            return R.id._17;

        }
        return 0;
    }

    synchronized public static String[] getCodes(List<CartModel> list) {
        int size = 0;
        if (list != null)
            size = list.size();

        String[] s = new String[size];
        for (int i = 0; i < size; i++) {
            s[i] = list.get(i).getCode();
        }
        return s;
    }

    public static String getPresentDate() {
       return new SimpleDateFormat("EEE, d MMM yyyy").format(Calendar.getInstance().getTime());
     /*   Date date = new Date();
        sd.setTimeZone(TimeZone.getTimeZone("IST"));
        return sd.format(date);*/
    }

    public static ArrayList<String> geEachUrl(String urls) {
        int indexOfSpace = 0;
        int nextIndexOfSpace = 0;
        ArrayList<String> list = new ArrayList<>();
        String word;
        if (urls != null) {
            int lastIndexOfSpace = urls.lastIndexOf(' ');
            while (indexOfSpace < lastIndexOfSpace) {
                nextIndexOfSpace = urls.indexOf(' ', indexOfSpace);
                if (nextIndexOfSpace > indexOfSpace) {
                    word = urls.substring(indexOfSpace, nextIndexOfSpace);
                    list.add(word);
                    Log.d("KIMI", word);
                }
                indexOfSpace = nextIndexOfSpace;
            }
            String lastWord;
            if (lastIndexOfSpace > 0) {
                lastWord = urls.substring(lastIndexOfSpace);
                Log.d("KIMI", lastWord);
                list.add(lastWord);
            }
        }
        if (list.size() == 0) {
            Log.d("KIMI", urls);
            list.add(urls);
        }

        return list;
    }


    public static ArrayList<String> geEachUrl2(String urls) {

        ArrayList<String> list = new ArrayList<>();
        String word = "";
        for (int i = 0; i < urls.length(); i++) {
            if (urls.charAt(i) == ' ') {
                list.add(word);
                Log.d("KIMI", word);
                word = "";
                continue;
            }
            word = word + Character.toString(urls.charAt(i));
            if (i == urls.length() - 1) {
                list.add(word);
                Log.d("KIMI", word);
                word = "";
            }
        }
        return list;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}


 /*     _1-- Jari Border
             Without Jari Border

        _17- Colour
             White-Cream
             Embroidery
             Print */

