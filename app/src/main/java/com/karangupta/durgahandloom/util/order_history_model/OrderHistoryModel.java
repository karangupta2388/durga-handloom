package com.karangupta.durgahandloom.util.order_history_model;

/**
 * Created by karangupta on 04/04/18.
 */

public class OrderHistoryModel {
    private String OrderId;
    private  String FirstName;
    private String SecondName;
     OrderHistoryModel()
    {

    }
    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        this.Address = address;
    }

    private String Address;
    private String Pin;
    private String City;
    private String Phone;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }


    public String getUId() {
        return UId;
    }

    public void setUId(String UId) {
        this.UId = UId;
    }

    private String UId;
    private String Date;
    private String Quantity;


    public OrderHistoryModel(String orderId, String firstName, String secondname, String address, String pin, String city, String phone, String UId, String date, String quantity) {
        this.OrderId = orderId;
        FirstName = firstName;
        SecondName = secondname;
        this.Address = address;
        Pin = pin;
        City = city;
        Phone = phone;
        this.UId = UId;
        Date = date;
        Quantity = quantity;
    }



    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }




    public String getPin() {
        return Pin;
    }

    public void setPin(String pin) {
        Pin = pin;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }


    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }
}

