package com.karangupta.durgahandloom.util.order_detail_model;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

/**
 * Created by karangupta on 04/04/18.
 */

public class OrderDetailModel {
    private String OrderId;
    private  String FirstName;
    OrderDetailModel()
    {

    }
    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    private String SecondName;
    private String Address;
    private String Pin;
    private String City;
    private String Phone;
    private String UId;
    private String Date;
    private String Quantity;

    private List<CartModel> Cart;


    public OrderDetailModel(String orderId, String firstName, String secondname, String address, String pin, String city, String phone, String UId, String date, String quantity, List<CartModel> cart) {
        this.OrderId = orderId;
        FirstName = firstName;
        this.SecondName = secondname;
        this.Address = address;
        Pin = pin;
        City = city;
        Phone = phone;
        this.UId = UId;
        Date = date;
        Quantity = quantity;
        Cart = cart;
    }



    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPin() {
        return Pin;
    }

    public void setPin(String pin) {
        Pin = pin;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getUId() {
        return UId;
    }

    public void setUId(String UId) {
        this.UId = UId;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public List<CartModel> getCart() {
        return Cart;
    }

    public void setCart(List<CartModel> cart) {
        Cart = cart;
    }
}
