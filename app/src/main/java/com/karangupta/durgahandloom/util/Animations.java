package com.karangupta.durgahandloom.util;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.karangupta.durgahandloom.R;

import static com.karangupta.durgahandloom.util.Utilities.setView_Height;

/**
 * Created by karangupta on 27/03/18.
 */

public class Animations  {

    public static void animateHeight(final View view, int from, int to, int duration) {
        ValueAnimator anim = ValueAnimator.ofInt(from, to);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                setView_Height(view,val);
            }
        });
        anim.setDuration(duration);
        anim.start();
    }

    public static void onTapAnimation(final ImageView imageView, final int i, Context context) {
        //if(i==1)
        //    sareesActivity.addWishListImageViewstoList(imageView);
        //imageView.setBackgroundResource(R.drawable.chat_background);
        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        myAnim.setDuration(650);
        myAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //starting animation drawable
                if(i==1)
                {
                    imageView.setBackgroundResource(R.drawable.animation_list_cart);
                    AnimationDrawable anim = (AnimationDrawable) imageView.getBackground();
                    anim.start();
                }else if(i==2)
                {
                    imageView.setBackgroundResource(R.drawable.animation_list_cart_reverse);
                    AnimationDrawable anim = (AnimationDrawable) imageView.getBackground();
                    anim.start();
                }else if(i==3)
                {
                    imageView.setBackgroundResource(R.drawable.animation_list_favourites);
                    AnimationDrawable anim = (AnimationDrawable) imageView.getBackground();
                    anim.start();
                }else if(i==4)
                {
                    imageView.setBackgroundResource(R.drawable.animation_list_favourites_reverse);
                    AnimationDrawable anim = (AnimationDrawable) imageView.getBackground();
                    anim.start();
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(i==1)
                {
                    ((AnimationDrawable)(imageView.getBackground())).stop();
                    imageView.setBackgroundDrawable(null);
                    //imageView.setBackgroundResource(R.drawable.animation);
                    imageView.setBackgroundResource(R.drawable.add_to_cart5);
                    //myAnim.cancel();
                    //myAnim.reset();
                }else if(i==2)
                {
                    imageView.setBackgroundResource(R.drawable.add_to_cart1);
                    myAnim.cancel();
                    myAnim.reset();
                }else if(i==3)
                {
                    imageView.setBackgroundResource(R.drawable.add_to_favourite5);

                }else if(i==4)
                {
                    imageView.setBackgroundResource(R.drawable.add_to_favourite1);

                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        imageView.startAnimation(myAnim);
    }

}
