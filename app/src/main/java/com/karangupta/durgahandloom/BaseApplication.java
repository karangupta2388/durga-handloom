package com.karangupta.durgahandloom;

import android.os.StrictMode;

import com.karangupta.durgahandloom.address.AddressActivityComponent;
import com.karangupta.durgahandloom.address.AddressActivityModule;
import com.karangupta.durgahandloom.cart.CartComponent;
import com.karangupta.durgahandloom.cart.CartModule;
import com.karangupta.durgahandloom.collections.CollectionsComponent;
import com.karangupta.durgahandloom.collections.CollectionsModule;
import com.karangupta.durgahandloom.details.DetailsComponent;
import com.karangupta.durgahandloom.details.DetailsModule;
import com.karangupta.durgahandloom.firebase.FirebaseeModule;
import com.karangupta.durgahandloom.local_storage.LocalStorageModule;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentComponent;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentModule;
import com.karangupta.durgahandloom.main_activity.order_history.OrderHistoryComponent;
import com.karangupta.durgahandloom.main_activity.order_history.OrderHistoryModule;
import com.karangupta.durgahandloom.order_detail.OrderDetailComponent;
import com.karangupta.durgahandloom.order_detail.OrderDetailModule;
import com.karangupta.durgahandloom.wishlist.WishlistComponent;
import com.karangupta.durgahandloom.wishlist.WishlistModule;

/**
 * @author arun
 */
public class BaseApplication extends com.activeandroid.app.Application {
    private AppComponent appComponent;
    private MainFragmentComponent mainFragmentComponent;
    private CollectionsComponent collectionsComponent;
    private CartComponent cartComponent;
    private AddressActivityComponent addressActivityComponent;
    private WishlistComponent wishlistComponent;
    private OrderHistoryComponent orderHistoryComponent;
    private OrderDetailComponent orderDetailComponent;
    private DetailsComponent detailsComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.enableDefaults();
        appComponent = createAppComponent();
    }

    private AppComponent createAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .firebaseeModule(new FirebaseeModule())
                .localStorageModule(new LocalStorageModule())
                .build();
    }

    public CollectionsComponent createCollectionsComponent() {
        collectionsComponent = appComponent.plus(new CollectionsModule());
        return collectionsComponent;
    }

    public MainFragmentComponent createMainFragmentComponent() {
        mainFragmentComponent = appComponent.plus(new MainFragmentModule());
        return mainFragmentComponent;
    }

    public CartComponent createCartComponent() {
        cartComponent = appComponent.plus(new CartModule());
        return cartComponent;
    }

    public AddressActivityComponent createAddressActivityComponent() {
        addressActivityComponent = appComponent.plus(new AddressActivityModule());
        return addressActivityComponent;
    }

    public WishlistComponent createWishlistComponent() {
        wishlistComponent = appComponent.plus(new WishlistModule());
        return wishlistComponent;
    }

    public OrderHistoryComponent createOrderHistoryComponent() {
        orderHistoryComponent = appComponent.plus(new OrderHistoryModule());
        return orderHistoryComponent;
    }

    public OrderDetailComponent createOrderDetailComponent() {
        orderDetailComponent = appComponent.plus(new OrderDetailModule());
        return orderDetailComponent;
    }

    public DetailsComponent createDetailsComponent() {
        detailsComponent = appComponent.plus(new DetailsModule());
        return detailsComponent;
    }

    public void releaseMainFragmentComponent() {
        mainFragmentComponent = null;
    }

    public void releaseCollectionsComponent() {
        collectionsComponent = null;
    }

    public void releaseAddressActivityComponent() {
        addressActivityComponent = null;
    }

    public void releaseWishlistComponent() {
        wishlistComponent = null;
    }

    public void releaseOrderHistoryComponent() {
        orderHistoryComponent = null;
    }

    public void releaseOrderDetailComponent() {
        orderDetailComponent = null;
    }

    public void releaseDetailsComponent() {
        detailsComponent = null;
    }

    public MainFragmentComponent getMainFragmentComponent() {
        return mainFragmentComponent;
    }

    public void releaseCartomponent() {
        cartComponent = null;
    }
}
