package com.karangupta.durgahandloom.intro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.login.EmailPasswordActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.splash_screen);
        ImageView imageView=(ImageView)findViewById(R.id.splash);
        imageView.getLayoutParams().width = (int) (getResources().getDisplayMetrics().widthPixels*0.50 );
        imageView.getLayoutParams().height = imageView.getLayoutParams().width;
                //(int) ((getResources().getDisplayMetrics().heightPixels * 0.20));
        new Handler().postDelayed(new Runnable() {

            // Using handler with postDelayed called runnable run method

            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, EmailPasswordActivity.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, 1000);

    }
}
