package com.karangupta.durgahandloom.login;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customfonts.MyEditText;

/**
 * Created by karangupta on 17/04/18.
 */

public class ForgotPassword extends AppCompatActivity {

    private  final String TAG = "TAG";
    private Unbinder unbinder;
    @BindView(R.id.edit_text)
    MyEditText edit_text;

    @BindView(R.id.button)
    Button button;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.forgot_password);
        unbinder=ButterKnife.bind(this);

        button.setOnClickListener(view -> {
            if((edit_text.getText().toString().contains("@")) && edit_text.getText().toString().contains("."))
            {
                FirebaseAuth.getInstance().sendPasswordResetEmail(edit_text.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Utilities.hideKeyboard(ForgotPassword.this);
                                    Toast.makeText(ForgotPassword.this, "Email Sent", Toast.LENGTH_LONG).show();
                                    //Snackbar.make(button,"Email Sent",Snackbar.LENGTH_LONG).show();
                                    finish();
                                }
                            }
                        }).addOnFailureListener(e -> Snackbar.make(button,"Something went Wrong",Snackbar.LENGTH_LONG).show());
            }
            else
            {
                Toast.makeText(ForgotPassword.this, "Wrong Input", Toast.LENGTH_LONG).show();

            }
        });
    }

}
