package com.karangupta.durgahandloom.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.FirebaseDatabase;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.Typewriter;
import com.karangupta.durgahandloom.main_activity.MainActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EmailPasswordActivity extends AppCompatActivity implements
        View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {
    public ProgressDialog mProgressDialog;
    private TextView forgot_password;
/*

res/layout/main_activity.xml           # For handsets (smaller than 600dp available width)
res/layout-sw600dp/main_activity.xml   # For 7” tablets (600dp wide and bigger)
res/layout-sw720dp/main_activity.xml   # For 10” tablets (720dp wide and bigger)

res/layout/main_activity.xml         # For handsets (smaller than 600dp available width)
res/layout-w600dp/main_activity.xml  # Multi-pane (any screen with 600dp available width or more)
Nexus-S and Nexus-One create layout-w480dp
 */
    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


    private static final String TAG = "EmailPassword";

    private TextView mStatusTextView;
    private TextView mDetailTextView;
    private EditText mEmailField;
    private EditText mPasswordField;
    CallbackManager callbackManager;
    ProfileTracker profileTracker;
    AccessToken accessToken;
    AccessTokenTracker accessTokenTracker;
    //EditText ed;
    String graphAPiVariable;
    // [START declare_auth]
    private FirebaseAuth mAuth;
    private String a;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 12345;
    private TextView ed;

    // [END declare_auth]

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        setContentView(R.layout.activity_emailpassword);
        forgot_password = (TextView) findViewById(R.id.forgot_password);
        if (forgot_password != null)
        {
            forgot_password.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(EmailPasswordActivity.this, ForgotPassword.class));
                }
            });
    }
        ImageView im=(ImageView)findViewById(R.id.imageView);

        Typeface face= Typeface.createFromAsset(getAssets(),
                "fonts/ElsieSwashCaps-Black.ttf");
        Typewriter writer=findViewById(R.id.typewriter);
        TextView tv=(TextView)findViewById(R.id.custom);
        //Add a character every 150ms

        writer.setCharacterDelay(150);
        writer.animateText("Durga Handloom\nSarees");
        writer.setTypeface(face);
        //Using res/anim/uptodown
        Animation.AnimationListener collapseListener=new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {

            }
            public void onAnimationRepeat(Animation animation) { // not needed
            }
            public void onAnimationStart(Animation animation) { // not needed
            } };
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        animation.setAnimationListener(collapseListener);
        im.setAnimation(animation);
        // Views

      //  mStatusTextView = (TextView) findViewById(R.id.status);
        //mDetailTextView = (TextView) findViewById(R.id.detail);
        mEmailField = (EditText) findViewById(R.id.field_email);
        mPasswordField = (EditText) findViewById(R.id.field_password);

        // Buttons
        findViewById(R.id.email_sign_in_button).setOnClickListener(this);
        findViewById(R.id.email_create_account_button).setOnClickListener(this);
       // findViewById(R.id.sign_out_button).setOnClickListener(this);
       // findViewById(R.id.verify_email_button).setOnClickListener(this);

        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                // again go to login screen
            }
        });
        // [END initialize_auth]

        //fb login button
        final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
       // ed = (EditText) findViewById(R.id.editText);
//            loginButton.setReadPermissions("email");
        loginButton.setReadPermissions("public_profile", "email");

        //google sign in button
        com.shobhitpuri.custombuttons.GoogleSignInButton signInButton = (com.shobhitpuri.custombuttons.GoogleSignInButton) findViewById(R.id.sign_in_button);

Typewriter typewriter= (Typewriter)findViewById(R.id.typewriter);
        LinearLayout linearLayout=(LinearLayout)findViewById(R.id.linear_layout);
        Button b=(Button)findViewById(R.id.email_sign_in_button);
        Button bb=(Button)findViewById(R.id.email_create_account_button);
        typewriter.getLayoutParams().width = (int) (getResources().getDisplayMetrics().widthPixels );
        typewriter.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.20));
      //  linearLayout.getLayoutParams().width = (int) (getResources().getDisplayMetrics().widthPixels );
      //  linearLayout.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.22));

    //    loginButton.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.044));
     //   signInButton.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.044));
     //   b.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.044));
     //   bb.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.5));






        if(getResources().getDisplayMetrics().widthPixels>=600 && getResources().getDisplayMetrics().widthPixels<=700)
        {
         //   b.setTextSize(20);
       //     bb.setTextSize(20);
        }else if(getResources().getDisplayMetrics().widthPixels>700)
        {
           // b.setTextSize(24);
          //  bb.setTextSize(24);
        }
        if(getResources().getDisplayMetrics().widthPixels<=480 )
        {
            //loginButton.setTextSize(7);
            //loginButton.setTextSize(20);
        //    linearLayout.getLayoutParams().height = (int) ((getResources().getDisplayMetrics().heightPixels * 0.40));
        }




        callbackManager = CallbackManager.Factory.create();//for facebook
        // If using in a fragment`
//            loginButton.setFragment(this);
        // Other app specific specialization

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                handleFacebookAccessToken(loginResult.getAccessToken());
                // Profile profile = Profile.getCurrentProfile();
                try {
                    //        Toast.makeText(EmailPasswordActivity.this, "onSuccess Login\n1. " + profile.getFirstName() + "\n2. " + profile.getId() + "\n3. " + profile.getLastName() + "\n4. " + profile.getMiddleName() + "\n5. " + profile.getLastName() + "\n6. " + profile.getName() + "\n8. " + profile.getProfilePictureUri(200, 200), Toast.LENGTH_LONG).show();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                //  String a = loginResult.getAccessToken().getUserId();

                Log.d("KARAN", "onSuccess  Login" + loginResult.getAccessToken() + "  " + a);

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

//You have to atlease store userid and accesstoken in database or shared references
        AccessTokenTracker fbTracker;
        fbTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken2) {
                if (accessToken2 == null) {
                    Log.d("FB", "User Logged Out.");
                    signOut();
                }
            }
        };
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        // ed = (TextView) findViewById(R.id.mStatusTextView);
       /* b = (Button) findViewById(R.id.logout);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    // ...
                    case R.id.logout:
                        signOut();
                        break;
                    // ...
                }

            }
        });*/
       // signInButton.setSize(SignInButton.SIZE_STANDARD);
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.sign_in_button:
                        signIn_Google();
                        break;
                    // ...
                }

            }
        });
    }




    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            // Request only the user's ID token, which can be used to identify the
            // user securely to your backend. This will contain the user's basic
            // profile (name, profile picture URL, etc) so you should not need to
            // make an additional call to personalize your application.We need server client id for IdToken to be returned else
            //null would be returned
//            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                    .requestIdToken(getString(R.string.server_client_id))
//                    .build();
           // ed.setText("\nDisplayName:" + acct.getDisplayName() + "\nEmail:" + acct.getEmail() + "\nId:" + acct.getId() + "\ngetIdToken:" + acct.getIdToken() + "\nPhotoUrl:" + acct.getPhotoUrl() + "\nServerAuthCode:" + acct.getServerAuthCode() + "\n\n");
//            updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}


    private void signIn_Google() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void handleFacebookAccessToken(AccessToken token) {
        //Log.d(TAG, "handleFacebookAccessToken:" + token);
        // [START_EXCLUDE silent]
        showProgressDialog();
        // [END_EXCLUDE]

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed. Login from other Credential.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
    }


    private void calculateHashKey(String yourPackageName) {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    yourPackageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:",
                        Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_check_user]

    @Override
    protected void onStop() {
        hideProgressDialog();
        super.onStop();
    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed createUserWithEmailAndPassword",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END create_user_with_email]
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmailPasswordActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        if (!task.isSuccessful()) {
                          //  mStatusTextView.setText(R.string.auth_failed);
                        }
                        hideProgressDialog();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }

    private void signOut() {
        mAuth.signOut();
        updateUI(null);
    }

    private void sendEmailVerification() {
        // Disable button
       // findViewById(R.id.verify_email_button).setEnabled(false);

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        // Re-enable button
                       // findViewById(R.id.verify_email_button).setEnabled(true);

                        if (task.isSuccessful()) {
                            Toast.makeText(EmailPasswordActivity.this,
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(EmailPasswordActivity.this,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [END_EXCLUDE]
                    }
                });
        // [END send_email_verification]
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        return valid;
    }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
           // mStatusTextView.setText(getString(R.string.emailpassword_status_fmt,user.getEmail(), user.isEmailVerified()));
           // mDetailTextView.setText(getString(R.string.firebase_status_fmt, user.getUid()));

            findViewById(R.id.email_password_buttons).setVisibility(View.GONE);
            findViewById(R.id.email_password_fields).setVisibility(View.GONE);
          //  findViewById(R.id.signed_in_buttons).setVisibility(View.VISIBLE);
           // Toast.makeText(this, "user!=null", Toast.LENGTH_SHORT).show();
           try{ FirebaseDatabase.getInstance().getReference().child("KARAN").setValue(user.getEmail()+" "+user.getDisplayName()+" "+user.getMetadata());}catch (Exception e){e.printStackTrace();}
          //  findViewById(R.id.verify_email_button).setEnabled(!user.isEmailVerified());



            startActivity(new Intent(this,MainActivity.class));
            finish();
        } else {
//            mStatusTextView.setText(R.string.signed_out);
          //  mDetailTextView.setText(null);
            //Toast.makeText(this, "user==null", Toast.LENGTH_SHORT).show();
FirebaseAuth.getInstance().addAuthStateListener(new FirebaseAuth.AuthStateListener() {
    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

    }
});
FirebaseAuth.getInstance().addIdTokenListener(new FirebaseAuth.IdTokenListener() {
    @Override
    public void onIdTokenChanged(@NonNull FirebaseAuth firebaseAuth) {

    }
});
            try{ FirebaseDatabase.getInstance().getReference().child("KARAN").setValue("1");}catch (Exception e){e.printStackTrace();
                Toast.makeText(this, "Exception thrown to access database", Toast.LENGTH_SHORT).show();}

            findViewById(R.id.email_password_buttons).setVisibility(View.VISIBLE);
            findViewById(R.id.email_password_fields).setVisibility(View.VISIBLE);
          //  findViewById(R.id.signed_in_buttons).setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.email_create_account_button) {
            createAccount(mEmailField.getText().toString(), mPasswordField.getText().toString());
        } else if (i == R.id.email_sign_in_button) {
            signIn(mEmailField.getText().toString(), mPasswordField.getText().toString());
        } //else if (i == R.id.sign_out_button) {
           // signOut();
        //} else if (i == R.id.verify_email_button) {
        //    sendEmailVerification();
       // }
    }
}