package com.karangupta.durgahandloom.cart;


import android.support.annotation.NonNull;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentModule;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 02/04/18.
 */
@Module
public class CartModule  {

    @Provides
    CartInteractor provideCartInteractor(@NonNull FirebaseStore firebaseStore) {
        return new CartInteractorImpl(firebaseStore);
    }

    @Provides
    CartPresenter provideCartPresenter(@NonNull ConnectionDetector connectionDetector,
                                       @NonNull CartInteractor cartInteractor,
                                       @NonNull LocalStorageStoreInteractor localStorageStoreInteractor) {
        return new CartPresenterImpl(connectionDetector, cartInteractor, localStorageStoreInteractor);
    }
}