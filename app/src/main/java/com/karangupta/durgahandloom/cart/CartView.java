package com.karangupta.durgahandloom.cart;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

/**
 * Created by karangupta on 02/04/18.
 */
interface CartView {

    void stopLoadingAnim();

    void displayCount(@Nullable String add_or_remove,
                      @Nullable String cart_or_favourite,
                      @NonNull String[] count);

    void loadingFailed(@NonNull String message,
                       @NonNull String countAddOrRemove,
                       @NonNull String cart_or_favourites);

    void loadingStarted();

    void stopLoading();

    void displayCartView(@NonNull List<CartModel> cartModel_List);

    //From Cart Adapter
    void calculateTotal();

    void displayTotal(@NonNull String total);

    void displayEmptyList();

    void displayMovedToWishlist();

    void startLoadingAnim();

    void noInternet();
}
