package com.karangupta.durgahandloom.cart;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 02/04/18.
 */
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface CartScope {
}
