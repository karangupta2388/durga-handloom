package com.karangupta.durgahandloom.cart;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.firebase.database.DataSnapshot;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.List;

/**
 * Created by karangupta on 02/04/18.
 */
public class CartPresenterImpl implements CartPresenter {

    @NonNull private final ConnectionDetector connectionDetector;
    @NonNull private final LocalStorageStoreInteractor localStorageStoreInteractor;
    @NonNull private final CartInteractor cartInteractor;
    @Nullable private CartView view;
    @Nullable private DataSnapshot dataSnapshot; // snapshot of presently loaded view
    @Nullable private List<CartModel> present_list; // List that is presently visible

    private int remove; // to store which dropdown id is having a border
    private int from; // For pagination to load "from" to 15 more than from ie from+15

    public CartPresenterImpl(@NonNull ConnectionDetector connectionDetector,
                             @NonNull CartInteractor cartInteractor,
                             @NonNull LocalStorageStoreInteractor localStorageStoreInteractor) {
        this.cartInteractor = cartInteractor;
        this.connectionDetector = connectionDetector;
        this.localStorageStoreInteractor = localStorageStoreInteractor;
    }

    @Override
    public void setView(@NonNull Cart cart) {
        this.view = cart;
    }

    @Override
    public void askPresenter_displayCount() {
        if (isViewAttached())
            view.displayCount(null, null, localStorageStoreInteractor.getCountValueFromSharedPrefs());
    }

    @Override
    public void fetchCartViewsFromPresenter() {
        if (connectionDetector.isConnectingToInternet() && view != null) {
            view.loadingStarted();
            cartInteractor.fetchCartViews(this);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    @Override
    public void destroy() {
        view = null;
    }

    @Override
    public void addToFavourites(@NonNull CartModel fo4) {
        if (connectionDetector.isConnectingToInternet() && view != null) {
            view.startLoadingAnim();
            cartInteractor.addToFavourites(fo4, "Favourites", this);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    @Override
    public void updateSpinner(@NonNull String item, @NonNull CartModel fo4) {
        if (connectionDetector.isConnectingToInternet()) {
            cartInteractor.updateSpinner(item, fo4, this);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    @Override
    public void removeFromCart(@NonNull CartModel fo4) {
        if (connectionDetector.isConnectingToInternet() && view != null) {
            view.startLoadingAnim();
            cartInteractor.removeFromCart(fo4, this);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    // After removing cart
    public void countUpdatedCart(@NonNull CartModel fo4,
                                 @NonNull String remove,
                                 @NonNull String scount,
                                 @NonNull String cart) {
        localStorageStoreInteractor.setCountValueToSharedPrefs(scount, cart);
        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(), remove, cart);
        if (present_list.contains(fo4))
            present_list.remove(fo4);
        if (isViewAttached()) {
            view.stopLoadingAnim();
            view.displayCount(remove, cart, localStorageStoreInteractor.getCountValueFromSharedPrefs());
        }
        displayCartViews(present_list);
    }

    public void loadingFailed(@NonNull String message,
                              @NonNull String countAddOrRemove,
                              @NonNull String cart_or_favourites) {
        if (isViewAttached())
            view.loadingFailed(message, countAddOrRemove, cart_or_favourites);
    }

    private boolean isViewAttached() {
        return view != null;
    }

    public void displayCartViews(@Nullable List<CartModel> viewsModelList) {
        if (isViewAttached()) {
            if (viewsModelList == null) {
                view.displayEmptyList();
            } else {
                this.present_list = viewsModelList;
                view.stopLoadingAnim();
                view.stopLoading();
                view.displayCartView(viewsModelList);
                if (viewsModelList.isEmpty()) {
                    view.displayEmptyList();
                }
            }
        }
    }

    public void countUpdatedForFavouritesAndCart(@NonNull CartModel fo4,
                                                 @NonNull String add_or_remove,
                                                 @NonNull String count1,
                                                 @NonNull String count2,
                                                 @NonNull String cart_or_favourites) {
        // Setting count value for Cart in shared prefs
        localStorageStoreInteractor.setCountValueToSharedPrefs(count1, "Cart");
        // Setting count value for Favourite in shared prefs
        localStorageStoreInteractor.setCountValueToSharedPrefs(count2, "Favourites");

        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(), "remove", "Cart");
        localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(), "add", "Favourites");
        if (isViewAttached())
            view.displayMovedToWishlist();
        if (present_list != null && present_list.contains(fo4))
            present_list.remove(fo4);
        displayCartViews(present_list);
    }

    public void spinnerUpdated() {
        if (view != null) {
            view.calculateTotal();
        }
    }
}
