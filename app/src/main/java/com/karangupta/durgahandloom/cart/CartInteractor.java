package com.karangupta.durgahandloom.cart;

import android.support.annotation.NonNull;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 02/04/18.
 */
interface CartInteractor {

    void removeFromCart(@NonNull CartModel fo4, @NonNull CartPresenterImpl cartPresenterImpl);

    void fetchCartViews(@NonNull CartPresenterImpl cartPresenterImpl);

    void addToFavourites(@NonNull CartModel fo4,
                         @NonNull String favourites,
                         @NonNull CartPresenterImpl cartPresenter);

    void updateSpinner(@NonNull String item,
                       @NonNull CartModel fo4,
                       @NonNull CartPresenterImpl cartPresenter);
}


