package com.karangupta.durgahandloom.cart;

import android.support.annotation.NonNull;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 02/04/18.
 */

class CartInteractorImpl implements CartInteractor {
    @NonNull
    private final FirebaseStore firebaseStore;

    public CartInteractorImpl(@NonNull FirebaseStore firebaseStore) {
        this.firebaseStore = firebaseStore;

    }

    @Override
    public void removeFromCart(@NonNull CartModel fo4, @NonNull CartPresenterImpl cartPresenterImpl) {
        synchronized (firebaseStore) {
            firebaseStore.removefromCart(fo4, cartPresenterImpl);
        }
    }

    @Override
    public void fetchCartViews(@NonNull CartPresenterImpl cartPresenterImpl) {
        synchronized (firebaseStore) {
            firebaseStore.fetCartViews(cartPresenterImpl);
        }
    }

    @Override
    public void addToFavourites(@NonNull CartModel fo4,
                                @NonNull String favourites,
                                @NonNull CartPresenterImpl cartPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.addtoFavourites(fo4, favourites, cartPresenter);
        }
    }

    @Override
    public void updateSpinner(@NonNull String item,
                              @NonNull CartModel fo4,
                              @NonNull CartPresenterImpl cartPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.updateSpinner(item, fo4, cartPresenter);
        }
    }
}
