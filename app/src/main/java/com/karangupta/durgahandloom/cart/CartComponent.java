package com.karangupta.durgahandloom.cart;

import com.karangupta.durgahandloom.collections.CollectionsActivity;
import com.karangupta.durgahandloom.collections.CollectionsModule;
import com.karangupta.durgahandloom.collections.CollectionsScope;

import dagger.Subcomponent;

/**
 * Created by karangupta on 02/04/18.
 */
@CartScope
@Subcomponent(modules = {CartModule.class})
public interface CartComponent
{
    void inject(Cart target);
}