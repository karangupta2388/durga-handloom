package com.karangupta.durgahandloom.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.Constants;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.address.AddressActivity;
import com.karangupta.durgahandloom.details.Details;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.cart_model.CartModel;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customfonts.MyTextView;
import io.reactivex.annotations.Nullable;

/**
 * Created by karangupta on 02/04/18.
 */
public class Cart extends AppCompatActivity implements CartView {

    @Inject CartPresenter cartPresenter;

    @BindView(R.id.badge_count)
    TextView badge_count;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recycler_view_views)
    RecyclerView recycler_view_views;

    @BindView(R.id.image)
    ImageView image;

    @BindView(R.id.text)
    TextView text;

    @BindView(R.id.place_order_frame_layout)
    FrameLayout place_order_frame_layout;

    @BindView(R.id.appbar_linear_layout2)
    LinearLayout appbar_linear_layout2;

    @BindView(R.id.appbar_linear_layout)
    LinearLayout appbar_linear_layout;

    @BindView(R.id.cross)
    LinearLayout cross;

    @BindView(R.id.place_order)
    MyTextView place_order;

    @BindView(R.id.number)
    MyTextView number;

    @BindView(R.id.loading_frame_layout)
    FrameLayout loading_frame_layout;

    @BindView(R.id.loadingAVI)
    AVLoadingIndicatorView loadingAVI;

    @NonNull
    List<CartModel> cartModel_List = new ArrayList<>();
    @Nullable
    private Unbinder unbinder;
    @Nullable
    private CartAdapter cartAdapter;
    @Nullable
    private CartModel fo4;

    private String total = "0";
    private int intermidiate_value = 0;
    private String spinner_item = "0";

    // Required empty public constructor
    public Cart() {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.cart_activity);
        unbinder = ButterKnife.bind(this);
        setToolbar();
        ((BaseApplication) getApplication()).createCartComponent().inject(this);

        cartPresenter.setView(Cart.this);
        cross.setOnClickListener(view -> onBackPressed());
        place_order.setOnClickListener(view -> startActivity(new Intent(Cart.this, AddressActivity.class)));
        cartPresenter.askPresenter_displayCount();
        initAdapter();
        cartPresenter.fetchCartViewsFromPresenter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.slide_to_bottom);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) {
            return;
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);//cool
        appbar_linear_layout2.setVisibility(View.VISIBLE);
        appbar_linear_layout.setVisibility(View.GONE);
    }

    private void initAdapter() {
        // For Views recycler view
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recycler_view_views.setLayoutManager(gridLayoutManager);
        cartAdapter = new CartAdapter(cartModel_List, this);
        recycler_view_views.setAdapter(cartAdapter);
    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoading() {
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void startLoadingAnim() {
        loadingAVI.show();
        loading_frame_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoadingAnim() {
        loadingAVI.hide();
        loading_frame_layout.setVisibility(View.GONE);
    }

    @Override
    public void displayCartView(@NonNull List<CartModel> cartModel_List) {
        stopLoadingAnim();
        text.setVisibility(View.GONE);
        image.setVisibility(View.GONE);
        place_order_frame_layout.setVisibility(View.VISIBLE);
        stopLoading();
        this.cartModel_List.clear();
        this.cartModel_List.addAll(cartModel_List);
        recycler_view_views.post(() -> {
            if (cartAdapter != null)
                cartAdapter.notifyDataSetChanged();
        });
        int total = 0;
        for (int i = 0; i < cartModel_List.size(); i++) {
            total = total + Integer.parseInt(cartModel_List.get(i).getQuantity());
        }
        displayTotal(String.valueOf(total));
    }

    // From Cart Adapter
    @Override
    public void calculateTotal() {
        stopLoadingAnim();
        if (this.fo4 != null) {
            this.fo4.setQuantity(this.spinner_item);
        }

        int total = Integer.parseInt(this.total) + intermidiate_value;
        displayTotal(String.valueOf(total));
    }

    @Override
    public void displayTotal(@NonNull String total) {
        this.total = total;
        number.setText("Quantity " + total);
    }

    @Override
    public void displayEmptyList() {
        stopLoadingAnim();
        stopLoading();
        text.setVisibility(View.VISIBLE);
        image.setVisibility(View.VISIBLE);
        place_order_frame_layout.setVisibility(View.GONE);
    }

    @Override
    public void displayMovedToWishlist() {
        stopLoadingAnim();
        Snackbar.make(recycler_view_views, "Moved to Wishlist", Snackbar.LENGTH_LONG).show();
    }

    //From CartAdapter
    public void removeFromCart(@NonNull CartModel fo4) {
        cartPresenter.removeFromCart(fo4);
    }

    //From CartAdapter
    public void addToFavourites(@NonNull CartModel fo4) {
        cartPresenter.addToFavourites(fo4);
    }

    @Override
    public void displayCount(@Nullable String add_or_remove,
                             @Nullable String cart_or_favourite,
                             @NonNull String[] count) {
        stopLoadingAnim();

        if (cart_or_favourite != null && add_or_remove != null) {
            if (add_or_remove.equals("add"))
                Snackbar.make(recycler_view_views, "Added to " + cart_or_favourite, Snackbar.LENGTH_SHORT).show();
            else
                Snackbar.make(recycler_view_views, "Removed from " + cart_or_favourite, Snackbar.LENGTH_SHORT).show();
        }

        if (count[1] != null) {
            // Toast.makeText(getActivity(), "count[1]!=null && badge_count!=null", Toast.LENGTH_SHORT).show();
            if (count[1].equals("0")) {
                badge_count.setVisibility(View.GONE);
            } else {
                badge_count.setText(count[1]);
                badge_count.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void loadingFailed(@NonNull String message,
                              @NonNull String countAddOrRemove,
                              @NonNull String cart_or_favourites) {
        stopLoadingAnim();
        progress_bar.setVisibility(View.GONE);
        Snackbar.make(recycler_view_views, "Something went wrong.", Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void noInternet() {
        Snackbar.make(recycler_view_views, R.string.no_internet, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        cartPresenter.askPresenter_displayCount();
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        cartPresenter.destroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
        ((BaseApplication) getApplication()).releaseCartomponent();
        super.onDestroy();
    }

    public void imageClicked(@NonNull CartModel fo4, @NonNull ImageView image_view_show_card) {
        ArrayList<String> list = Utilities.geEachUrl2(fo4.getUrl());
        Intent intent = new Intent(Cart.this, Details.class);
        intent.putStringArrayListExtra(Constants.URLs, list);

        intent.putExtra(Constants.URL, fo4.getUrl());
        intent.putExtra(Constants.SERIES_NAME, fo4.getSeries_Name());
        intent.putExtra(Constants.CODE, fo4.getCode());
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, (View) image_view_show_card, "your_shared_transition_name");
        startActivity(intent, options.toBundle());
    }

    public void updateSpinner(@NonNull String spinner_item,
                              int intermediate_value,
                              @NonNull CartModel fo4) {
        startLoadingAnim();
        this.fo4 = fo4;
        this.intermidiate_value = intermediate_value;
        this.spinner_item = spinner_item;
        cartPresenter.updateSpinner(spinner_item, fo4);
    }
}
