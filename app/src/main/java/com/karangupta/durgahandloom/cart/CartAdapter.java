package com.karangupta.durgahandloom.cart;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.util.Utilities;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

import java.util.ArrayList;
import java.util.List;

import customfonts.MyTextView;

/**
 * Created by karangupta on 31/03/18.
 */
class CartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    @NonNull final List<CartModel> list;
    @NonNull final Cart cart;
    @NonNull final String[] items = {"5", "10", "15", "20", "25", "30", "35", "40", "45", "50"};

    private Context context;

    public CartAdapter(@NonNull List<CartModel> viewsModel_list, @NonNull Cart cart) {
        this.cart = cart;
        this.list = viewsModel_list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        return (new CartAdapter.RowController(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_views, parent, false)));
    }

    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        ((CartAdapter.RowController) holder).bindModel(list.get(position), position);
    }

    private class RowController extends RecyclerView.ViewHolder {
        ImageView image_view_show_card;
        TextView title;
        TextView sub_title;
        LinearLayout remove;
        LinearLayout wishlist;
        MyTextView spinner_text;
        View row;

        private RowController(@NonNull View row) {
            super(row);
            this.row = row;
            this.title = ((TextView) row.findViewById(R.id.title));
            this.sub_title = ((TextView) row.findViewById(R.id.sub_title));
            this.image_view_show_card = ((ImageView) row.findViewById(R.id.image_view_show_card));
            this.remove = ((LinearLayout) row.findViewById(R.id.remove));
            this.wishlist = ((LinearLayout) row.findViewById(R.id.wishlist));
            this.spinner_text = ((MyTextView) row.findViewById(R.id.spinner_text));
            this.image_view_show_card.getLayoutParams().width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.50);
            this.image_view_show_card.getLayoutParams().height = (int) ((context.getResources().getDisplayMetrics().widthPixels * 0.96) / 1.40);
        }

        private void bindModel(final @NonNull CartModel fo4, int position) {
            int px1 = (int) Utilities.convertDpToPixel(0.5f, context);//0.5 dp to pixel
            int px2 = (int) Utilities.convertDpToPixel(2, context);//1 dp to pixel
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) row.getLayoutParams();

            if (position % 2 == 0) {
                params.rightMargin = px1;
                params.topMargin = px1;
                params.bottomMargin = px1;
            } else {
                params.leftMargin = px1;
                params.topMargin = px1;
                params.bottomMargin = px1;

            }

            if (title != null && sub_title != null) {
                if (fo4.getSeries_Name() != null && !fo4.getSeries_Name().equals("")) {
                    String stitle = fo4.getSeries_Name().substring(0, fo4.getSeries_Name().indexOf(' '));
                    String ssub_title = fo4.getSeries_Name().substring(fo4.getSeries_Name().indexOf(' ') + 1, fo4.getSeries_Name().length());
                    StringBuilder sb = new StringBuilder(ssub_title);
                    try {
                        String ssub1 = ssub_title.substring(0, ssub_title.indexOf(' ') + 1);
                        String ssub2 = ssub_title.substring(ssub_title.indexOf(' ') + 1, sb.length());
                        ssub_title = ssub1 + "\n" + ssub2;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    title.setText(stitle);
                    sub_title.setText(ssub_title);
                    title.setVisibility(View.VISIBLE);
                    sub_title.setVisibility(View.VISIBLE);
                } else {
                    title.setVisibility(View.GONE);
                    sub_title.setVisibility(View.GONE);
                }

            }

            ArrayAdapter<String> aa = new ArrayAdapter<String>(context,
                    R.layout.cart_spinner,
                    items);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            if (spinner_text != null) {
                if (fo4.getQuantity() != null && !fo4.getQuantity().equals("")) {

                    spinner_text.setText(fo4.getQuantity() + " \u25BE");
                    spinner_text.setVisibility(View.VISIBLE);
                } else {
                    spinner_text.setVisibility(View.GONE);
                }
            }

            if (spinner_text != null) {
                spinner_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // setup the alert builder
                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Quantity:");

                        builder.setItems(items, (dialog, which) -> {
                            int total = 0;
                            int intermidiate_value = Integer.parseInt(items[which]) - Integer.parseInt(fo4.getQuantity());
                            spinner_text.setText(items[which] + " \u25BE");
                            cart.updateSpinner(items[which], intermidiate_value, fo4);
                        });

                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }

            if (image_view_show_card != null) {
                image_view_show_card.setOnClickListener(view -> cart.imageClicked(fo4, image_view_show_card));
            }

            if (image_view_show_card != null) {
                image_view_show_card.setImageResource(android.R.color.transparent);
                image_view_show_card.setVisibility(View.VISIBLE);
                ArrayList<String> list = Utilities.geEachUrl2(fo4.getUrl());
                Glide.with(context).load(list.get(0))
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(image_view_show_card);
            } else {
                image_view_show_card.setImageResource(android.R.color.transparent);
                image_view_show_card.setVisibility(View.INVISIBLE);
            }

            if (remove != null) {
                remove.setOnClickListener(view -> cart.removeFromCart(fo4));
            }

            if (wishlist != null) {
                wishlist.setOnClickListener(view -> cart.addToFavourites(fo4));
            }
        }
    }
}
