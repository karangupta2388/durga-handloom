package com.karangupta.durgahandloom.cart;

import android.support.annotation.NonNull;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 02/04/18.
 */

interface CartPresenter {

    void removeFromCart(@NonNull CartModel fo4);

    void setView(@NonNull Cart cart);

    void askPresenter_displayCount();

    void fetchCartViewsFromPresenter();

    void destroy();

    void addToFavourites(@NonNull CartModel fo4);

    void updateSpinner(@NonNull String item, CartModel fo4);
}
