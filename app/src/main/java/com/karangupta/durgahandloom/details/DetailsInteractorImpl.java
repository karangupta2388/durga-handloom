package com.karangupta.durgahandloom.details;

import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 06/04/18.
 */

class DetailsInteractorImpl implements DetailsInteractor {
    FirebaseStore firebaseStore;

    public DetailsInteractorImpl(FirebaseStore firebaseStore) {
        this.firebaseStore = firebaseStore;
    }

    @Override
    public void saveToFvourites(CartModel cartModel, String favourites, DetailsPresenterImpl detailsPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.saveToFavourites(cartModel, favourites, detailsPresenter);
        }
    }

    @Override
    public void saveToCart(CartModel cartModel, String cart, DetailsPresenterImpl detailsPresenter) {
        synchronized (firebaseStore) {
            firebaseStore.saveToCART(cartModel, cart, detailsPresenter);
        }

    }
}
