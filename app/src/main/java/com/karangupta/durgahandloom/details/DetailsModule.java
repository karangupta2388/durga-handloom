package com.karangupta.durgahandloom.details;


import com.karangupta.durgahandloom.firebase.FirebaseStore;
import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.main_activity.main_fragment.MainFragmentModule;
import com.karangupta.durgahandloom.util.ConnectionDetector;

import dagger.Module;
import dagger.Provides;

/**
 * Created by karangupta on 06/04/18.
 */
@Module
public class DetailsModule extends MainFragmentModule {
    @Provides
    DetailsInteractor provideDetailsInteractor(FirebaseStore firebaseStore) {
        return new DetailsInteractorImpl(firebaseStore);
    }

    @Provides
    DetailsPresenter provideDetailsPresenter(ConnectionDetector connectionDetector, DetailsInteractor DetailsInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
        return new DetailsPresenterImpl(connectionDetector, DetailsInteractor, localStorageStoreInteractor);
    }
}
