package com.karangupta.durgahandloom.details;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 06/04/18.
 */

interface DetailsInteractor {
    void saveToFvourites(CartModel cartModel, String favourites, DetailsPresenterImpl detailsPresenter);

    void saveToCart(CartModel cartModel, String cart, DetailsPresenterImpl detailsPresenter);
}
