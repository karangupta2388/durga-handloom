package com.karangupta.durgahandloom.details;

import android.support.annotation.NonNull;

import com.karangupta.durgahandloom.local_storage.LocalStorageStoreInteractor;
import com.karangupta.durgahandloom.util.ConnectionDetector;
import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 06/04/18.
 */

public class DetailsPresenterImpl implements DetailsPresenter {
    private DetailsView view;
    ConnectionDetector connectionDetector;
    DetailsInteractor detailsInteractor;
    LocalStorageStoreInteractor localStorageStoreInteractor;

    public DetailsPresenterImpl(ConnectionDetector connectionDetector, DetailsInteractor detailsInteractor, LocalStorageStoreInteractor localStorageStoreInteractor) {
        this.connectionDetector = connectionDetector;
        this.detailsInteractor = detailsInteractor;
        this.localStorageStoreInteractor = localStorageStoreInteractor;
    }

    @Override
    public void setView(Details details) {
        this.view = details;
    }

    @Override
    public void askPresenter_displayCount() {
        if (isViewAttached())
            view.displayCount((localStorageStoreInteractor.getCountValueFromSharedPrefs())[1]);
    }

    @Override
    public void destroy() {
        view = null;
    }

    @Override
    public boolean codePresentinFavouritesDatabase(@NonNull String code) {
        return localStorageStoreInteractor.addedToFavourites(code);
    }

    @Override
    public boolean codePresentinCartDatabase(String code) {
        return localStorageStoreInteractor.addedToCart(code);
    }

    @Override
    public void saveToFvourites(CartModel cartModel) {
        if (connectionDetector.isConnectingToInternet()) {
            detailsInteractor.saveToFvourites(cartModel, "Favourites", this);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    @Override
    public void saveToCart(CartModel cartModel) {
        if (connectionDetector.isConnectingToInternet()) {
            detailsInteractor.saveToCart(cartModel, "Cart", this);
        } else {
            if (isViewAttached()) {
                view.noInternet();
            }
        }
    }

    @Override
    public void askPresenter_displaySeriesName(String series_name) {
        view.displaySeriesName(series_name);
    }

    public void countUpdatedforFavourites_andCart(CartModel fo4, String add, String count1, String count2, String cart_or_favourites) {
        if (cart_or_favourites != null) {
            if (cart_or_favourites.equals("Cart")) {
                localStorageStoreInteractor.setCountValueToSharedPrefs(count1, "Cart");//setting count value for Cart in Sharedprefs
                localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(), "add", "Cart");
                if (isViewAttached())
                    view.displayCount(count1);

            } else {
                localStorageStoreInteractor.setCountValueToSharedPrefs(count2, "Favourites");//setting count value for Favoutite in Sharedprefs
                localStorageStoreInteractor.setCodeInSharedPrefs(fo4.getCode(), "add", "Favourites");

            }
        }


    }

    public void loadingFailed(String message, String countAddorRemove, String cart_or_favourites) {
        if (isViewAttached())
            view.loadingFailed(message, cart_or_favourites);
    }

    private boolean isViewAttached() {
        return view != null;
    }


}
