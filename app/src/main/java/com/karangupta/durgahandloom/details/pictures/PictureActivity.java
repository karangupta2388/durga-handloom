package com.karangupta.durgahandloom.details.pictures;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.karangupta.durgahandloom.Constants;
import com.karangupta.durgahandloom.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by karangupta on 08/04/18.
 */

public class PictureActivity extends AppCompatActivity  {
private int present_position=0;
private ArrayList<String> urls=null;
private  int size=0;
    private PicturesAdapter _adapter;


    private PicturesAdapter picturesAdapter;
    private Unbinder unbinder;

    @BindView(R.id.pager)
    ViewPager _pager;

    @BindView(R.id.thumbnails)
    LinearLayout _thumbnails;

    @BindView(R.id.backkk)
    FrameLayout backkk;



    private GestureDetector gDetector;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.picture_activity);
        unbinder= ButterKnife.bind(this);

        //gDetector = new GestureDetector(this);
        urls = getIntent().getStringArrayListExtra(Constants.URLs);
        //present_position = getIntent().getIntExtra(Constants.POSITION,0);
        getIntent().removeExtra(Constants.URLs);
        if(urls!=null)
        size=urls.size();
        backkk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
      //setImage();
initAdadper();
    }

    private void initAdadper() {
        _adapter = new PicturesAdapter(this,_thumbnails,_pager,urls);
        _pager.setAdapter(_adapter);
        _pager.setOffscreenPageLimit(6); // how many images to load into memory



         // recycler_view_picture_views.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //picturesAdapter = new PicturesAdapter(urls,this );
        //recycler_view_picture_views.setAdapter(picturesAdapter);

    }

   /* private void setImage() {
        Glide.with(this).load(urls.get(this.present_position))
                .asBitmap()
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image);
    }*/

    /*@Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float xVelocity, float yVelocity) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent start, MotionEvent finish, float xVelocity, float yVelocity) {
        if (start.getRawX() < finish.getRawX()) {
            if(present_position==size-1)
            {
                this.present_position=0;
                setImage();
            }else if(present_position>=0 && present_position<size-1)
            {
               present_position++;
               setImage();
            }

        } else {

            if(present_position==0)
            {
                this.present_position=size-1;
                setImage();
            }else if(present_position>0 && present_position<=size-1)
            {
                present_position--;
                setImage();
            }

        }
        return true;    }*/
}
