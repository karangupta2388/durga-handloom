package com.karangupta.durgahandloom.details;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by karangupta on 06/04/18.
 */


@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface DetailsScope {
}
