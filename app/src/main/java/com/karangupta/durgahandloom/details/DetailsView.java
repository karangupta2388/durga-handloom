package com.karangupta.durgahandloom.details;

/**
 * Created by karangupta on 07/04/18.
 */

interface DetailsView {

    void loadingStarted();

    void stopLoading();

    void displayCount(String countValuefromSharedPrefs);

    void loadingFailed(String message, String cart_or_favourites);

    void displaySeriesName(String series_name);

    void noInternet();
}
