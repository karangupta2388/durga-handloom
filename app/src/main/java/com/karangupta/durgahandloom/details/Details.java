package com.karangupta.durgahandloom.details;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.karangupta.durgahandloom.BaseApplication;
import com.karangupta.durgahandloom.Constants;
import com.karangupta.durgahandloom.R;
import com.karangupta.durgahandloom.cart.Cart;
import com.karangupta.durgahandloom.details.pictures.PictureActivity;
import com.karangupta.durgahandloom.util.cart_model.CartModel;
import com.karangupta.durgahandloom.wishlist.Wishlist;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageClickListener;
import com.synnapps.carouselview.ViewListener;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import customfonts.MyTextView;

/**
 * Created by karangupta on 06/04/18.
 */

public class Details extends AppCompatActivity implements DetailsView {

    @Inject
    DetailsPresenter detailsPresenter;

    @BindView(R.id.cart_badgeCombo)
    RelativeLayout cart_badgeCombo;

    @BindView(R.id.heart)
    LinearLayout heart;

    @BindView(R.id.backk)
    FrameLayout backk;

    @BindView(R.id.progress_bar)
    ProgressBar progress_bar;

    @BindView(R.id.carouselView)
    CarouselView carouselView;

    @BindView(R.id.save)
    RelativeLayout save;


    @BindView(R.id.add_to_cart)
    MyTextView add_to_cart;

    @BindView(R.id.save_text)
    MyTextView save_text;

    @BindView(R.id.badge_count)
    TextView badge_count;

    @BindView(R.id.series_nam)
    MyTextView series_nam;

    private Unbinder unbinder;
    private View.OnClickListener onClickListener1, onClickListener2, onClickListener3, onClickListener4;
    private ArrayList<String> urls;
    private String url;//combination of urls
    private String code;
    private String series_name;
    private String quantity;
    private Context contextt;
   /* private boolean addtoCartClicked = false;
    private Set<String> addCartCodeList = new HashSet<>();
    private Set<String> removeCartCodeList = new HashSet<>();
    private Set<String> addFavouriteCodeList = new HashSet<>();
    private Set<String> removeFavouriteCodeList = new HashSet<>();*/

    public Details() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.details_activity);
        unbinder = ButterKnife.bind(this);
        ((BaseApplication) getApplication()).createDetailsComponent().inject(this);
        contextt = this;
        detailsPresenter.setView(this);
        detailsPresenter.askPresenter_displayCount();
        urls = getIntent().getStringArrayListExtra(Constants.URLs);
        url = getIntent().getStringExtra(Constants.URL);
        code = getIntent().getStringExtra(Constants.CODE);
        series_name = getIntent().getStringExtra(Constants.SERIES_NAME);
        quantity = "5";
        getIntent().removeExtra(Constants.URLs);
        getIntent().removeExtra(Constants.CODE);
        getIntent().removeExtra(Constants.SERIES_NAME);
        getIntent().removeExtra(Constants.URL);
        detailsPresenter.askPresenter_displaySeriesName(series_name);


        cart_badgeCombo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Details.this, Cart.class));
                overridePendingTransition(R.anim.slide_to_top, 0);

            }
        });
        heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Details.this, Wishlist.class));

            }

        });

        backk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        ViewListener viewListener = new ViewListener() {

            @Override
            public View setViewForPosition(int position) {
                View customView = getLayoutInflater().inflate(R.layout.custom_carousel, null);
                //set view attributes here
                Context context = customView.getContext();
                ImageView imageView = customView.findViewById(R.id.image);
               /* if(imageView!=null)
                {imageView.getLayoutParams().width = (int) (contextt.getResources().getDisplayMetrics().widthPixels );
                imageView.getLayoutParams().height = (int) ((contextt.getResources().getDisplayMetrics().heightPixels * 0.60));}*/

                Glide.with(context).load(urls.get(position))
                        .asBitmap()
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
                return customView;
            }
        };
        carouselView.setPageCount(urls.size());
        // set ViewListener for custom view
        carouselView.setViewListener(viewListener);
        carouselView.setImageClickListener(new ImageClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(Details.this, PictureActivity.class);
                intent.putExtra(Constants.POSITION, position);
                intent.putStringArrayListExtra(Constants.URLs, urls);
                startActivity(intent);
            }
        });

        Listeners();
        if (detailsPresenter.codePresentinFavouritesDatabase(code)) {
            save_text.setText("SAVED");
            save.setOnClickListener(null);
            save.setOnClickListener(onClickListener1);

        } else {
            save_text.setText("SAVE");
            save.setOnClickListener(null);
            save.setOnClickListener(onClickListener2);

        }


        if (detailsPresenter.codePresentinCartDatabase(code)) {
            add_to_cart.setText("GO TO CART");
            add_to_cart.setOnClickListener(null);
            add_to_cart.setOnClickListener(onClickListener3);

        } else {
            add_to_cart.setText("ADD TO CART");
            add_to_cart.setOnClickListener(null);
            add_to_cart.setOnClickListener(onClickListener4);

        }
    }

    private void Listeners() {
        //SAVED
        onClickListener1 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //empty
            }
        };

        //SAVE
        onClickListener2 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save_text.setText("SAVED");
                save.setOnClickListener(onClickListener1);
                detailsPresenter.saveToFvourites(new CartModel(code, url, series_name, quantity));
            }
        };

        //GO to CART
        onClickListener3 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Details.this, Cart.class));
                overridePendingTransition(R.anim.slide_to_top, 0);
            }
        };


        //ADD to CART
        onClickListener4 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                add_to_cart.setOnClickListener(null);
                add_to_cart.setOnClickListener(onClickListener3);
                detailsPresenter.saveToCart(new CartModel(code, url, series_name, quantity));
                animation();

            }
        };
    }


    @Override
    public void displaySeriesName(String series_name) {
        series_nam.setText(series_name);
    }

    private void animation() {

        add_to_cart.animate().setDuration(800).rotationX(180f).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                add_to_cart.setText("");
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                animator.removeAllListeners();
                animator.cancel();
                animator.end();
                if (add_to_cart != null) {
                    add_to_cart.animate().rotationX(0f).setDuration(0).start();
                    add_to_cart.setText("GO TO CART");
                    add_to_cart.clearAnimation();
                    if (add_to_cart.getAnimation() != null)
                        add_to_cart.getAnimation().cancel();

                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        }).start();
    }

    @Override
    public void displayCount(String count) {


        if (count != null) {
            // Toast.makeText(getActivity(), "count[1]!=null && badge_count!=null", Toast.LENGTH_SHORT).show();
            if (count.equals("0")) {
                badge_count.setVisibility(View.GONE);
            } else {
                badge_count.setText(count);
                badge_count.setVisibility(View.VISIBLE);
            }
        }
        badge_count.setVisibility(View.GONE);

    }

    @Override
    public void noInternet() {
        Snackbar.make(add_to_cart, R.string.no_internet, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void loadingFailed(String errorMessage, String cart_or_favourites) {
        progress_bar.setVisibility(View.GONE);
        if (cart_or_favourites != null) {

            if (cart_or_favourites.equals("Cart")) {
                add_to_cart.setText("ADD TO CART");
                add_to_cart.setOnClickListener(null);
                add_to_cart.setOnClickListener(onClickListener4);
            } else {
                save_text.setText("SAVE");
                save_text.setOnClickListener(null);
                save_text.setOnClickListener(onClickListener2);
            }
        }
        Snackbar.make(add_to_cart, "Something Went Wrong", Snackbar.LENGTH_LONG);
    }

    @Override
    protected void onDestroy() {
        detailsPresenter.destroy();
        unbinder.unbind();
        ((BaseApplication) getApplication()).releaseDetailsComponent();

        super.onDestroy();
    }

    @Override
    protected void onStart() {
        detailsPresenter.askPresenter_displayCount();

        super.onStart();
    }

    @Override
    public void loadingStarted() {
        progress_bar.setVisibility(View.VISIBLE);

    }

    @Override
    public void stopLoading() {
        progress_bar.setVisibility(View.GONE);

    }

}
