package com.karangupta.durgahandloom.details;

import com.karangupta.durgahandloom.util.cart_model.CartModel;

/**
 * Created by karangupta on 06/04/18.
 */

interface DetailsPresenter {

    void setView(Details details);

    void askPresenter_displayCount();

    void destroy();

    boolean codePresentinFavouritesDatabase(String code);

    boolean codePresentinCartDatabase(String code);

    void saveToFvourites(CartModel cartModel);

    void saveToCart(CartModel cartModel);

    void askPresenter_displaySeriesName(String series_name);
}
