package com.karangupta.durgahandloom.details;

import dagger.Subcomponent;

/**
 * Created by karangupta on 06/04/18.
 */


@DetailsScope
@Subcomponent(modules = {DetailsModule.class})
public interface DetailsComponent {
    void inject(Details target);
}