package com.karangupta.durgahandloom.details.pictures;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.karangupta.durgahandloom.R;

import java.util.ArrayList;

/**
 * Created by karangupta on 09/04/18.
 */

class PicturesAdapter extends PagerAdapter {
    private ArrayList<String> _images;
    private PicturesAdapter _adapter;

    ViewPager _pager;
    LinearLayout _thumbnails;
    Context _context;
    LayoutInflater _inflater;

    public PicturesAdapter(Context context, LinearLayout _thumbnails,ViewPager _pager,ArrayList<String> urls) {
        this._thumbnails=_thumbnails;
        this._pager=_pager;
        this._images=urls;
        _context = context;
        _inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return _images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = _inflater.inflate(R.layout.pictures_adapter, container, false);
        container.addView(itemView);

        // Get the border size to show around each image
        int borderSize = _thumbnails.getPaddingTop();

        // Get the size of the actual thumbnail image
        int thumbnailSize = ((FrameLayout.LayoutParams)
                _pager.getLayoutParams()).bottomMargin - (borderSize*2);

        // Set the thumbnail layout parameters. Adjust as required
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(thumbnailSize, thumbnailSize);
        params.setMargins(0, 0, borderSize, 0);

        // You could also set like so to remove borders
        //ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
        //        ViewGroup.LayoutParams.WRAP_CONTENT,
        //        ViewGroup.LayoutParams.WRAP_CONTENT);

        final ImageView thumbView = new ImageView(_context);
        thumbView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        thumbView.setLayoutParams(params);
        thumbView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("KIMI", "Thumbnail clicked");

                // Set the pager position when thumbnail clicked
                _pager.setCurrentItem(position);
            }
        });
        _thumbnails.addView(thumbView);

        final SubsamplingScaleImageView imageView =
                (SubsamplingScaleImageView) itemView.findViewById(R.id.image);

        // Asynchronously load the image and set the thumbnail and pager view
        Glide.with(_context)
                .load(_images.get(position))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        imageView.setImage(ImageSource.bitmap(bitmap));
                        thumbView.setImageBitmap(bitmap);
                    }
                });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}